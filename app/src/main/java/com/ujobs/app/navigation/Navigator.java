package com.ujobs.app.navigation;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.ui.activity.BaseActivity;
import com.ujobs.app.ui.activity.ClientActivity;
import com.ujobs.app.ui.activity.ClientRegisterResquestActivity;
import com.ujobs.app.ui.activity.ClientRequestActivity;
import com.ujobs.app.ui.activity.LoginActivity;
import com.ujobs.app.ui.activity.TypeLoginActivity;
import com.ujobs.app.ui.activity.TypeUserActivity;
import com.ujobs.app.ui.activity.SignInActivity;
import com.ujobs.app.ui.activity.WorkerActivity;
import com.ujobs.app.ui.activity.WorkerRegisterActivity;
import com.ujobs.app.ui.activity.WorkerRequestActivity;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * Created by Manuel Torres on 16/09/17.
 */

public class Navigator {

    public static enum Type {
        SPLASH, LOGIN;

        public void go(BaseActivity poActivity, Navigator poNavigator) {
            if (LOGIN.equals(this)) {
                poNavigator.navigateToLogin();
            }
        }


    }

    private final BaseActivity goContext;

    public Navigator(BaseActivity goContext) {
        this.goContext = goContext;
    }

    public void navigateToLogin() {
        if (goContext != null) {
            Intent loIntentToLaunch = LoginActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToTypeLogin() {
        if (goContext != null) {
            List<String> lasPermission = new ArrayList<>();
            lasPermission.add(READ_PHONE_STATE);
            lasPermission.add(READ_EXTERNAL_STORAGE);
            if (!mayPermission(goContext, lasPermission, Type.LOGIN.ordinal())) {
                return;
            }
            Intent loIntentToLaunch = TypeLoginActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }


    public void navigateToTypeUser() {
        if (goContext != null) {
            Intent loIntentToLaunch = TypeUserActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToClient() {
        if (goContext != null) {
            Intent loIntentToLaunch = ClientActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToSignIn() {
        if (goContext != null) {
            Intent loIntentToLaunch = SignInActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToRegisterRequest(SubcategoryModel poSubcategoryModel) {
        if (goContext != null) {
            Intent loIntentToLaunch = ClientRegisterResquestActivity.getCallingIntent(goContext, poSubcategoryModel);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToClientRequest(RequestModel poRequestModel) {
        if (goContext != null) {
            Intent loIntentToLaunch = ClientRequestActivity.getCallingIntent(goContext, poRequestModel);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToWorkerRegister() {
        if (goContext != null) {
            Intent loIntentToLaunch = WorkerRegisterActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public void navigateToWorker() {
        if (goContext != null) {
            Intent loIntentToLaunch = WorkerActivity.getCallingIntent(goContext);
            goContext.startActivity(loIntentToLaunch);
        }
    }


    public void navigateToWorkerRequest(RequestModel poRequestModel) {
        if (goContext != null) {
            Intent loIntentToLaunch = WorkerRequestActivity.getCallingIntent(goContext, poRequestModel);
            goContext.startActivity(loIntentToLaunch);
        }
    }

    public boolean mayPermission(Activity poContext, List<String> lasPermission, int piType) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        List<String> lasPermissionPending = new ArrayList<>();
        for (String lsPermission : lasPermission) {
            if (poContext.checkSelfPermission(lsPermission) != PackageManager.PERMISSION_GRANTED) {
                lasPermissionPending.add(lsPermission);
            }

        }
        if (lasPermissionPending.isEmpty()) {
            return true;
        } else {
            poContext.requestPermissions(lasPermissionPending.toArray(new String[lasPermissionPending.size()]), piType);
            return false;
        }
    }


}
