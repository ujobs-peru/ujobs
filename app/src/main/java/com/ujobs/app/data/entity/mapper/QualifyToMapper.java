package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.QualifyEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.QualifyModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class QualifyToMapper {


    private static QualifyToMapper INSTANCE;

    public static QualifyToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new QualifyToMapper();
        }
        return INSTANCE;
    }

    public QualifyToMapper() {
    }


    public QualifyEntity transform(QualifyModel poQualifyModel) {

        QualifyEntity loQualifyEntity = null;
        if (poQualifyModel != null) {
            loQualifyEntity = new QualifyEntity();
            ParseUtil.parseObject(poQualifyModel, loQualifyEntity);
        }
        return loQualifyEntity;
    }

    public List<QualifyEntity> transform(List<QualifyModel> poQualifyModel) {

        List<QualifyEntity> lasQualifyEntity = new ArrayList<>();
        if (poQualifyModel != null) {
            for (QualifyModel loQualifyModel : poQualifyModel) {
                QualifyEntity loQualifyEntity = transform(loQualifyModel);
                lasQualifyEntity.add(loQualifyEntity);
            }
        }
        return lasQualifyEntity;
    }
}
