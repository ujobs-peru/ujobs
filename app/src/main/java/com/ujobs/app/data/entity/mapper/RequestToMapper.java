package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.RequestModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RequestToMapper {


    private static RequestToMapper INSTANCE;

    public static RequestToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RequestToMapper();
        }
        return INSTANCE;
    }

    private final CategoryToMapper goCategoryToMapper;
    private final UserToMapper goUserToMapper;

    public RequestToMapper() {
        goCategoryToMapper = CategoryToMapper.newInstance();
        goUserToMapper = UserToMapper.newInstance();
    }


    public RequestEntity transform(RequestModel poRequestModel) {

        RequestEntity loRequestEntity = null;
        if (poRequestModel != null) {
            loRequestEntity = new RequestEntity();
            ParseUtil.parseObject(poRequestModel, loRequestEntity);
            loRequestEntity.setLstCategory(goCategoryToMapper.transform(poRequestModel.getLstCategory()));
            loRequestEntity.setUser(goUserToMapper.transform(poRequestModel.getUser()));
        }
        return loRequestEntity;
    }

    public List<RequestEntity> transform(List<RequestModel> poRequestModel) {

        List<RequestEntity> lasRequestEntity = new ArrayList<>();
        if (poRequestModel != null) {
            for (RequestModel loRequestModel : poRequestModel) {
                RequestEntity loRequestEntity = transform(loRequestModel);
                lasRequestEntity.add(loRequestEntity);
            }
        }
        return lasRequestEntity;
    }
}
