package com.ujobs.app.data.entity;

import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DistrictEntity {

    @JsonProperty("idDistrito")
    private int idDistrict;
    @JsonProperty("nombreDistrito")
    private String name;

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
