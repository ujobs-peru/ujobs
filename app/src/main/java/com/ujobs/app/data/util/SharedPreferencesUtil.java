package com.ujobs.app.data.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.ujobs.app.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by bn on 13/11/17.
 */

public class SharedPreferencesUtil {

    private final Context goContext;

    public SharedPreferencesUtil(Context poContext) {
        this.goContext = poContext;

    }

    public Context getContext() {
        return this.goContext;
    }

    public void putString(String psKey, String psValue) {
        SharedPreferences sharedPref = getContext().getSharedPreferences(goContext.getString(R.string.app_name), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(psKey, psValue);
        editor.commit();
    }

    public String getString(String psKey) {
        SharedPreferences sharedPref = getContext().getSharedPreferences(goContext.getString(R.string.app_name), MODE_PRIVATE);
        return sharedPref.getString(psKey, "");
    }


}
