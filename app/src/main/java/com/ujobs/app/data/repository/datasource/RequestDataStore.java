package com.ujobs.app.data.repository.datasource;


import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.UpdateTypeRequestEntity;

import java.util.List;

/**
 * Interface that represents a data store from where data is retrieved.
 * <p>
 * Created by Manuel Torres on 23/01/2017.
 */

public interface RequestDataStore {


    void registerRequest(CallbackDataStore<Integer> poCallback, RegisterRequestEntity poRequest);

    void listRequestClient(CallbackDataStore<List<RequestEntity>> poCallback, FindRequestEntity poRequest);

    void listRequestWorker(CallbackDataStore<List<RequestEntity>> poCallback, FindRequestEntity poRequest);

    void listQuote(CallbackDataStore<List<QuoteEntity>> poCallback, FindQuoteEntity poRequest);

    void updateTypeRequest(CallbackDataStore<String> poCallback, UpdateTypeRequestEntity poRequest);

    void registerQuote(CallbackDataStore<Integer> poCallback, RegisterQuoteEntity poRequest);

    void registerQualify(final CallbackDataStore<Integer> poCallback, RegisterQualifyEntity poRequest, boolean isClient);


}
