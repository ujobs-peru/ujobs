package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.RegisterRequestModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RegisterRequestToMapper {


    private static RegisterRequestToMapper INSTANCE;

    public static RegisterRequestToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RegisterRequestToMapper();
        }
        return INSTANCE;
    }


    public RegisterRequestEntity transform(RegisterRequestModel poRegisterRequestModel) {

        RegisterRequestEntity loRegisterRequestEntity = null;
        if (poRegisterRequestModel != null) {
            loRegisterRequestEntity = new RegisterRequestEntity();
            ParseUtil.parseObject(poRegisterRequestModel, loRegisterRequestEntity);
        }
        return loRegisterRequestEntity;
    }

    public List<RegisterRequestEntity> transform(List<RegisterRequestModel> poRegisterRequestModel) {

        List<RegisterRequestEntity> lasRegisterRequestEntity = new ArrayList<>();
        if (poRegisterRequestModel != null) {
            for (RegisterRequestModel loRegisterRequestModel : poRegisterRequestModel) {
                RegisterRequestEntity loRegisterRequestEntity = transform(loRegisterRequestModel);
                lasRegisterRequestEntity.add(loRegisterRequestEntity);
            }
        }
        return lasRegisterRequestEntity;
    }
}
