package com.ujobs.app.data.net;

import com.ujobs.app.data.entity.BaseResponseEntity;
import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.UpdateTypeRequestEntity;
import com.ujobs.app.data.entity.UserEntity;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Hernan Pareja on 4/01/2017.
 */

public interface RestApi {

    @POST("{url}")
    Call<BaseResponseEntity<UserEntity>> userLogin(
            @Path(value = "url", encoded = true) String psUrl, @Body UserEntity poBody);

    @POST("{url}")
    Call<BaseResponseEntity<Integer>> userSignIn(
            @Path(value = "url", encoded = true) String psUrl, @Body UserEntity poBody);


    @GET("{url}")
    Call<BaseResponseEntity<List<CategoryEntity>>> listCatAndSubcat(
            @Path(value = "url", encoded = true) String psUrl);

    @GET("{url}")
    Call<BaseResponseEntity<List<DistrictEntity>>> listDistrict(
            @Path(value = "url", encoded = true) String psUrl);


    @POST("{url}")
    Call<BaseResponseEntity<String>> registerWorker(
            @Path(value = "url", encoded = true) String psUrl, @Body RegisterWorkerEntity poBody);

    @POST("{url}")
    Call<BaseResponseEntity<Integer>> registerRequest(
            @Path(value = "url", encoded = true) String psUrl, @Body RegisterRequestEntity poBody);


    @POST("{url}")
    Call<BaseResponseEntity<List<RequestEntity>>> listRequestClient(
            @Path(value = "url", encoded = true) String psUrl, @Body FindRequestEntity poBody);

    @POST("{url}")
    Call<BaseResponseEntity<List<RequestEntity>>> listRequestWorker(
            @Path(value = "url", encoded = true) String psUrl, @Body FindRequestEntity poBody);

    @POST("{url}")
    Call<BaseResponseEntity<List<QuoteEntity>>> listQuote(
            @Path(value = "url", encoded = true) String psUrl, @Body FindQuoteEntity poBody);

    @POST("{url}")
    Call<BaseResponseEntity<String>> updateTypeRequest(
            @Path(value = "url", encoded = true) String psUrl, @Body UpdateTypeRequestEntity poBody);

    @Multipart
    @POST("{url}")
    Call<BaseResponseEntity<String>> uploadFile(@Path(value = "url", encoded = true) String psUrl,
                                                @Part("file\"; filename=\"image.png\"") RequestBody file,
                                                @Part("type") RequestBody type, @Part("id") RequestBody id);


    @POST("{url}")
    Call<BaseResponseEntity<Integer>> registerQuote(
            @Path(value = "url", encoded = true) String psUrl, @Body RegisterQuoteEntity poBody);


    @POST("{url}")
    Call<BaseResponseEntity<Integer>> registerQualify(
            @Path(value = "url", encoded = true) String psUrl, @Body RegisterQualifyEntity poBody);


}
