package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.RequestModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RequestModelToMapper {


    private static RequestModelToMapper INSTANCE;

    public static RequestModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RequestModelToMapper();
        }
        return INSTANCE;
    }


    private final CategoryModelToMapper goCategoryModelToMapper;
    private final UserModelToMapper goUserModelToMapper;

    public RequestModelToMapper() {
        goCategoryModelToMapper = CategoryModelToMapper.newInstance();
        goUserModelToMapper = UserModelToMapper.newInstance();
    }

    public RequestModel transform(RequestEntity poRequestEntity) {

        RequestModel loRequestModel = null;
        if (poRequestEntity != null) {
            loRequestModel = new RequestModel();
            ParseUtil.parseObject(poRequestEntity, loRequestModel);
            loRequestModel.setLstCategory(goCategoryModelToMapper.transform(poRequestEntity.getLstCategory()));
            loRequestModel.setUser(goUserModelToMapper.transform(poRequestEntity.getUser()));
        }
        return loRequestModel;
    }

    public List<RequestModel> transform(List<RequestEntity> poRequestEntity) {

        List<RequestModel> lasRequestModel = new ArrayList<>();
        if (poRequestEntity != null) {
            for (RequestEntity loRequestEntity : poRequestEntity) {
                RequestModel loRequestModel = transform(loRequestEntity);
                lasRequestModel.add(loRequestModel);
            }
        }
        return lasRequestModel;
    }
}
