package com.ujobs.app.data.repository.datasource;


import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;

/**
 * Interface that represents a data store from where data is retrieved.
 * <p>
 * Created by Manuel Torres on 23/01/2017.
 */

public interface UserDataStore {

    void login(CallbackDataStore<UserEntity> poCallback, UserEntity poRequest);

    void signIn(CallbackDataStore<Integer> poCallback, UserEntity poRequest);

    void registerWorker(CallbackDataStore<String> poCallback, RegisterWorkerEntity poRequest);

    void saveUser(UserEntity loUserEntity);

    UserEntity getUser();

}
