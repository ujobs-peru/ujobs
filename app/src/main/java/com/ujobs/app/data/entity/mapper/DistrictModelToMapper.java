package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.DistrictModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class DistrictModelToMapper {


    private static DistrictModelToMapper INSTANCE;

    public static DistrictModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DistrictModelToMapper();
        }
        return INSTANCE;
    }


    public DistrictModel transform(DistrictEntity poDistrictEntity) {

        DistrictModel loDistrictModel = null;
        if (poDistrictEntity != null) {
            loDistrictModel = new DistrictModel();
            ParseUtil.parseObject(poDistrictEntity, loDistrictModel);
        }
        return loDistrictModel;
    }

    public List<DistrictModel> transform(List<DistrictEntity> poDistrictEntity) {

        List<DistrictModel> lasDistrictModel = new ArrayList<>();
        if (poDistrictEntity != null) {
            for (DistrictEntity loDistrictEntity : poDistrictEntity) {
                DistrictModel loDistrictModel = transform(loDistrictEntity);
                lasDistrictModel.add(loDistrictModel);
            }
        }
        return lasDistrictModel;
    }
}
