package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseEntity {
}
