package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Hernan Pareja on 4/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponseEntity<T> {

    @JsonProperty("data")
    private T data;
    @JsonProperty("msg")
    private String msg;
    @JsonProperty("msgError")
    private String msgerror;
    @JsonProperty("codResult")
    private String codResult;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgerror() {
        return msgerror;
    }

    public void setMsgerror(String msgerror) {
        this.msgerror = msgerror;
    }

    public String getCodResult() {
        return codResult;
    }

    public void setCodResult(String codResult) {
        this.codResult = codResult;
    }
}
