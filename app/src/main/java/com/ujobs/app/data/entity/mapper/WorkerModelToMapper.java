package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.WorkerEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.WorkerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class WorkerModelToMapper {


    private static WorkerModelToMapper INSTANCE;
    private final DistrictModelToMapper goDistrictModelToMapper;
    private final CategoryModelToMapper goCategoryModelToMapper;
    private final QualifyModelToMapper goQualifyModelToMapper;


    public static WorkerModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WorkerModelToMapper();
        }
        return INSTANCE;
    }


    public WorkerModelToMapper() {

        goDistrictModelToMapper = DistrictModelToMapper.newInstance();
        goCategoryModelToMapper = CategoryModelToMapper.newInstance();
        goQualifyModelToMapper = QualifyModelToMapper.newInstance();
    }

    public WorkerModel transform(WorkerEntity poWorkerEntity) {

        WorkerModel loWorkerModel = null;
        if (poWorkerEntity != null) {
            loWorkerModel = new WorkerModel();
            ParseUtil.parseObject(poWorkerEntity, loWorkerModel);
            loWorkerModel.setLstDistrict(goDistrictModelToMapper.transform(poWorkerEntity.getLstDistrict()));
            loWorkerModel.setLstCategory(goCategoryModelToMapper.transform(poWorkerEntity.getLstCategory()));
            loWorkerModel.setLstQualify(goQualifyModelToMapper.transform(poWorkerEntity.getLstQualify()));
        }
        return loWorkerModel;
    }

    public List<WorkerModel> transform(List<WorkerEntity> poWorkerEntity) {

        List<WorkerModel> lasWorkerModel = new ArrayList<>();
        if (poWorkerEntity != null) {
            for (WorkerEntity loWorkerEntity : poWorkerEntity) {
                WorkerModel loWorkerModel = transform(loWorkerEntity);
                lasWorkerModel.add(loWorkerModel);
            }
        }
        return lasWorkerModel;
    }
}
