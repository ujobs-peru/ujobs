package com.ujobs.app.data.net;

import android.content.Context;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ujobs.app.R;
import com.ujobs.app.data.util.SSLUtil;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Hernan Pareja on 5/01/2017.
 */
public class RestBase {
    private static final String TAG = RestBase.class.getSimpleName();
    Context ioContext;
    RestApi ioRestApi;

    /**
     * Constructs a {@link RestBase}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public RestBase(Context poContext) {
        this.ioContext = poContext;
        this.ioRestApi = createRestApi();
    }

    public Context getContext() {
        return ioContext;
    }

    public RestApi getRestApi() {
        return ioRestApi;
    }

    /**
     * Create an implementation of the API endpoints defined by the {@code service} interface.
     *
     * @return Instance retrofit when invoke to diferents services {@link RestApi}
     * @author Manuel Torres
     * @version 1.0
     * @since 09/01/2017
     */
    private RestApi createRestApi() {
        if (ioRestApi == null) {
            String lsUrlServer = String.format("%s%s", getProtocol(), getHost());

            OkHttpClient.Builder loBuilder = new OkHttpClient.Builder();
            loBuilder.connectTimeout(getConnetTimeout(), TimeUnit.SECONDS);
            loBuilder.readTimeout(getReadTimeout(), TimeUnit.SECONDS);


            // this dispatcher will ensure networking is done on the main thread
            //loBuilder.dispatcher(new Dispatcher(new SynchronousExecutor())); //Only for test

            HttpLoggingInterceptor loLogging = new HttpLoggingInterceptor();
            loLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
            loBuilder.addInterceptor(loLogging);

            try {
                final SSLSocketFactory sslSocketFactory = SSLUtil.getSSLConfig(getContext()).getSocketFactory();
                loBuilder.sslSocketFactory(sslSocketFactory);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

            loBuilder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });


            OkHttpClient loClient = loBuilder.build();
            ObjectMapper loObjectMapper = new ObjectMapper()
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                    .enable(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS);
            Retrofit loRetrofit = new Retrofit.Builder()
                    .baseUrl(lsUrlServer)
                    .client(loClient)
                    .addConverterFactory(JacksonConverterFactory.create(loObjectMapper))
                    .build();
            ioRestApi = loRetrofit.create(RestApi.class);
        }
        return ioRestApi;
    }

    protected String getHost() {
        return ioContext.getString(R.string.host);
    }

    protected String getProtocol() {
        return ioContext.getString(R.string.protocol);
    }

    protected int getConnetTimeout() {
        return ioContext.getResources().getInteger(R.integer.connect_timeout);
    }

    protected int getReadTimeout() {
        return ioContext.getResources().getInteger(R.integer.read_timeout);
    }
}
