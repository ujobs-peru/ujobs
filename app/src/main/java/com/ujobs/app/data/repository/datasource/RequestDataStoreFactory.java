package com.ujobs.app.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Factory that creates different implementations of {@link RequestDataStore}.
 * <p>
 * Created by Hernan Pareja on 7/02/2017.
 */
public class RequestDataStoreFactory {

    private static final String TAG = RequestDataStoreFactory.class.getSimpleName();

    private final Context goContext;
    private static RequestDataStoreFactory INSTANCE;

    public static RequestDataStoreFactory newInstance(Context poContext) {
        if (INSTANCE == null) {
            INSTANCE = new RequestDataStoreFactory(poContext);
        }
        return INSTANCE;
    }

    /**
     * Construct a {@link RequestDataStoreFactory} that creates different implementations.
     *
     * @param poContext The {@link Context} to application.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public RequestDataStoreFactory(@NonNull Context poContext) {
        this.goContext = poContext;
    }

    /**
     * Create {@link RequestDataStore} to retrieve data from the Cloud.
     *
     * @return {@link RequestCloudDataStore} of the Cloud.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public RequestDataStore createCloud() {
        return new RequestCloudDataStore(goContext);
    }

}
