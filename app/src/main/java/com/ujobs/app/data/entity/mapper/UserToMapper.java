package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.UserModel;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class UserToMapper {


    private static UserToMapper INSTANCE;
    private final DistrictToMapper goDistrictModelToMapper;
    private final CategoryToMapper goCategoryModelToMapper;
    private final QualifyToMapper goQualifyToMapper;

    public static UserToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserToMapper();
        }
        return INSTANCE;
    }

    public UserToMapper() {
        goDistrictModelToMapper = DistrictToMapper.newInstance();
        goCategoryModelToMapper = CategoryToMapper.newInstance();
        goQualifyToMapper = QualifyToMapper.newInstance();
    }


    public UserEntity transform(UserModel poUserModel) {

        UserEntity loUserEntity = null;
        if (poUserModel != null) {
            loUserEntity = new UserEntity();
            ParseUtil.parseObject(poUserModel, loUserEntity);
            loUserEntity.setLstCategory(goCategoryModelToMapper.transform(poUserModel.getLstCategory()));
            loUserEntity.setLstDistrict(goDistrictModelToMapper.transform(poUserModel.getLstDistrict()));
            loUserEntity.setLstQualify(goQualifyToMapper.transform(poUserModel.getLstQualify()));
        }
        return loUserEntity;
    }
}
