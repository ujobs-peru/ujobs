package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.SubcategoryEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.SubcategoryModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class SubcategoryModelToMapper {


    private static SubcategoryModelToMapper INSTANCE;

    public static SubcategoryModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SubcategoryModelToMapper();
        }
        return INSTANCE;
    }


    public SubcategoryModel transform(SubcategoryEntity poSubcategoryEntity) {

        SubcategoryModel loSubcategoryModel = null;
        if (poSubcategoryEntity != null) {
            loSubcategoryModel = new SubcategoryModel();
            ParseUtil.parseObject(poSubcategoryEntity, loSubcategoryModel);
        }
        return loSubcategoryModel;
    }

    public List<SubcategoryModel> transform(List<SubcategoryEntity> poSubcategoryEntity) {

        List<SubcategoryModel> lasSubcategoryModel = new ArrayList<>();
        if (poSubcategoryEntity != null) {
            for (SubcategoryEntity loSubcategoryEntity : poSubcategoryEntity) {
                SubcategoryModel loSubcategoryModel = transform(loSubcategoryEntity);
                lasSubcategoryModel.add(loSubcategoryModel);
            }
        }
        return lasSubcategoryModel;
    }
}
