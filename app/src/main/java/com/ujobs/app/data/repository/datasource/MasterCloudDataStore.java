package com.ujobs.app.data.repository.datasource;

import android.content.Context;

import com.ujobs.app.R;
import com.ujobs.app.data.entity.BaseResponseEntity;
import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.exception.BaseException;
import com.ujobs.app.data.net.RestBase;
import com.ujobs.app.data.net.RestCallback;
import com.ujobs.app.data.net.RestReceptor;
import com.ujobs.app.data.util.LogUtil;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class MasterCloudDataStore extends RestBase implements MasterDataStore {

    private static final String TAG = MasterCloudDataStore.class.getSimpleName();

    /**
     * Constructs a {@link RestBase}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public MasterCloudDataStore(Context poContext) {
        super(poContext);
    }


    @Override
    public void listCatAndSubcategories(final CallbackDataStore<List<CategoryEntity>> poCallback) {
        LogUtil.i(TAG, "INICIO - listCatAndSubcategories");
        String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_find);
        String lsEndpoint = getContext().getString(R.string.endpoint_list_cat_subcat);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<List<CategoryEntity>>> loCall =
                getRestApi().listCatAndSubcat(lsUrl);

        RestReceptor<List<CategoryEntity>> loRestReceptor = new RestReceptor<List<CategoryEntity>>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<List<CategoryEntity>>() {
            @Override
            public void onResponse(List<CategoryEntity> poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - listCatAndSubcategories: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - listCatAndSubcategories: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void listDistrict(final CallbackDataStore<List<DistrictEntity>> poCallback) {
        LogUtil.i(TAG, "INICIO - listDistrict");
        String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_find);
        String lsEndpoint = getContext().getString(R.string.endpoint_list_district);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<List<DistrictEntity>>> loCall =
                getRestApi().listDistrict(lsUrl);

        RestReceptor<List<DistrictEntity>> loRestReceptor = new RestReceptor<List<DistrictEntity>>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<List<DistrictEntity>>() {
            @Override
            public void onResponse(List<DistrictEntity> poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - listDistrict: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - listDistrict: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void uploadFile(final CallbackDataStore<String> poCallback, File poFile, String psType, int piId) {
        LogUtil.i(TAG, "INICIO - uploadFile");
        String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_resource);
        String lsEndpoint = getContext().getString(R.string.endpoint_upload_file);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        RequestBody loBodyFile = RequestBody.create(MediaType.parse("image/*"), poFile);
        RequestBody loBodyType = RequestBody.create(MediaType.parse("text/plain"), psType);
        RequestBody loBodyId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(piId));

        Call<BaseResponseEntity<String>> loCall =
                getRestApi().uploadFile(lsUrl, loBodyFile, loBodyType, loBodyId);

        RestReceptor<String> loRestReceptor = new RestReceptor<String>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<String>() {
            @Override
            public void onResponse(String poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - uploadFile: onResponse");
                poCallback.onSuccess(getContext().getString(R.string.rest_message_register_success));
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - uploadFile: onError");
                poCallback.onError(poException);
            }
        });
    }
}
