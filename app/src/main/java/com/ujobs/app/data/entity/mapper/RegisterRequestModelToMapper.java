package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.RegisterRequestModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RegisterRequestModelToMapper {


    private static RegisterRequestModelToMapper INSTANCE;

    public static RegisterRequestModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RegisterRequestModelToMapper();
        }
        return INSTANCE;
    }


    public RegisterRequestModel transform(RegisterRequestEntity poRegisterRequestEntity) {

        RegisterRequestModel loRegisterRequestModel = null;
        if (poRegisterRequestEntity != null) {
            loRegisterRequestModel = new RegisterRequestModel();
            ParseUtil.parseObject(poRegisterRequestEntity, loRegisterRequestModel);
        }
        return loRegisterRequestModel;
    }

    public List<RegisterRequestModel> transform(List<RegisterRequestEntity> poRegisterRequestEntity) {

        List<RegisterRequestModel> lasRegisterRequestModel = new ArrayList<>();
        if (poRegisterRequestEntity != null) {
            for (RegisterRequestEntity loRegisterRequestEntity : poRegisterRequestEntity) {
                RegisterRequestModel loRegisterRequestModel = transform(loRegisterRequestEntity);
                lasRegisterRequestModel.add(loRegisterRequestModel);
            }
        }
        return lasRegisterRequestModel;
    }
}
