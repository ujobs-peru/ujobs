package com.ujobs.app.data.util;

import java.lang.reflect.Field;

/**
 * Created by Manuel Torres on 26/01/2017.
 */

public class ParseUtil {


    public static void parseObject(Object origen, Object destino) {

        try {
            for (Field field : origen.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(origen) != null) {
                    // Field fieldDestino =
                    // destino.getClass().getDeclaredField(f.getName());
                    try {
                        Field fieldDestino = destino.getClass()
                                .getDeclaredField(field.getName());
                        fieldDestino.setAccessible(true);

                        Object dataOrigen = field.get(origen);
                        if (fieldDestino.getType().equals(field.getType())) {
                            dataOrigen = dataOrigen;
                        } else if (fieldDestino.getType().equals(String.class)) {
                            dataOrigen = String.valueOf(dataOrigen);
                        } else if (fieldDestino.getType().equals(int.class)) {
                            dataOrigen = Integer.parseInt(dataOrigen.toString());
                        } else if (fieldDestino.getType().equals(double.class)) {
                            dataOrigen = Double.parseDouble(dataOrigen.toString());
                        }
                        fieldDestino.set(destino, dataOrigen);
                    } catch (Exception e) {
                        // log.debug("[Funciones]/parseObject","No se ha
                        // encontrado variable '"+f.getName()+"' de Objeto
                        // origen:"+origen.getClass()+" -> En objeto
                        // destino:"+destino.getClass(), "1");
                    }
                    // where.append(f.getName() + " LIKE '%" +
                    // f.get(xrespuesta).toString().toUpperCase() + "%' AND ");
                }
            }
        } catch (Exception e) {
            // log.debug("[Funciones]/parseObject","No es un objeto con
            // variables declaradas.", "1");
        }

        try {
            if (!(origen.getClass().getSuperclass().getName().equals("java.lang.Object"))) {
                for (Field field : origen.getClass().getSuperclass().getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.get(origen) != null) {
                        // Field fieldDestino =
                        // destino.getClass().getDeclaredField(f.getName());
                        try {
                            Field fieldDestino = destino.getClass().getSuperclass()
                                    .getDeclaredField(field.getName());
                            fieldDestino.setAccessible(true);

                            Object dataOrigen = field.get(origen);
                            if (fieldDestino.getType().equals(field.getType())) {
                                dataOrigen = dataOrigen;
                            } else if (fieldDestino.getType().equals(String.class)) {
                                dataOrigen = String.valueOf(dataOrigen);
                            } else if (fieldDestino.getType().equals(int.class)) {
                                dataOrigen = Integer.parseInt(dataOrigen.toString());
                            } else if (fieldDestino.getType().equals(double.class)) {
                                dataOrigen = Double.parseDouble(dataOrigen.toString());
                            }
                            fieldDestino.set(destino, dataOrigen);
                        } catch (Exception e) {
                            // log.debug("[Funciones]/parseObject","No se ha
                            // encontrado variable '"+f.getName()+"' de Objeto
                            // origen:"+origen.getClass()+" -> En objeto
                            // destino:"+destino.getClass(), "1");
                        }
                        // where.append(f.getName() + " LIKE '%" +
                        // f.get(xrespuesta).toString().toUpperCase() + "%' AND ");
                    }
                }
            }
        } catch (Exception e) {
            // log.debug("[Funciones]/parseObject","No es un objeto con
            // variables declaradas.", "1");
        }

//        return destino;
    }


}
