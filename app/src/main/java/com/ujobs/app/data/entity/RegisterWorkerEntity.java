package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterWorkerEntity {

    @JsonProperty("idTrabajador")
    private int idWorker;
    @JsonProperty("lsCatSubcategoria")
    private List<Integer> listIdsCatAndSubcat;
    @JsonProperty("lsDistritos")
    private List<Integer> listIdsDistrict;

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public List<Integer> getListIdsCatAndSubcat() {
        return listIdsCatAndSubcat;
    }

    public void setListIdsCatAndSubcat(List<Integer> listIdsCatAndSubcat) {
        this.listIdsCatAndSubcat = listIdsCatAndSubcat;
    }

    public List<Integer> getListIdsDistrict() {
        return listIdsDistrict;
    }

    public void setListIdsDistrict(List<Integer> listIdsDistrict) {
        this.listIdsDistrict = listIdsDistrict;
    }
}
