package com.ujobs.app.data.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

/**
 * Created by bn on 13/11/17.
 */

public class JsonUtil {

    private static final String TAG = JsonUtil.class.getSimpleName();
    private static final ObjectMapper goMapper = new ObjectMapper();

    public static <T> T getJsonToString(String psData, Class<T> valueType) {

        try {
            return goMapper.readValue(psData, valueType);
        } catch (IOException e) {
            LogUtil.e(TAG, "getJsonToString", e);
            return null;
        }
    }

    public static String getStringToJson(Object poObject) {

        try {
            String jsonInString = goMapper.writeValueAsString(poObject);
            return jsonInString;
        } catch (IOException e) {
            LogUtil.e(TAG, "getJsonToString", e);
            return null;
        }
    }
}