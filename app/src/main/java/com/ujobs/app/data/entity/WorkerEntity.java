package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bn on 14/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkerEntity extends UserEntity {

    @JsonProperty("idTrabajador")
    public int idWorker;


    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

}
