package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bn on 27/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterQualifyEntity {

    @JsonProperty("idRequerimiento")
    private Integer idRequest;
    @JsonProperty("nota")
    private Integer stars;
    @JsonProperty("comentario")
    private String comment;

    public Integer getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(Integer idRequest) {
        this.idRequest = idRequest;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
