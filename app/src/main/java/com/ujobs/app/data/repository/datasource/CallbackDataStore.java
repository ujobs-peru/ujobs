package com.ujobs.app.data.repository.datasource;

/**
 * Created by Manuel Torres on 16/01/2017.
 * <p>
 * Interface that represents callback to DataStore
 */
public interface CallbackDataStore<T> {

    void onSuccess(T poData);

    void onError(Throwable poException);

    
}
