package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.QuoteModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class QuoteModelToMapper {


    private static QuoteModelToMapper INSTANCE;

    public static QuoteModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new QuoteModelToMapper();
        }
        return INSTANCE;
    }


    private final WorkerModelToMapper goWorkerModelToMapper;

    public QuoteModelToMapper() {
        goWorkerModelToMapper = WorkerModelToMapper.newInstance();
    }

    public QuoteModel transform(QuoteEntity poQuoteEntity) {

        QuoteModel loQuoteModel = null;
        if (poQuoteEntity != null) {
            loQuoteModel = new QuoteModel();
            ParseUtil.parseObject(poQuoteEntity, loQuoteModel);
            loQuoteModel.setWorker(goWorkerModelToMapper.transform(poQuoteEntity.getWorker()));
        }
        return loQuoteModel;
    }

    public List<QuoteModel> transform(List<QuoteEntity> poQuoteEntity) {

        List<QuoteModel> lasQuoteModel = new ArrayList<>();
        if (poQuoteEntity != null) {
            for (QuoteEntity loQuoteEntity : poQuoteEntity) {
                QuoteModel loQuoteModel = transform(loQuoteEntity);
                lasQuoteModel.add(loQuoteModel);
            }
        }
        return lasQuoteModel;
    }
}
