package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubcategoryEntity {

    @JsonProperty("idCatSubcategoria")
    private int idCatSubcategory;
    @JsonProperty("idSubcategoria")
    private int idSubcategory;
    @JsonProperty("nombreSubcategoria")
    private String name;

    public int getIdCatSubcategory() {
        return idCatSubcategory;
    }

    public void setIdCatSubcategory(int idCatSubcategory) {
        this.idCatSubcategory = idCatSubcategory;
    }

    public int getIdSubcategory() {
        return idSubcategory;
    }

    public void setIdSubcategory(int idSubcategory) {
        this.idSubcategory = idSubcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
