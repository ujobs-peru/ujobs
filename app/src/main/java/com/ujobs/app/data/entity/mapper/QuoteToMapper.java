package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.QuoteModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class QuoteToMapper {


    private static QuoteToMapper INSTANCE;

    public static QuoteToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new QuoteToMapper();
        }
        return INSTANCE;
    }

    private final WorkerToMapper goWorkerToMapper;

    public QuoteToMapper() {
        goWorkerToMapper = WorkerToMapper.newInstance();
    }


    public QuoteEntity transform(QuoteModel poQuoteModel) {

        QuoteEntity loQuoteEntity = null;
        if (poQuoteModel != null) {
            loQuoteEntity = new QuoteEntity();
            ParseUtil.parseObject(poQuoteModel, loQuoteEntity);
            loQuoteEntity.setWorker(goWorkerToMapper.transform(poQuoteModel.getWorker()));
        }
        return loQuoteEntity;
    }

    public List<QuoteEntity> transform(List<QuoteModel> poQuoteModel) {

        List<QuoteEntity> lasQuoteEntity = new ArrayList<>();
        if (poQuoteModel != null) {
            for (QuoteModel loQuoteModel : poQuoteModel) {
                QuoteEntity loQuoteEntity = transform(loQuoteModel);
                lasQuoteEntity.add(loQuoteEntity);
            }
        }
        return lasQuoteEntity;
    }
}
