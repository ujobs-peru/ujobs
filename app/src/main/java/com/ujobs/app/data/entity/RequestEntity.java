package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ujobs.app.model.CategoryModel;

import java.util.List;

/**
 * Created by bn on 13/11/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestEntity {


    @JsonProperty("comentario")
    private String description;
    @JsonProperty("fechaFin")
    private String endDate;
    @JsonProperty("fechaIni")
    private String startDate;
    @JsonProperty("idCotizacion")
    private int idCotizacion;
    @JsonProperty("idDistrito")
    private int idDistrict;
    @JsonProperty("nombreDistrito")
    private String districtName;
    @JsonProperty("idRequerimiento")
    private int idRequest;
    @JsonProperty("idTipoEstado")
    private int idStateType;
    @JsonProperty("idUsuario")
    private int idUser;
    @JsonProperty("nombreRequerimiento")
    private String name;
    @JsonProperty("cantCotizaciones")
    private int countQuotes;
    @JsonProperty("lsCategorias")
    private List<CategoryEntity> lstCategory;
    @JsonProperty("lsImagenes")
    private List<Integer> lstImage;
    @JsonProperty("usuario")
    private UserEntity user;
    @JsonProperty("cantValoraciones")
    private int countQualify;

    public int getCountQualify() {
        return countQualify;
    }

    public void setCountQualify(int countQualify) {
        this.countQualify = countQualify;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(int idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }

    public int getIdStateType() {
        return idStateType;
    }

    public void setIdStateType(int idStateType) {
        this.idStateType = idStateType;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountQuotes() {
        return countQuotes;
    }

    public void setCountQuotes(int countQuotes) {
        this.countQuotes = countQuotes;
    }

    public List<CategoryEntity> getLstCategory() {
        return lstCategory;
    }

    public void setLstCategory(List<CategoryEntity> lstCategory) {
        this.lstCategory = lstCategory;
    }

    public List<Integer> getLstImage() {
        return lstImage;
    }

    public void setLstImage(List<Integer> lstImage) {
        this.lstImage = lstImage;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
