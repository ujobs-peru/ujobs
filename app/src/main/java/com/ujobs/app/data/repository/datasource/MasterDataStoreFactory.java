package com.ujobs.app.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Factory that creates different implementations of {@link MasterDataStore}.
 * <p>
 * Created by Hernan Pareja on 7/02/2017.
 */
public class MasterDataStoreFactory {

    private static final String TAG = MasterDataStoreFactory.class.getSimpleName();

    private final Context goContext;
    private static MasterDataStoreFactory INSTANCE;

    public static MasterDataStoreFactory newInstance(Context poContext) {
        if (INSTANCE == null) {
            INSTANCE = new MasterDataStoreFactory(poContext);
        }
        return INSTANCE;
    }

    /**
     * Construct a {@link MasterDataStoreFactory} that creates different implementations.
     *
     * @param poContext The {@link Context} to application.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public MasterDataStoreFactory(@NonNull Context poContext) {
        this.goContext = poContext;
    }

    /**
     * Create {@link MasterDataStore} to retrieve data from the Cloud.
     *
     * @return {@link MasterCloudDataStore} of the Cloud.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public MasterDataStore createCloud() {
        return new MasterCloudDataStore(goContext);
    }

}
