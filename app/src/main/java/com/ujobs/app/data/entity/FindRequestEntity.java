package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bn on 13/11/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FindRequestEntity {

    @JsonProperty("idTrabajador")
    private int idWorker;
    @JsonProperty("idUsuario")
    private int idUser;
    @JsonProperty("idTipoEstadoReq")
    private int idStateRequestType;

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdStateRequestType() {
        return idStateRequestType;
    }

    public void setIdStateRequestType(int idStateRequestType) {
        this.idStateRequestType = idStateRequestType;
    }
}
