package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.SubcategoryEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.SubcategoryModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class SubcategoryToMapper {


    private static SubcategoryToMapper INSTANCE;

    public static SubcategoryToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SubcategoryToMapper();
        }
        return INSTANCE;
    }


    public SubcategoryEntity transform(SubcategoryModel poSubcategoryModel) {

        SubcategoryEntity loSubcategoryEntity = null;
        if (poSubcategoryModel != null) {
            loSubcategoryEntity = new SubcategoryEntity();
            ParseUtil.parseObject(poSubcategoryModel, loSubcategoryEntity);
        }
        return loSubcategoryEntity;
    }

    public List<SubcategoryEntity> transform(List<SubcategoryModel> poSubcategoryModel) {

        List<SubcategoryEntity> lasSubcategoryEntity = new ArrayList<>();
        if (poSubcategoryModel != null) {
            for (SubcategoryModel loSubcategoryModel : poSubcategoryModel) {
                SubcategoryEntity loSubcategoryEntity = transform(loSubcategoryModel);
                lasSubcategoryEntity.add(loSubcategoryEntity);
            }
        }
        return lasSubcategoryEntity;
    }
}
