package com.ujobs.app.data.util;

import android.util.Log;

import com.ujobs.app.BuildConfig;


/**
 * Created by Hernan Pareja on 5/01/2017.
 */

public final class LogUtil {

    private static final String TAG = LogUtil.class.getSimpleName();

    public static void v(String psTag, String psMessage) {
        if (BuildConfig.DEBUG) {
            Log.v(psTag, psMessage);
        }
    }

    public static void d(String psTag, String psMessage) {
        if (BuildConfig.DEBUG) {
            Log.d(psTag, psMessage);
        }
    }

    public static void i(String psTag, String psMessage) {
        if (BuildConfig.DEBUG) {
            Log.i(psTag, psMessage);
        }
    }

    public static void w(String psTag, String psMessage) {
        if (BuildConfig.DEBUG) {
            Log.w(psTag, psMessage);
        }
    }

    public static void e(String psTag, String psMessage) {
        if (BuildConfig.DEBUG) {
            Log.e(psTag, psMessage);
        }
    }

    public static void e(String psTag, String psMessage, Throwable poException) {
        if (BuildConfig.DEBUG) {
            Log.e(psTag, psMessage, poException);
        }
    }
}
