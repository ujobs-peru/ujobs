package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.QualifyEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.QualifyModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class QualifyModelToMapper {


    private static QualifyModelToMapper INSTANCE;

    public static QualifyModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new QualifyModelToMapper();
        }
        return INSTANCE;
    }
    
    public QualifyModelToMapper() {
    }

    public QualifyModel transform(QualifyEntity poQualifyEntity) {

        QualifyModel loQualifyModel = null;
        if (poQualifyEntity != null) {
            loQualifyModel = new QualifyModel();
            ParseUtil.parseObject(poQualifyEntity, loQualifyModel);
        }
        return loQualifyModel;
    }

    public List<QualifyModel> transform(List<QualifyEntity> poQualifyEntity) {

        List<QualifyModel> lasQualifyModel = new ArrayList<>();
        if (poQualifyEntity != null) {
            for (QualifyEntity loQualifyEntity : poQualifyEntity) {
                QualifyModel loQualifyModel = transform(loQualifyEntity);
                lasQualifyModel.add(loQualifyModel);
            }
        }
        return lasQualifyModel;
    }
}
