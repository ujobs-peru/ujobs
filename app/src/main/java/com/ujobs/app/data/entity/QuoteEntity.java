package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bn on 14/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteEntity {


    @JsonProperty("detalle")
    public String detail;
    @JsonProperty("idCotizacion")
    public int idQuote;
    @JsonProperty("idTrabajador")
    public int idWorker;
    @JsonProperty("importe")
    public double amount;
    @JsonProperty("lsImagenes")
    public List<Integer> lstIdsImage;
    @JsonProperty("trabajador")
    public WorkerEntity worker;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getIdQuote() {
        return idQuote;
    }

    public void setIdQuote(int idQuote) {
        this.idQuote = idQuote;
    }

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<Integer> getLstIdsImage() {
        return lstIdsImage;
    }

    public void setLstIdsImage(List<Integer> lstIdsImage) {
        this.lstIdsImage = lstIdsImage;
    }

    public WorkerEntity getWorker() {
        return worker;
    }

    public void setWorker(WorkerEntity worker) {
        this.worker = worker;
    }
}
