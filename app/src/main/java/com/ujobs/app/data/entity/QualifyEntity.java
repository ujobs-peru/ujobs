package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bn on 14/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualifyEntity extends UserEntity {

    @JsonProperty("idCalifacion")
    private Integer idQualify;
    @JsonProperty("nota")
    private Integer stars;
    @JsonProperty("comentario")
    private String comment;

    public Integer getIdQualify() {
        return idQualify;
    }

    public void setIdQualify(Integer idQualify) {
        this.idQualify = idQualify;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
