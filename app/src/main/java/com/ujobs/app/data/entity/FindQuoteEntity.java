package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bn on 13/11/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FindQuoteEntity {


    @JsonProperty("idRequerimiento")
    private int idRequest;

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }
}
