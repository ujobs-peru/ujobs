package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.UserModel;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class UserModelToMapper {


    private static UserModelToMapper INSTANCE;
    private final DistrictModelToMapper goDistrictModelToMapper;
    private final CategoryModelToMapper goCategoryModelToMapper;
    private final QualifyModelToMapper goQualifyModelToMapper;

    public static UserModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserModelToMapper();
        }
        return INSTANCE;
    }

    public UserModelToMapper() {
        goDistrictModelToMapper = new DistrictModelToMapper();
        goCategoryModelToMapper = new CategoryModelToMapper();
        goQualifyModelToMapper = QualifyModelToMapper.newInstance();
    }

    public UserModel transform(UserEntity poUserEntity) {

        UserModel loUserModel = null;
        if (poUserEntity != null) {
            loUserModel = new UserModel();
            ParseUtil.parseObject(poUserEntity, loUserModel);
            loUserModel.setLstDistrict(goDistrictModelToMapper.transform(poUserEntity.getLstDistrict()));
            loUserModel.setLstCategory(goCategoryModelToMapper.transform(poUserEntity.getLstCategory()));
            loUserModel.setLstQualify(goQualifyModelToMapper.transform(poUserEntity.getLstQualify()));
        }
        return loUserModel;
    }
}
