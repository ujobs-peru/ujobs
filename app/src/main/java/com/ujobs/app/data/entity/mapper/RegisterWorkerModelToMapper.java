package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.RegisterWorkerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RegisterWorkerModelToMapper {


    private static RegisterWorkerModelToMapper INSTANCE;

    public static RegisterWorkerModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RegisterWorkerModelToMapper();
        }
        return INSTANCE;
    }


    public RegisterWorkerModel transform(RegisterWorkerEntity poRegisterWorkerEntity) {

        RegisterWorkerModel loRegisterWorkerModel = null;
        if (poRegisterWorkerEntity != null) {
            loRegisterWorkerModel = new RegisterWorkerModel();
            ParseUtil.parseObject(poRegisterWorkerEntity, loRegisterWorkerModel);
        }
        return loRegisterWorkerModel;
    }

    public List<RegisterWorkerModel> transform(List<RegisterWorkerEntity> poRegisterWorkerEntity) {

        List<RegisterWorkerModel> lasRegisterWorkerModel = new ArrayList<>();
        if (poRegisterWorkerEntity != null) {
            for (RegisterWorkerEntity loRegisterWorkerEntity : poRegisterWorkerEntity) {
                RegisterWorkerModel loRegisterWorkerModel = transform(loRegisterWorkerEntity);
                lasRegisterWorkerModel.add(loRegisterWorkerModel);
            }
        }
        return lasRegisterWorkerModel;
    }
}
