package com.ujobs.app.data.repository;

import android.content.Context;

import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.UpdateTypeRequestEntity;
import com.ujobs.app.data.repository.datasource.CallbackDataStore;
import com.ujobs.app.data.repository.datasource.RequestDataStore;
import com.ujobs.app.data.repository.datasource.RequestDataStoreFactory;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RequestDataRepository implements RequestRepository {

    private final RequestDataStoreFactory goRequestDataStoreFactory;

    public static RequestDataRepository newInstance(Context poContext) {
        return new RequestDataRepository(poContext);
    }

    public RequestDataRepository(Context poContext) {
        this.goRequestDataStoreFactory = RequestDataStoreFactory.newInstance(poContext);
    }

    @Override
    public void registerRequest(RegisterRequestEntity poRegisterRequestEntity, final DomainCallback<Integer> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.registerRequest(new CallbackDataStore<Integer>() {
            @Override
            public void onSuccess(Integer psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poRegisterRequestEntity);
    }

    @Override
    public void listRequestClient(FindRequestEntity poFindRequestEntity, final DomainCallback<List<RequestEntity>> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.listRequestClient(new CallbackDataStore<List<RequestEntity>>() {
            @Override
            public void onSuccess(List<RequestEntity> psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poFindRequestEntity);
    }

    @Override
    public void listRequestWorker(FindRequestEntity poFindRequestEntity, final DomainCallback<List<RequestEntity>> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.listRequestWorker(new CallbackDataStore<List<RequestEntity>>() {
            @Override
            public void onSuccess(List<RequestEntity> psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poFindRequestEntity);
    }

    @Override
    public void listQuote(FindQuoteEntity poFindRequestEntity, final DomainCallback<List<QuoteEntity>> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.listQuote(new CallbackDataStore<List<QuoteEntity>>() {
            @Override
            public void onSuccess(List<QuoteEntity> psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poFindRequestEntity);
    }

    @Override
    public void updateTypeRequest(UpdateTypeRequestEntity poUpdateTypeRequestEntity, final DomainCallback<String> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.updateTypeRequest(new CallbackDataStore<String>() {
            @Override
            public void onSuccess(String psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poUpdateTypeRequestEntity);
    }

    @Override
    public void registerQuote(RegisterQuoteEntity poRegisterQuoteEntity, final DomainCallback<Integer> poDomainCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.registerQuote(new CallbackDataStore<Integer>() {
            @Override
            public void onSuccess(Integer psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poRegisterQuoteEntity);
    }

    @Override
    public void registerQualify(RegisterQualifyEntity poRequest, boolean isClient, final DomainCallback<Integer> poCallback) {
        final RequestDataStore loRequestDataStore = goRequestDataStoreFactory.createCloud();
        loRequestDataStore.registerQualify(new CallbackDataStore<Integer>() {
            @Override
            public void onSuccess(Integer psData) {
                poCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poCallback.onError(poException);
            }
        }, poRequest, isClient);
    }
}
