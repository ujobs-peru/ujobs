package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class CategoryToMapper {


    private static CategoryToMapper INSTANCE;

    public static CategoryToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryToMapper();
        }
        return INSTANCE;
    }

    public CategoryToMapper() {
        goSubcategoryToMapper = SubcategoryToMapper.newInstance();
    }

    private SubcategoryToMapper goSubcategoryToMapper;

    public CategoryEntity transform(CategoryModel poCategoryModel) {

        CategoryEntity loCategoryEntity = null;
        if (poCategoryModel != null) {
            loCategoryEntity = new CategoryEntity();
            ParseUtil.parseObject(poCategoryModel, loCategoryEntity);
            loCategoryEntity.setListSubcategories(goSubcategoryToMapper.transform(poCategoryModel.getListSubcategories()));
        }
        return loCategoryEntity;
    }

    public List<CategoryEntity> transform(List<CategoryModel> poCategoryModel) {

        List<CategoryEntity> lasCategoryEntity = new ArrayList<>();
        if (poCategoryModel != null) {
            for (CategoryModel loCategoryModel : poCategoryModel) {
                CategoryEntity loCategoryEntity = transform(loCategoryModel);
                lasCategoryEntity.add(loCategoryEntity);
            }
        }
        return lasCategoryEntity;
    }
}
