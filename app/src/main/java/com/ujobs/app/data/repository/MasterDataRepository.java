package com.ujobs.app.data.repository;

import android.content.Context;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.repository.datasource.CallbackDataStore;
import com.ujobs.app.data.repository.datasource.MasterDataStore;
import com.ujobs.app.data.repository.datasource.MasterDataStoreFactory;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.MasterRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class MasterDataRepository implements MasterRepository {

    private final MasterDataStoreFactory goMasterDataStoreFactory;

    public static MasterDataRepository newInstance(Context poContext) {
        return new MasterDataRepository(poContext);
    }

    public MasterDataRepository(Context poContext) {
        this.goMasterDataStoreFactory = MasterDataStoreFactory.newInstance(poContext);
    }


    @Override
    public void listCatAndSubcategories(final DomainCallback<List<CategoryEntity>> poDomainCallback) {
        final MasterDataStore loMasterDataStore = goMasterDataStoreFactory.createCloud();
        loMasterDataStore.listCatAndSubcategories(new CallbackDataStore<List<CategoryEntity>>() {
            @Override
            public void onSuccess(List<CategoryEntity> poData) {
                poDomainCallback.onSuccess(poData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        });
    }

    @Override
    public void listDistrict(final DomainCallback<List<DistrictEntity>> poDomainCallback) {
        final MasterDataStore loMasterDataStore = goMasterDataStoreFactory.createCloud();

        loMasterDataStore.listDistrict(new CallbackDataStore<List<DistrictEntity>>() {
            @Override
            public void onSuccess(List<DistrictEntity> poData) {
                poDomainCallback.onSuccess(poData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        });
    }

    @Override
    public void uploadFile(List<File> files, String psType, int piId, DomainCallback<String> poCallback) {
        uploadFile(files, psType, piId, poCallback, 0, 0);
    }

    public void uploadFile(final List<File> files, final String psType, final int piId, final DomainCallback<String> poCallback, final int count, final int piCountError) {

        final MasterDataStore loMasterDataStore = goMasterDataStoreFactory.createCloud();
        loMasterDataStore.uploadFile(new CallbackDataStore<String>() {
            @Override
            public void onSuccess(String poData) {
                if (count == files.size() - 1) {
                    poCallback.onSuccess(poData);
                } else {
                    uploadFile(files, psType, piId, poCallback, count + 1, 0);
                }

            }

            @Override
            public void onError(Throwable poException) {
                if (piCountError == 3) {
                    poCallback.onError(poException);
                } else {
                    uploadFile(files, psType, piId, poCallback, count, piCountError + 1);
                }

            }
        }, files.get(count), psType, piId);
    }
}
