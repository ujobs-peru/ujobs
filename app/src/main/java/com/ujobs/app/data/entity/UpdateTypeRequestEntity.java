package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Manuel Torres on 19/11/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateTypeRequestEntity {

    @JsonProperty("idRequerimiento")
    private int idRequest;
    @JsonProperty("idTipoEstado")
    private int idTypeRequest;
    @JsonProperty("idCotizacion")
    private int idQuote;

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }

    public int getIdTypeRequest() {
        return idTypeRequest;
    }

    public void setIdTypeRequest(int idTypeRequest) {
        this.idTypeRequest = idTypeRequest;
    }

    public int getIdQuote() {
        return idQuote;
    }

    public void setIdQuote(int idQuote) {
        this.idQuote = idQuote;
    }
}
