package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.RegisterWorkerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class RegisterWorkerToMapper {


    private static RegisterWorkerToMapper INSTANCE;

    public static RegisterWorkerToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RegisterWorkerToMapper();
        }
        return INSTANCE;
    }


    public RegisterWorkerEntity transform(RegisterWorkerModel poRegisterWorkerModel) {

        RegisterWorkerEntity loRegisterWorkerEntity = null;
        if (poRegisterWorkerModel != null) {
            loRegisterWorkerEntity = new RegisterWorkerEntity();
            ParseUtil.parseObject(poRegisterWorkerModel, loRegisterWorkerEntity);
        }
        return loRegisterWorkerEntity;
    }

    public List<RegisterWorkerEntity> transform(List<RegisterWorkerModel> poRegisterWorkerModel) {

        List<RegisterWorkerEntity> lasRegisterWorkerEntity = new ArrayList<>();
        if (poRegisterWorkerModel != null) {
            for (RegisterWorkerModel loRegisterWorkerModel : poRegisterWorkerModel) {
                RegisterWorkerEntity loRegisterWorkerEntity = transform(loRegisterWorkerModel);
                lasRegisterWorkerEntity.add(loRegisterWorkerEntity);
            }
        }
        return lasRegisterWorkerEntity;
    }
}
