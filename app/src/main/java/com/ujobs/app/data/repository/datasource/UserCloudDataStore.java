package com.ujobs.app.data.repository.datasource;

import android.content.Context;

import com.ujobs.app.R;
import com.ujobs.app.data.entity.BaseResponseEntity;
import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.exception.BaseException;
import com.ujobs.app.data.net.RestBase;
import com.ujobs.app.data.net.RestCallback;
import com.ujobs.app.data.net.RestReceptor;
import com.ujobs.app.data.util.LogUtil;

import retrofit2.Call;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UserCloudDataStore extends RestBase implements UserDataStore {

    private static final String TAG = UserCloudDataStore.class.getSimpleName();

    /**
     * Constructs a {@link RestBase}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public UserCloudDataStore(Context poContext) {
        super(poContext);
    }

    @Override
    public void login(final CallbackDataStore<UserEntity> poCallback, UserEntity poRequest) {
        LogUtil.i(TAG, "INICIO - login");
        String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_user);
        String lsEndpoint = getContext().getString(R.string.endpoint_login);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<UserEntity>> loCall =
                getRestApi().userLogin(lsUrl, poRequest);

        RestReceptor<UserEntity> loRestReceptor = new RestReceptor<UserEntity>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<UserEntity>() {
            @Override
            public void onResponse(UserEntity poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - login: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - login: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void signIn(final CallbackDataStore<Integer> poCallback, UserEntity poRequest) {
        LogUtil.i(TAG, "INICIO - signIn");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_user);
        String lsEndpoint = getContext().getString(R.string.endpoint_register);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<Integer>> loCall =
                getRestApi().userSignIn(lsUrl, poRequest);

        RestReceptor<Integer> loRestReceptor = new RestReceptor<Integer>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<Integer>() {
            @Override
            public void onResponse(Integer poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - signIn: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - login: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void registerWorker(final CallbackDataStore<String> poCallback, RegisterWorkerEntity poRequest) {
        LogUtil.i(TAG, "INICIO - registerWorker");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_worker);
        String lsEndpoint = getContext().getString(R.string.endpoint_register_worker);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<String>> loCall =
                getRestApi().registerWorker(lsUrl, poRequest);

        RestReceptor<String> loRestReceptor = new RestReceptor<String>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<String>() {
            @Override
            public void onResponse(String poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - registerWorker: onResponse");
                poCallback.onSuccess(getContext().getString(R.string.rest_message_register_success));
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - registerWorker: onError");
                poCallback.onError(poException);
            }
        });

    }

    @Override
    public void saveUser(UserEntity loUserEntity) {

    }

    @Override
    public UserEntity getUser() {
        return null;
    }
}
