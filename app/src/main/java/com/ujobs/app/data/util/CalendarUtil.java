package com.ujobs.app.data.util;

/**
 * Created by bn on 13/11/17.
 */


import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarUtil {

    private static final String TAG = CalendarUtil.class.getSimpleName();

    public static String getFormatCurrentDate(String format) {

        Calendar cal = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(cal.getTime());

    }

    public static String getFormatDate(Date date, String format) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(date);

    }

    public static Date getStringToDate(String date, String initFormat) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(initFormat);

            return dateFormat.parse(date);

        } catch (Exception e) {
            Log.e(TAG, "getStringToDate", e);
        }

        return null;

    }

    public static String getFormatDate(String date, String initFormat,
                                       String endFormat) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(initFormat);

            return getFormatDate(dateFormat.parse(date), endFormat);

        } catch (Exception e) {
            Log.e(TAG, "getFormatDate", e);
        }

        return date;

    }

    public static Date addDay(Date date, int day) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.DATE, day);

        return cal.getTime();

    }

    public static String addDayFormat(String date, int day, String format) {
        try {
            SimpleDateFormat formateador = new SimpleDateFormat(format);
            Date forDate = formateador.parse(date);

            return formateador.format(addDay(forDate, day));
        } catch (Exception e) {
            Log.e(TAG, "addDayDateFormat", e);
        }

        return date;
    }

    // >0 date 1 is after date 2
    // <0 date 1 is before date 2
    // =0 date 1 is iqual date 2
    public static int compareTo(String date1, String date2, String format) {

        try {
            SimpleDateFormat formateador = new SimpleDateFormat(format);
            Date forDate1 = formateador.parse(date1);
            Date forDate2 = formateador.parse(date2);
            return forDate1.compareTo(forDate2);
        } catch (Exception e) {
            Log.e(TAG, "compareTo", e);
        }

        return 0;
    }
}