package com.ujobs.app.data.repository.datasource;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ujobs.app.R;
import com.ujobs.app.data.entity.BaseResponseEntity;
import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.exception.BaseException;
import com.ujobs.app.data.net.RestBase;
import com.ujobs.app.data.net.RestCallback;
import com.ujobs.app.data.net.RestReceptor;
import com.ujobs.app.data.util.JsonUtil;
import com.ujobs.app.data.util.LogUtil;
import com.ujobs.app.data.util.SharedPreferencesUtil;

import retrofit2.Call;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UserSharedDataStore extends SharedPreferencesUtil implements UserDataStore {

    private static final String TAG = UserSharedDataStore.class.getSimpleName();

    private static final String SHARED_USER = "USER";

    /**
     * Constructs a {@link RestBase}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public UserSharedDataStore(Context poContext) {
        super(poContext);

    }


    @Override
    public void login(CallbackDataStore<UserEntity> poCallback, UserEntity poRequest) {

    }

    @Override
    public void signIn(CallbackDataStore<Integer> poCallback, UserEntity poRequest) {

    }

    @Override
    public void registerWorker(CallbackDataStore<String> poCallback, RegisterWorkerEntity poRequest) {

    }

    @Override
    public void saveUser(UserEntity loUserEntity) {
        putString(SHARED_USER, JsonUtil.getStringToJson(loUserEntity));

    }

    @Override
    public UserEntity getUser() {
        return JsonUtil.getJsonToString(getString(SHARED_USER), UserEntity.class);
    }
}
