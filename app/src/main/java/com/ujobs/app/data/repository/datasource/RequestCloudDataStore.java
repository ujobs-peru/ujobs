package com.ujobs.app.data.repository.datasource;

import android.content.Context;

import com.ujobs.app.R;
import com.ujobs.app.data.entity.BaseResponseEntity;
import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.UpdateTypeRequestEntity;
import com.ujobs.app.data.exception.BaseException;
import com.ujobs.app.data.net.RestBase;
import com.ujobs.app.data.net.RestCallback;
import com.ujobs.app.data.net.RestReceptor;
import com.ujobs.app.data.util.LogUtil;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RequestCloudDataStore extends RestBase implements RequestDataStore {

    private static final String TAG = RequestCloudDataStore.class.getSimpleName();

    /**
     * Constructs a {@link RestBase}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public RequestCloudDataStore(Context poContext) {
        super(poContext);
    }

    @Override
    public void registerRequest(final CallbackDataStore<Integer> poCallback, RegisterRequestEntity poRequest) {
        LogUtil.i(TAG, "INICIO - registerRequest");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_request);
        String lsEndpoint = getContext().getString(R.string.endpoint_register_request);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<Integer>> loCall =
                getRestApi().registerRequest(lsUrl, poRequest);

        RestReceptor<Integer> loRestReceptor = new RestReceptor<Integer>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<Integer>() {
            @Override
            public void onResponse(Integer poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - registerRequest: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - registerRequest: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void listRequestClient(final CallbackDataStore<List<RequestEntity>> poCallback, FindRequestEntity poRequest) {
        LogUtil.i(TAG, "INICIO - listRequestClient");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_user);
        String lsEndpoint = getContext().getString(R.string.endpoint_list_request);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<List<RequestEntity>>> loCall =
                getRestApi().listRequestClient(lsUrl, poRequest);

        RestReceptor<List<RequestEntity>> loRestReceptor = new RestReceptor<List<RequestEntity>>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<List<RequestEntity>>() {
            @Override
            public void onResponse(List<RequestEntity> poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - listRequestClient: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - listRequestClient: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void listRequestWorker(final CallbackDataStore<List<RequestEntity>> poCallback, FindRequestEntity poRequest) {
        LogUtil.i(TAG, "INICIO - listRequestWorker");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_worker);
        String lsEndpoint = getContext().getString(R.string.endpoint_list_request);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<List<RequestEntity>>> loCall =
                getRestApi().listRequestWorker(lsUrl, poRequest);

        RestReceptor<List<RequestEntity>> loRestReceptor = new RestReceptor<List<RequestEntity>>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<List<RequestEntity>>() {
            @Override
            public void onResponse(List<RequestEntity> poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - listRequestWorker: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - listRequestWorker: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void listQuote(final CallbackDataStore<List<QuoteEntity>> poCallback, FindQuoteEntity poRequest) {
        LogUtil.i(TAG, "INICIO - listQuote");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_quote);
        String lsEndpoint = getContext().getString(R.string.endpoint_list_quote);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<List<QuoteEntity>>> loCall =
                getRestApi().listQuote(lsUrl, poRequest);

        RestReceptor<List<QuoteEntity>> loRestReceptor = new RestReceptor<List<QuoteEntity>>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<List<QuoteEntity>>() {
            @Override
            public void onResponse(List<QuoteEntity> poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - listQuote: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - listQuote: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void updateTypeRequest(final CallbackDataStore<String> poCallback, UpdateTypeRequestEntity poRequest) {
        LogUtil.i(TAG, "INICIO - updateTypeRequest");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_request);
        String lsEndpoint = getContext().getString(R.string.endpoint_update_type_request);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<String>> loCall =
                getRestApi().updateTypeRequest(lsUrl, poRequest);

        RestReceptor<String> loRestReceptor = new RestReceptor<String>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<String>() {
            @Override
            public void onResponse(String poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - updateTypeRequest: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - updateTypeRequest: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void registerQuote(final CallbackDataStore<Integer> poCallback, RegisterQuoteEntity poRequest) {
        LogUtil.i(TAG, "INICIO - registerQuote");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_quote);
        String lsEndpoint = getContext().getString(R.string.endpoint_register);
        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<Integer>> loCall =
                getRestApi().registerQuote(lsUrl, poRequest);

        RestReceptor<Integer> loRestReceptor = new RestReceptor<Integer>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<Integer>() {
            @Override
            public void onResponse(Integer poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - registerQuote: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - registerQuote: onError");
                poCallback.onError(poException);
            }
        });
    }

    @Override
    public void registerQualify(final CallbackDataStore<Integer> poCallback, RegisterQualifyEntity poRequest, boolean isClient) {
        LogUtil.i(TAG, "INICIO - registerQualify");
        final String lsContext = getContext().getString(R.string.context_jobs);
        String lsWs = getContext().getString(R.string.ws_qualify);
        String lsEndpoint = "";
        if (isClient) {
            lsEndpoint = getContext().getString(R.string.endpoint_qualify_client);
        } else {
            lsEndpoint = getContext().getString(R.string.endpoint_qualify_worker);
        }


        String lsUrl = String.format("%s%s%s", lsContext, lsWs, lsEndpoint);

        Call<BaseResponseEntity<Integer>> loCall =
                getRestApi().registerQualify(lsUrl, poRequest);

        RestReceptor<Integer> loRestReceptor = new RestReceptor<Integer>(getContext());

        loRestReceptor.invoke(loCall, new RestCallback<Integer>()

        {
            @Override
            public void onResponse(Integer poData, BaseResponseEntity poBaseResponseEntity) {
                LogUtil.i(TAG, "FIN - registerQualify: onResponse");
                poCallback.onSuccess(poData);
            }

            @Override
            public void onError(BaseException poException) {
                LogUtil.i(TAG, "FIN - registerQualify: onError");
                poCallback.onError(poException);
            }
        });
    }
}
