package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryEntity {

    @JsonProperty("idCategoria")
    private int idCatSubcategory;
    @JsonProperty("lsSubcategorias")
    private List<SubcategoryEntity> listSubcategories;
    @JsonProperty("nombreCategoria")
    private String name;

    public int getIdCatSubcategory() {
        return idCatSubcategory;
    }

    public void setIdCatSubcategory(int idCatSubcategory) {
        this.idCatSubcategory = idCatSubcategory;
    }

    public List<SubcategoryEntity> getListSubcategories() {
        return listSubcategories;
    }

    public void setListSubcategories(List<SubcategoryEntity> listSubcategories) {
        this.listSubcategories = listSubcategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
