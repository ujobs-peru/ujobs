package com.ujobs.app.data.repository;

import android.content.Context;

import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.exception.BaseException;
import com.ujobs.app.data.repository.datasource.CallbackDataStore;
import com.ujobs.app.data.repository.datasource.UserDataStore;
import com.ujobs.app.data.repository.datasource.UserDataStoreFactory;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.UserRepository;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UserDataRepository implements UserRepository {

    private final UserDataStoreFactory goUserDataStoreFactory;

    public static UserDataRepository newInstance(Context poContext) {
        return new UserDataRepository(poContext);
    }

    public UserDataRepository(Context poContext) {
        this.goUserDataStoreFactory = UserDataStoreFactory.newInstance(poContext);
    }

    @Override
    public void login(final UserEntity poUser, final DomainCallback<UserEntity> poUserDomainCallback) {
        final UserDataStore loUserDataStore = goUserDataStoreFactory.createCloud();
        loUserDataStore.login(new CallbackDataStore<UserEntity>() {
            @Override
            public void onSuccess(UserEntity poData) {
                UserDataStore loUserDataStore = goUserDataStoreFactory.createShared();
                loUserDataStore.saveUser(poData);
                poUserDomainCallback.onSuccess(poData);
            }

            @Override
            public void onError(Throwable poException) {
                poUserDomainCallback.onError(poException);
            }
        }, poUser);

    }

    @Override
    public void signIn(UserEntity poUser, final DomainCallback<Integer> poDomainCallback) {
        final UserDataStore loUserDataStore = goUserDataStoreFactory.createCloud();
        loUserDataStore.signIn(new CallbackDataStore<Integer>() {
            @Override
            public void onSuccess(Integer psData) {
                poDomainCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poDomainCallback.onError(poException);
            }
        }, poUser);
    }

    @Override
    public void registerWorker(RegisterWorkerEntity poRequest, final DomainCallback<String> poCallback) {
        final UserDataStore loUserDataStore = goUserDataStoreFactory.createCloud();
        loUserDataStore.registerWorker(new CallbackDataStore<String>() {
            @Override
            public void onSuccess(String psData) {
                poCallback.onSuccess(psData);
            }

            @Override
            public void onError(Throwable poException) {
                poCallback.onError(poException);
            }
        }, poRequest);
    }

    @Override
    public void getUser(DomainCallback<UserEntity> poCallback) {
        final UserDataStore loUserDataStore = goUserDataStoreFactory.createShared();
        UserEntity loUserEntity = loUserDataStore.getUser();
        if (loUserEntity != null) {
            poCallback.onSuccess(loUserEntity);
        } else {
            poCallback.onError(new BaseException(BaseException.ERROR_SESSION_USER, null));
        }

    }
}
