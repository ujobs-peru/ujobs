package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserEntity {


    @JsonProperty("apeMaterno")
    private String motherLastName;
    @JsonProperty("apePaterno")
    private String lastName;
    @JsonProperty("clave")
    private String password;
    @JsonProperty("correo")
    private String email;
    @JsonProperty("idTipoEstadoReq")
    private String idEstateRequest;
    @JsonProperty("idUsuario")
    private int idUser;
    @JsonProperty("idImagen")
    private int idImage;
    @JsonProperty("nombres")
    private String names;
    @JsonProperty("nroDNI")
    private String dni;
    @JsonProperty("telefono")
    private String phone;
    @JsonProperty("lsCategorias")
    private List<CategoryEntity> lstCategory;
    @JsonProperty("lsDistritos")
    private List<DistrictEntity> lstDistrict;
    @JsonProperty("lsValoraciones")
    public List<QualifyEntity> lstQualify;
    @JsonProperty("idFCM")
    private String idFCM;

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public String getFullName() {
        return String.format("%s %s", names, lastName);
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdEstateRequest() {
        return idEstateRequest;
    }

    public void setIdEstateRequest(String idEstateRequest) {
        this.idEstateRequest = idEstateRequest;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<CategoryEntity> getLstCategory() {
        return lstCategory;
    }

    public void setLstCategory(List<CategoryEntity> lstCategory) {
        this.lstCategory = lstCategory;
    }

    public List<DistrictEntity> getLstDistrict() {
        return lstDistrict;
    }

    public void setLstDistrict(List<DistrictEntity> lstDistrict) {
        this.lstDistrict = lstDistrict;
    }

    public List<QualifyEntity> getLstQualify() {
        return lstQualify;
    }

    public void setLstQualify(List<QualifyEntity> lstQualify) {
        this.lstQualify = lstQualify;
    }

    public String getIdFCM() {
        return idFCM;
    }

    public void setIdFCM(String idFCM) {
        this.idFCM = idFCM;
    }
}
