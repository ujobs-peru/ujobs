package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.WorkerEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.WorkerModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class WorkerToMapper {


    private static WorkerToMapper INSTANCE;
    private final DistrictToMapper goDistrictModelToMapper;
    private final CategoryToMapper goCategoryModelToMapper;
    private final QualifyToMapper goQualifyToMapper;

    public static WorkerToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WorkerToMapper();
        }
        return INSTANCE;
    }


    public WorkerToMapper() {
        goDistrictModelToMapper = DistrictToMapper.newInstance();
        goCategoryModelToMapper = CategoryToMapper.newInstance();
        goQualifyToMapper = QualifyToMapper.newInstance();
    }


    public WorkerEntity transform(WorkerModel poWorkerModel) {

        WorkerEntity loWorkerEntity = null;
        if (poWorkerModel != null) {
            loWorkerEntity = new WorkerEntity();
            ParseUtil.parseObject(poWorkerModel, loWorkerEntity);
            loWorkerEntity.setLstCategory(goCategoryModelToMapper.transform(poWorkerModel.getLstCategory()));
            loWorkerEntity.setLstDistrict(goDistrictModelToMapper.transform(poWorkerModel.getLstDistrict()));
            loWorkerEntity.setLstQualify(goQualifyToMapper.transform(poWorkerModel.getLstQualify()));

        }
        return loWorkerEntity;
    }

    public List<WorkerEntity> transform(List<WorkerModel> poWorkerModel) {

        List<WorkerEntity> lasWorkerEntity = new ArrayList<>();
        if (poWorkerModel != null) {
            for (WorkerModel loWorkerModel : poWorkerModel) {
                WorkerEntity loWorkerEntity = transform(loWorkerModel);
                lasWorkerEntity.add(loWorkerEntity);
            }
        }
        return lasWorkerEntity;
    }
}
