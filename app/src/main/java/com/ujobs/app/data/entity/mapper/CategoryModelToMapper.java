package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.SubcategoryModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class CategoryModelToMapper {


    private static CategoryModelToMapper INSTANCE;

    public static CategoryModelToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryModelToMapper();
        }
        return INSTANCE;
    }

    public CategoryModelToMapper() {
        goSubcategoryModelToMapper = SubcategoryModelToMapper.newInstance();
    }

    SubcategoryModelToMapper goSubcategoryModelToMapper;

    public CategoryModel transform(CategoryEntity poCategoryEntity) {

        CategoryModel loCategoryModel = null;
        if (poCategoryEntity != null) {
            List<SubcategoryModel> subcategoryModelList = goSubcategoryModelToMapper.transform(poCategoryEntity.getListSubcategories());
            loCategoryModel = new CategoryModel(poCategoryEntity.getName(), subcategoryModelList);
            ParseUtil.parseObject(poCategoryEntity, loCategoryModel);
            loCategoryModel.setListSubcategories(subcategoryModelList);
        }
        return loCategoryModel;
    }

    public List<CategoryModel> transform(List<CategoryEntity> poCategoryEntity) {

        List<CategoryModel> lasCategoryModel = new ArrayList<>();
        if (poCategoryEntity != null) {
            for (CategoryEntity loCategoryEntity : poCategoryEntity) {
                CategoryModel loCategoryModel = transform(loCategoryEntity);
                lasCategoryModel.add(loCategoryModel);
            }
        }
        return lasCategoryModel;
    }
}
