package com.ujobs.app.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequestEntity {

    @JsonProperty("nombre")
    public String name;
    @JsonProperty("comentario")
    public String description;
    @JsonProperty("fechaIni")
    public String startDate;
    @JsonProperty("idUsuario")
    public int idUser;
    @JsonProperty("idDistrito")
    public int idDistrict;
    @JsonProperty("lsCatSubcategoria")
    public List<Integer> lstIdsCatSubcategory;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public List<Integer> getLstIdsCatSubcategory() {
        return lstIdsCatSubcategory;
    }

    public void setLstIdsCatSubcategory(List<Integer> lstIdsCatSubcategory) {
        this.lstIdsCatSubcategory = lstIdsCatSubcategory;
    }
}
