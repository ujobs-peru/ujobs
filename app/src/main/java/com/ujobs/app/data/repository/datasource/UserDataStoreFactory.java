package com.ujobs.app.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Factory that creates different implementations of {@link UserDataStore}.
 * <p>
 * Created by Hernan Pareja on 7/02/2017.
 */
public class UserDataStoreFactory {

    private static final String TAG = UserDataStoreFactory.class.getSimpleName();

    private final Context goContext;
    private static UserDataStoreFactory INSTANCE;

    public static UserDataStoreFactory newInstance(Context poContext) {
        if (INSTANCE == null) {
            INSTANCE = new UserDataStoreFactory(poContext);
        }
        return INSTANCE;
    }

    /**
     * Construct a {@link UserDataStoreFactory} that creates different implementations.
     *
     * @param poContext The {@link Context} to application.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public UserDataStoreFactory(@NonNull Context poContext) {
        this.goContext = poContext;
    }

    /**
     * Create {@link UserDataStore} to retrieve data from the Cloud.
     *
     * @return {@link UserCloudDataStore} of the Cloud.
     * @author Hernan Pareja
     * @version 1.0
     * @since 7/02/2017
     */
    public UserDataStore createCloud() {
        return new UserCloudDataStore(goContext);
    }

    public UserDataStore createShared() {
        return new UserSharedDataStore(goContext);
    }

}
