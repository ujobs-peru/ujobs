package com.ujobs.app.data.repository.datasource;


import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;

import java.io.File;
import java.util.List;

/**
 * Interface that represents a data store from where data is retrieved.
 * <p>
 * Created by Manuel Torres on 23/01/2017.
 */

public interface MasterDataStore {


    void listCatAndSubcategories(CallbackDataStore<List<CategoryEntity>> poCallback);

    void listDistrict(CallbackDataStore<List<DistrictEntity>> poCallback);

    void uploadFile(CallbackDataStore<String> poCallback, File poFile, String psType, int piId);


}
