package com.ujobs.app.data.util;

import android.text.TextUtils;

import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.StringCharacterIterator;

/**
 * Created by bn on 13/03/17.
 */

public class DecimalUtil {

    public static String PATTERN = "#,###,###,##0.00";
    public static char CHAR_DECIMAL_SEPARATOR = '.';
    public static char CHAR_GROUPING_SEPARATOR = ',';

    public static String getFormatToString(String psNumber) {
        String lsNumber = "";
        if (!TextUtils.isEmpty(psNumber)) {
            String lsNumberDecimal = String.format("%s.%s",
                    psNumber.substring(0, psNumber.length() - 2),
                    psNumber.substring(psNumber.length() - 2));
            lsNumber = format(Double.parseDouble(lsNumberDecimal));
        }
        return lsNumber;
    }

    public static String getFormatReplaceAllSeparator(String psNumber) {
        String ldValue = format(Double.parseDouble(psNumber.replace(",", "")));
        return ldValue.replace(".", "").replace(",", "");
    }

    public static String getFormatReplaceGroupSeparator(String psNumber) {
        return psNumber.replace(",", "");
    }

    public static String format(Object poObject) {
        DecimalFormatSymbols loSymbols = new DecimalFormatSymbols();
        loSymbols.setDecimalSeparator(CHAR_DECIMAL_SEPARATOR);
        loSymbols.setGroupingSeparator(CHAR_GROUPING_SEPARATOR);
        DecimalFormat loDecimalFormat = new DecimalFormat(PATTERN, loSymbols);
        return loDecimalFormat.format(poObject);
    }

    public static String format(Object poObject, String format) {
        DecimalFormatSymbols loSymbols = new DecimalFormatSymbols();
        loSymbols.setDecimalSeparator(CHAR_DECIMAL_SEPARATOR);
        DecimalFormat loDecimalFormat = new DecimalFormat(format, loSymbols);
        return loDecimalFormat.format(poObject);
    }


    public static String leftZero(String stringToPad, String padder, int size) {
        if (padder.length() == 0) {
            return stringToPad;
        }

        StringBuffer sb;
        StringCharacterIterator sci;

        sb = new StringBuffer(size);
        sci = new StringCharacterIterator(padder);

        while (sb.length() < (size - stringToPad.length())) {
            for (char ch = sci.first(); ch != CharacterIterator.DONE; ch = sci.next()) {
                if (sb.length() < size - stringToPad.length()) {
                    sb.insert(sb.length(), String.valueOf(ch));
                }
            }
        }
        return sb.append(stringToPad).toString();

    }

}
