package com.ujobs.app.data.entity.mapper;

import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.util.ParseUtil;
import com.ujobs.app.model.DistrictModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hernan Pareja on 10/02/2017.
 */
public class DistrictToMapper {


    private static DistrictToMapper INSTANCE;

    public static DistrictToMapper newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DistrictToMapper();
        }
        return INSTANCE;
    }


    public DistrictEntity transform(DistrictModel poDistrictModel) {

        DistrictEntity loDistrictEntity = null;
        if (poDistrictModel != null) {
            loDistrictEntity = new DistrictEntity();
            ParseUtil.parseObject(poDistrictModel, loDistrictEntity);
        }
        return loDistrictEntity;
    }

    public List<DistrictEntity> transform(List<DistrictModel> poDistrictModel) {

        List<DistrictEntity> lasDistrictEntity = new ArrayList<>();
        if (poDistrictModel != null) {
            for (DistrictModel loDistrictModel : poDistrictModel) {
                DistrictEntity loDistrictEntity = transform(loDistrictModel);
                lasDistrictEntity.add(loDistrictEntity);
            }
        }
        return lasDistrictEntity;
    }
}
