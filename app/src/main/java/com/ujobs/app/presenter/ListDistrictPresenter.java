package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.entity.mapper.DistrictModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.master.ListDistrictUseCase;
import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.ui.view.ListDistrictView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class ListDistrictPresenter implements Presenter {

    private ListDistrictView goListDistrictView;

    private ListDistrictUseCase goListDistrictUseCase;
    private final DistrictModelToMapper goDistrictModelMapper;

    public static ListDistrictPresenter newInstance(ListDistrictView poListDistrictView) {
        return new ListDistrictPresenter(poListDistrictView);
    }

    public ListDistrictPresenter(ListDistrictView poListDistrictView) {
        this.goListDistrictView = poListDistrictView;
        this.goListDistrictUseCase = ListDistrictUseCase.newInstance(poListDistrictView.context());
        this.goDistrictModelMapper = DistrictModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goListDistrictUseCase = null;
        this.goListDistrictView = null;
    }

    public void initialize() {
        this.listDistrict();
    }

    private void listDistrict() {
        this.hideViewRetry();
        this.showViewLoading();
        this.doListDistrict();
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goListDistrictView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goListDistrictView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goListDistrictView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goListDistrictView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goListDistrictView.showError(psErrorMessage);
    }

    private void showListDistrictSuccessToView(List<DistrictEntity> poData) {
        List<DistrictModel> gaoListCategoryModel = goDistrictModelMapper.transform(poData);
        this.goListDistrictView.showListDistrictSuccess(gaoListCategoryModel);
    }

    private void doListDistrict() {

        this.goListDistrictUseCase.buildUseCaseObservable(new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<List<DistrictEntity>> {

        @Override
        public void onSuccess(List<DistrictEntity> poData) {
            ListDistrictPresenter.this.hideViewLoading();
            ListDistrictPresenter.this.showListDistrictSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            ListDistrictPresenter.this.hideViewLoading();
            ListDistrictPresenter.this.showErrorMessage(poException.getMessage());
            ListDistrictPresenter.this.showViewRetry();
        }
    }
}
