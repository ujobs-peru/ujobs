package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.mapper.RegisterWorkerToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.user.RegisterWorkerUseCase;
import com.ujobs.app.model.RegisterWorkerModel;
import com.ujobs.app.ui.view.RegisterWorkerView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RegisterWorkerPresenter implements Presenter {

    private RegisterWorkerView goRegisterWorkerView;

    private RegisterWorkerUseCase goRegisterWorkerUseCase;
    private final RegisterWorkerToMapper goRegisterWorkerToMapper;

    public static RegisterWorkerPresenter newInstance(RegisterWorkerView poRegisterWorkerView) {
        return new RegisterWorkerPresenter(poRegisterWorkerView);
    }

    public RegisterWorkerPresenter(RegisterWorkerView poRegisterWorkerView) {
        this.goRegisterWorkerView = poRegisterWorkerView;
        this.goRegisterWorkerUseCase = RegisterWorkerUseCase.newInstance(poRegisterWorkerView.context());
        this.goRegisterWorkerToMapper = RegisterWorkerToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goRegisterWorkerUseCase = null;
        this.goRegisterWorkerView = null;
    }

    public void initialize(RegisterWorkerModel poRegisterWorkerModel) {
        this.registerWorker(poRegisterWorkerModel);
    }

    private void registerWorker(RegisterWorkerModel poRegisterWorkerModel) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doRegisterWorker(poRegisterWorkerModel);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goRegisterWorkerView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goRegisterWorkerView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goRegisterWorkerView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goRegisterWorkerView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goRegisterWorkerView.showError(psErrorMessage);
    }

    private void showRegisterWorkerSuccessToView(String psMessage) {
        this.goRegisterWorkerView.showRegisterWorkerSuccess(psMessage);
    }

    private void doRegisterWorker(RegisterWorkerModel poRegisterWorkerModel) {
        RegisterWorkerUseCase.Params loParams = RegisterWorkerUseCase.Params.forRegisterWorker(goRegisterWorkerToMapper.transform(poRegisterWorkerModel));
        this.goRegisterWorkerUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<String> {

        @Override
        public void onSuccess(String poData) {
            RegisterWorkerPresenter.this.hideViewLoading();
            RegisterWorkerPresenter.this.showRegisterWorkerSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            RegisterWorkerPresenter.this.hideViewLoading();
            RegisterWorkerPresenter.this.showErrorMessage(poException.getMessage());
            RegisterWorkerPresenter.this.showViewRetry();
        }
    }
}
