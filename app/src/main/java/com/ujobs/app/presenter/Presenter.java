package com.ujobs.app.presenter;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface Presenter {


    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void resume();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void pause();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void destroy();
}
