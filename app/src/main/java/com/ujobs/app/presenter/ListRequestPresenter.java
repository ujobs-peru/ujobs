package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.mapper.RequestModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.ListRequestUseCase;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.ui.view.ListRequestView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class ListRequestPresenter implements Presenter {


    public final static int TYPE_USER = ListRequestUseCase.TYPE_USER;
    public final static int TYPE_WORKER = ListRequestUseCase.TYPE_WORKER;

    private ListRequestView goListRequestView;

    private ListRequestUseCase goListRequestUseCase;
    private final RequestModelToMapper goRequestModelMapper;

    public static ListRequestPresenter newInstance(ListRequestView poListRequestView) {
        return new ListRequestPresenter(poListRequestView);
    }

    public ListRequestPresenter(ListRequestView poListRequestView) {
        this.goListRequestView = poListRequestView;
        this.goListRequestUseCase = ListRequestUseCase.newInstance(poListRequestView.context());
        this.goRequestModelMapper = RequestModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goListRequestUseCase = null;
        this.goListRequestView = null;
    }

    public void initialize(int idUser, int idStateRequestType, int type) {
        this.listRequest(idUser, idStateRequestType, type);
    }

    private void listRequest(int idUser, int idStateRequestType, int type) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doListRequest(idUser, idStateRequestType, type);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goListRequestView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goListRequestView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goListRequestView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goListRequestView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goListRequestView.showError(psErrorMessage);
    }

    private void showListRequestSuccessToView(List<RequestEntity> poData) {
        List<RequestModel> gaoListCategoryModel = goRequestModelMapper.transform(poData);
        this.goListRequestView.showListRequestSuccess(gaoListCategoryModel);
    }

    private void doListRequest(int idUser, int idStateRequestType, int type) {
        ListRequestUseCase.Params loParams = ListRequestUseCase.Params.forListRequest(idUser, idStateRequestType, type);
        this.goListRequestUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<List<RequestEntity>> {

        @Override
        public void onSuccess(List<RequestEntity> poData) {
            ListRequestPresenter.this.hideViewLoading();
            ListRequestPresenter.this.showListRequestSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            ListRequestPresenter.this.hideViewLoading();
            ListRequestPresenter.this.showErrorMessage(poException.getMessage());
            ListRequestPresenter.this.showViewRetry();
        }
    }
}
