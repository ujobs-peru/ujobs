package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.mapper.RequestModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.UpdateTypeRequestUseCase;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.ui.view.UpdateTypeRequestView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UpdateTypeRequestPresenter implements Presenter {

    private UpdateTypeRequestView goUpdateTypeRequestView;

    private UpdateTypeRequestUseCase goUpdateTypeRequestUseCase;

    public static UpdateTypeRequestPresenter newInstance(UpdateTypeRequestView poUpdateTypeRequestView) {
        return new UpdateTypeRequestPresenter(poUpdateTypeRequestView);
    }

    public UpdateTypeRequestPresenter(UpdateTypeRequestView poUpdateTypeRequestView) {
        this.goUpdateTypeRequestView = poUpdateTypeRequestView;
        this.goUpdateTypeRequestUseCase = UpdateTypeRequestUseCase.newInstance(poUpdateTypeRequestView.context());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goUpdateTypeRequestUseCase = null;
        this.goUpdateTypeRequestView = null;
    }

    public void initialize(int idRequest, int idStateRequestType, int idQuote) {
        this.updateTypeRequest(idRequest, idStateRequestType, idQuote);
    }

    private void updateTypeRequest(int idRequest, int idStateRequestType, int idQuote) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doUpdateTypeRequest(idRequest, idStateRequestType, idQuote);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goUpdateTypeRequestView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goUpdateTypeRequestView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goUpdateTypeRequestView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goUpdateTypeRequestView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goUpdateTypeRequestView.showError(psErrorMessage);
    }

    private void showUpdateTypeRequestSuccessToView(String poData) {
        this.goUpdateTypeRequestView.showUpdateTypeRequestSuccess(poData);
    }

    private void doUpdateTypeRequest(int idRequest, int idStateRequestType, int idQuote) {
        UpdateTypeRequestUseCase.Params loParams = UpdateTypeRequestUseCase.Params.forUpdateTypeRequest(idRequest, idStateRequestType, idQuote);
        this.goUpdateTypeRequestUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<String> {

        @Override
        public void onSuccess(String poData) {
            UpdateTypeRequestPresenter.this.hideViewLoading();
            UpdateTypeRequestPresenter.this.showUpdateTypeRequestSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            UpdateTypeRequestPresenter.this.hideViewLoading();
            UpdateTypeRequestPresenter.this.showErrorMessage(poException.getMessage());
            UpdateTypeRequestPresenter.this.showViewRetry();
        }
    }
}
