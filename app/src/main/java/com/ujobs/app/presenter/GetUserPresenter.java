package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.entity.mapper.UserModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.user.GetUserUseCase;
import com.ujobs.app.ui.view.GetUserView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class GetUserPresenter implements Presenter {

    private GetUserView goGetUserView;

    private GetUserUseCase goGetUserUseCase;
    private final UserModelToMapper goUserModelMapper;

    public static GetUserPresenter newInstance(GetUserView poGetUserView) {
        return new GetUserPresenter(poGetUserView);
    }

    public GetUserPresenter(GetUserView poGetUserView) {
        this.goGetUserView = poGetUserView;
        this.goGetUserUseCase = GetUserUseCase.newInstance(poGetUserView.context());
        this.goUserModelMapper = UserModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goGetUserUseCase = null;
        this.goGetUserView = null;
    }

    public void initialize() {
        this.getUser();
    }

    private void getUser() {
        this.hideViewRetry();
        this.showViewLoading();
        this.doGetUser();
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goGetUserView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goGetUserView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goGetUserView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goGetUserView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goGetUserView.showError(psErrorMessage);
    }

    private void showGetUserSuccessToView(UserEntity poData) {
        this.goGetUserView.showGetUserSuccess(goUserModelMapper.transform(poData));
    }

    private void doGetUser() {

        this.goGetUserUseCase.buildUseCaseObservable(new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<UserEntity> {

        @Override
        public void onSuccess(UserEntity poData) {
            GetUserPresenter.this.hideViewLoading();
            GetUserPresenter.this.showGetUserSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            GetUserPresenter.this.hideViewLoading();
            GetUserPresenter.this.showErrorMessage(poException.getMessage());
            GetUserPresenter.this.showViewRetry();
        }
    }
}
