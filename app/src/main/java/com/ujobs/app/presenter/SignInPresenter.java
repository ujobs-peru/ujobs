package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.entity.mapper.UserToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.user.SignInUseCase;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.view.SignInView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class SignInPresenter implements Presenter {

    private SignInView goSignInView;

    private SignInUseCase goSignInUseCase;
    private final UserToMapper goUserToMapper;

    public static SignInPresenter newInstance(SignInView poSignInView) {
        return new SignInPresenter(poSignInView);
    }

    public SignInPresenter(SignInView poSignInView) {
        this.goSignInView = poSignInView;
        this.goSignInUseCase = SignInUseCase.newInstance(poSignInView.context());
        this.goUserToMapper = UserToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goSignInUseCase = null;
        this.goSignInView = null;
    }

    public void initialize(UserModel poUserModel) {
        this.signIn(poUserModel);
    }

    private void signIn(UserModel poUserModel) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doSignIn(poUserModel);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goSignInView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goSignInView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goSignInView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goSignInView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goSignInView.showError(psErrorMessage);
    }

    private void showSignInSuccessToView(Integer piId) {
        this.goSignInView.showSignInSuccess(piId);
    }

    private void doSignIn(UserModel poUserModel) {
        SignInUseCase.Params loParams = SignInUseCase.Params.forUser(goUserToMapper.transform(poUserModel));
        this.goSignInUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<Integer> {

        @Override
        public void onSuccess(Integer poData) {
            SignInPresenter.this.hideViewLoading();
            SignInPresenter.this.showSignInSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            SignInPresenter.this.hideViewLoading();
            SignInPresenter.this.showErrorMessage(poException.getMessage());
            SignInPresenter.this.showViewRetry();
        }
    }
}
