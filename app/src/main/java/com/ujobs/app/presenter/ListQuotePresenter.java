package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.mapper.QuoteModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.ListQuoteUseCase;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.ui.view.ListQuoteView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class ListQuotePresenter implements Presenter {

    private ListQuoteView goListQuoteView;

    private ListQuoteUseCase goListQuoteUseCase;
    private final QuoteModelToMapper goQuoteModelMapper;

    public static ListQuotePresenter newInstance(ListQuoteView poListQuoteView) {
        return new ListQuotePresenter(poListQuoteView);
    }

    public ListQuotePresenter(ListQuoteView poListQuoteView) {
        this.goListQuoteView = poListQuoteView;
        this.goListQuoteUseCase = ListQuoteUseCase.newInstance(poListQuoteView.context());
        this.goQuoteModelMapper = QuoteModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goListQuoteUseCase = null;
        this.goListQuoteView = null;
    }

    public void initialize(int idRequest) {
        this.listQuote(idRequest);
    }

    private void listQuote(int idRequest) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doListQuote(idRequest);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goListQuoteView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goListQuoteView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goListQuoteView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goListQuoteView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goListQuoteView.showError(psErrorMessage);
    }

    private void showListQuoteSuccessToView(List<QuoteEntity> poData) {
        List<QuoteModel> gaoListCategoryModel = goQuoteModelMapper.transform(poData);
        this.goListQuoteView.showListQuoteSuccess(gaoListCategoryModel);
    }

    private void doListQuote(int idRequest) {
        ListQuoteUseCase.Params loParams = ListQuoteUseCase.Params.forListQuote(idRequest);
        this.goListQuoteUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<List<QuoteEntity>> {

        @Override
        public void onSuccess(List<QuoteEntity> poData) {
            ListQuotePresenter.this.hideViewLoading();
            ListQuotePresenter.this.showListQuoteSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            ListQuotePresenter.this.hideViewLoading();
            ListQuotePresenter.this.showErrorMessage(poException.getMessage());
            ListQuotePresenter.this.showViewRetry();
        }
    }
}
