package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.mapper.CategoryModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.master.ListCatAndSubcatUseCase;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.ui.view.ListCatAndSubcatView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class ListCatAndSubcatPresenter implements Presenter {

    private ListCatAndSubcatView goListCatAndSubcatView;

    private ListCatAndSubcatUseCase goListCatAndSubcatUseCase;
    private final CategoryModelToMapper goUserModelMapper;

    public static ListCatAndSubcatPresenter newInstance(ListCatAndSubcatView poListCatAndSubcatView) {
        return new ListCatAndSubcatPresenter(poListCatAndSubcatView);
    }

    public ListCatAndSubcatPresenter(ListCatAndSubcatView poListCatAndSubcatView) {
        this.goListCatAndSubcatView = poListCatAndSubcatView;
        this.goListCatAndSubcatUseCase = ListCatAndSubcatUseCase.newInstance(poListCatAndSubcatView.context());
        this.goUserModelMapper = CategoryModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goListCatAndSubcatUseCase = null;
        this.goListCatAndSubcatView = null;
    }

    public void initialize() {
        this.listCatAndSubcat();
    }

    private void listCatAndSubcat() {
        this.hideViewRetry();
        this.showViewLoading();
        this.doListCatAndSubcat();
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goListCatAndSubcatView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goListCatAndSubcatView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goListCatAndSubcatView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goListCatAndSubcatView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goListCatAndSubcatView.showError(psErrorMessage);
    }

    private void showListCatAndSubcatSuccessToView(List<CategoryEntity> poData) {
        List<CategoryModel> gaoListCategoryModel = goUserModelMapper.transform(poData);
        this.goListCatAndSubcatView.showListCatAndSubcatSuccess(gaoListCategoryModel);
    }

    private void doListCatAndSubcat() {

        this.goListCatAndSubcatUseCase.buildUseCaseObservable(new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<List<CategoryEntity>> {

        @Override
        public void onSuccess(List<CategoryEntity> poData) {
            ListCatAndSubcatPresenter.this.hideViewLoading();
            ListCatAndSubcatPresenter.this.showListCatAndSubcatSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            ListCatAndSubcatPresenter.this.hideViewLoading();
            ListCatAndSubcatPresenter.this.showErrorMessage(poException.getMessage());
            ListCatAndSubcatPresenter.this.showViewRetry();
        }
    }
}
