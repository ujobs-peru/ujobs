package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.mapper.RegisterRequestToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.RegisterRequestUseCase;
import com.ujobs.app.model.RegisterRequestModel;
import com.ujobs.app.ui.view.RegisterRequestView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RegisterRequestPresenter implements Presenter {

    private RegisterRequestView goRegisterRequestView;

    private RegisterRequestUseCase goRegisterRequestUseCase;
    private final RegisterRequestToMapper goRegisterRequestToMapper;

    public static RegisterRequestPresenter newInstance(RegisterRequestView poRegisterRequestView) {
        return new RegisterRequestPresenter(poRegisterRequestView);
    }

    public RegisterRequestPresenter(RegisterRequestView poRegisterRequestView) {
        this.goRegisterRequestView = poRegisterRequestView;
        this.goRegisterRequestUseCase = RegisterRequestUseCase.newInstance(poRegisterRequestView.context());
        this.goRegisterRequestToMapper = RegisterRequestToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goRegisterRequestUseCase = null;
        this.goRegisterRequestView = null;
    }

    public void initialize(RegisterRequestModel poRegisterRequestModel) {
        this.registerRequest(poRegisterRequestModel);
    }

    private void registerRequest(RegisterRequestModel poRegisterRequestModel) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doRegisterRequest(poRegisterRequestModel);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goRegisterRequestView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goRegisterRequestView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goRegisterRequestView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goRegisterRequestView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goRegisterRequestView.showError(psErrorMessage);
    }

    private void showRegisterRequestSuccessToView(Integer piIdRequest) {
        this.goRegisterRequestView.showRegisterRequestSuccess(piIdRequest);
    }

    private void doRegisterRequest(RegisterRequestModel poRegisterRequestModel) {
        RegisterRequestUseCase.Params loParams = RegisterRequestUseCase.Params.forRegisterRequest(goRegisterRequestToMapper.transform(poRegisterRequestModel));
        this.goRegisterRequestUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<Integer> {

        @Override
        public void onSuccess(Integer poData) {
            RegisterRequestPresenter.this.hideViewLoading();
            RegisterRequestPresenter.this.showRegisterRequestSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            RegisterRequestPresenter.this.hideViewLoading();
            RegisterRequestPresenter.this.showErrorMessage(poException.getMessage());
            RegisterRequestPresenter.this.showViewRetry();
        }
    }
}
