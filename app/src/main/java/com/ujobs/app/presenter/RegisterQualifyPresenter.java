package com.ujobs.app.presenter;

import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.RegisterQualifyUseCase;
import com.ujobs.app.ui.view.RegisterQualifyView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RegisterQualifyPresenter implements Presenter {

    private RegisterQualifyView goRegisterQualifyView;

    private RegisterQualifyUseCase goRegisterQualifyUseCase;

    public static RegisterQualifyPresenter newInstance(RegisterQualifyView poRegisterQualifyView) {
        return new RegisterQualifyPresenter(poRegisterQualifyView);
    }

    public RegisterQualifyPresenter(RegisterQualifyView poRegisterQualifyView) {
        this.goRegisterQualifyView = poRegisterQualifyView;
        this.goRegisterQualifyUseCase = RegisterQualifyUseCase.newInstance(poRegisterQualifyView.context());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goRegisterQualifyUseCase = null;
        this.goRegisterQualifyView = null;
    }

    public void initialize(Integer idRequest, Integer stars, String comment, boolean isClient) {
        this.registerQualify(idRequest, stars, comment, isClient);
    }

    private void registerQualify(Integer idRequest, Integer stars, String comment, boolean isClient) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doRegisterQualify(idRequest, stars, comment, isClient);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goRegisterQualifyView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goRegisterQualifyView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goRegisterQualifyView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goRegisterQualifyView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goRegisterQualifyView.showError(psErrorMessage);
    }

    /**
     * Show result registerQualify
     *
     * @param piId {@link Integer}.
     * @author Manuel Torres
     * @version 1.0
     * @since 15/02/2017
     */
    private void showRegisterQualifySuccessToView(Integer piId) {
        this.goRegisterQualifyView.showRegisterQualifySuccess(piId);
    }

    private void doRegisterQualify(Integer idRequest, Integer stars, String comment, boolean isClient) {
        RegisterQualifyUseCase.Params loParams = RegisterQualifyUseCase.Params.forRegisterQualify(idRequest, stars, comment, isClient);
        this.goRegisterQualifyUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<Integer> {

        @Override
        public void onSuccess(Integer poData) {
            RegisterQualifyPresenter.this.hideViewLoading();
            RegisterQualifyPresenter.this.showRegisterQualifySuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            RegisterQualifyPresenter.this.hideViewLoading();
            RegisterQualifyPresenter.this.showErrorMessage(poException.getMessage());
            RegisterQualifyPresenter.this.showViewRetry();
        }
    }
}
