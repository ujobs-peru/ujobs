package com.ujobs.app.presenter;

import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.master.UploadFileUseCase;
import com.ujobs.app.ui.view.UploadFileView;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UploadFilePresenter implements Presenter {

    public static final String TYPE_USER = "U";
    public static final String TYPE_REQUEST = "R";
    public static final String TYPE_QUOTE = "C";

    private UploadFileView goUploadFileView;
    private UploadFileUseCase goUploadFileUseCase;

    public static UploadFilePresenter newInstance(UploadFileView poUploadFileView) {
        return new UploadFilePresenter(poUploadFileView);
    }

    public UploadFilePresenter(UploadFileView poUploadFileView) {
        this.goUploadFileView = poUploadFileView;
        this.goUploadFileUseCase = UploadFileUseCase.newInstance(poUploadFileView.context());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goUploadFileUseCase = null;
        this.goUploadFileView = null;
    }

    public void initialize(int id, String type, List<String> filePath) {
        this.uploadFile(id, type, filePath);
    }

    private void uploadFile(int id, String type, List<String> filePath) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doUploadFile(id, type, filePath);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goUploadFileView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goUploadFileView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goUploadFileView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goUploadFileView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goUploadFileView.showError(psErrorMessage);
    }

    private void showUploadFileSuccessToView(String poData) {
        this.goUploadFileView.showUploadFileSuccess(poData);
    }

    private void doUploadFile(int id, String type, List<String> filePath) {
        UploadFileUseCase.Params loParams = UploadFileUseCase.Params.forUploadFile(id, filePath, type);
        this.goUploadFileUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<String> {

        @Override
        public void onSuccess(String poData) {
            UploadFilePresenter.this.hideViewLoading();
            UploadFilePresenter.this.showUploadFileSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            UploadFilePresenter.this.hideViewLoading();
            UploadFilePresenter.this.showErrorMessage(poException.getMessage());
            UploadFilePresenter.this.showViewRetry();
        }
    }
}
