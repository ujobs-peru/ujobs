package com.ujobs.app.presenter;

import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.request.RegisterQuoteUseCase;
import com.ujobs.app.ui.view.RegisterQuoteView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RegisterQuotePresenter implements Presenter {

    private RegisterQuoteView goRegisterQuoteView;

    private RegisterQuoteUseCase goRegisterQuoteUseCase;

    public static RegisterQuotePresenter newInstance(RegisterQuoteView poRegisterQuoteView) {
        return new RegisterQuotePresenter(poRegisterQuoteView);
    }

    public RegisterQuotePresenter(RegisterQuoteView poRegisterQuoteView) {
        this.goRegisterQuoteView = poRegisterQuoteView;
        this.goRegisterQuoteUseCase = RegisterQuoteUseCase.newInstance(poRegisterQuoteView.context());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goRegisterQuoteUseCase = null;
        this.goRegisterQuoteView = null;
    }

    public void initialize(int idWorker, int idRequest, String amount, String detail) {
        this.registerQuote(idWorker, idRequest, amount, detail);
    }

    private void registerQuote(int idWorker, int idRequest, String amount, String detail) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doRegisterQuote(idWorker, idRequest, amount, detail);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goRegisterQuoteView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goRegisterQuoteView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goRegisterQuoteView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goRegisterQuoteView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goRegisterQuoteView.showError(psErrorMessage);
    }

    /**
     * Show result registerQuote
     *
     * @param piId {@link Integer}.
     * @author Manuel Torres
     * @version 1.0
     * @since 15/02/2017
     */
    private void showRegisterQuoteSuccessToView(Integer piId) {
        this.goRegisterQuoteView.showRegisterQuoteSuccess(piId);
    }

    private void doRegisterQuote(int idWorker, int idRequest, String amount, String detail) {
        RegisterQuoteUseCase.Params loParams = RegisterQuoteUseCase.Params.forRegisterQuote(idWorker, idRequest, amount, detail);
        this.goRegisterQuoteUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<Integer> {

        @Override
        public void onSuccess(Integer poData) {
            RegisterQuotePresenter.this.hideViewLoading();
            RegisterQuotePresenter.this.showRegisterQuoteSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            RegisterQuotePresenter.this.hideViewLoading();
            RegisterQuotePresenter.this.showErrorMessage(poException.getMessage());
            RegisterQuotePresenter.this.showViewRetry();
        }
    }
}
