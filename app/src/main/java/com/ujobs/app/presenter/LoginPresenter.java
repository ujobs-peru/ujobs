package com.ujobs.app.presenter;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.entity.mapper.UserModelToMapper;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.interactor.user.LoginUseCase;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.view.LoginView;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class LoginPresenter implements Presenter {

    private LoginView goLoginView;

    private LoginUseCase goLoginUseCase;
    private final UserModelToMapper goUserModelMapper;

    public static LoginPresenter newInstance(LoginView poLoginView) {
        return new LoginPresenter(poLoginView);
    }

    public LoginPresenter(LoginView poLoginView) {
        this.goLoginView = poLoginView;
        this.goLoginUseCase = LoginUseCase.newInstance(poLoginView.context());
        this.goUserModelMapper = UserModelToMapper.newInstance();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.goLoginUseCase = null;
        this.goLoginView = null;
    }

    public void initialize(String psEmail, String psPassword, String idFCM) {
        this.login(psEmail, psPassword, idFCM);
    }

    private void login(String psEmail, String psPassword, String idFCM) {
        this.hideViewRetry();
        this.showViewLoading();
        this.doLogin(psEmail, psPassword, idFCM);
    }

    /**
     * Show view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewLoading() {
        this.goLoginView.showLoading();
    }

    /**
     * Hide view loading.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewLoading() {
        this.goLoginView.hideLoading();
    }

    /**
     * Show view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showViewRetry() {
        this.goLoginView.showRetry();
    }

    /**
     * Hide view retry.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void hideViewRetry() {
        this.goLoginView.hideRetry();
    }

    /**
     * Show error message.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/02/2017
     */
    private void showErrorMessage(String psErrorMessage) {
        this.goLoginView.showError(psErrorMessage);
    }

    /**
     * Show result login
     *
     * @param poUserEntity {@link UserEntity}.
     * @author Manuel Torres
     * @version 1.0
     * @since 15/02/2017
     */
    private void showLoginSuccessToView(UserEntity poUserEntity) {
        UserModel loUserModel = goUserModelMapper.transform(poUserEntity);
        this.goLoginView.showLoginSuccess(loUserModel);
    }

    private void doLogin(String psEmail, String psPassword, String idFCM) {
        LoginUseCase.Params loParams = LoginUseCase.Params.forUser(psEmail, psPassword, idFCM);
        this.goLoginUseCase.buildUseCaseObservable(loParams, new DomainObserver());
    }

    private final class DomainObserver implements DomainCallback<UserEntity> {

        @Override
        public void onSuccess(UserEntity poData) {
            LoginPresenter.this.hideViewLoading();
            LoginPresenter.this.showLoginSuccessToView(poData);

        }

        @Override
        public void onError(Throwable poException) {
            LoginPresenter.this.hideViewLoading();
            LoginPresenter.this.showErrorMessage(poException.getMessage());
            LoginPresenter.this.showViewRetry();
        }
    }
}
