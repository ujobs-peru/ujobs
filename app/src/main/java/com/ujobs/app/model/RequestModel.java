package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ujobs.app.R;
import com.ujobs.app.data.util.CalendarUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bn on 13/11/17.
 */

public class RequestModel implements Parcelable {

    public final static String FORMAT_DATE_SERVER = "yyyy-MM-dd kk:mm:ss";
    public final static String FORMAT_DATE_SHOW = "dd/MM/yyyy";

    public final static int STATE_TYPE_ALL = 0;
    public final static int STATE_TYPE_PENDING_CLIENT = 1;
    public final static int STATE_TYPE_PENDING_WORKER = 2;
    public final static int STATE_TYPE_IN_PROGRESS = 3;
    public final static int STATE_TYPE_FINISH = 4;

    private String description;
    private String endDate;
    private String startDate;
    private int idCotizacion;
    private int idRequest;
    private int idStateType;
    private int idUser;
    private int idDistrict;
    private String districtName;
    private String name;
    private int countQuotes;
    private List<CategoryModel> lstCategory;
    private List<Integer> lstImage;
    private String startDateShow;
    private String endDateShow;
    private UserModel user;
    private int countQualify;


    public String getTextCategory() {
        if (lstCategory != null && !lstCategory.isEmpty()) {
            return String.format("%s - %s", lstCategory.get(0).getName(),
                    lstCategory.get(0).getListSubcategories().get(0).getName());
        } else {
            return "";
        }
    }

    public int getTitleStateTypeClient() {
        int liText = -1;
        switch (idStateType) {
            case STATE_TYPE_PENDING_CLIENT:
                liText = R.string.request_client_state_type_pending_client;
                break;
            case STATE_TYPE_PENDING_WORKER:
                liText = R.string.request_client_state_type_pending_worker;
                break;
            case STATE_TYPE_IN_PROGRESS:
                liText = R.string.request_client_state_type_in_progress;
                break;
            case STATE_TYPE_FINISH:
                liText = R.string.request_client_state_type_finish;
                break;
        }

        return liText;
    }

    public int getTitleStateTypeWorker() {
        int liText = -1;
        switch (idStateType) {
            case STATE_TYPE_PENDING_CLIENT:
                liText = R.string.request_worker_state_type_pending_client;
                break;
            case STATE_TYPE_PENDING_WORKER:
                liText = R.string.request_worker_state_type_pending_worker;
                break;
            case STATE_TYPE_IN_PROGRESS:
                liText = R.string.request_worker_state_type_in_progress;
                break;
            case STATE_TYPE_FINISH:
                liText = R.string.request_worker_state_type_finish;
                break;
        }

        return liText;
    }


    public String getStartDateShow() {
        if (startDateShow == null) {
            startDateShow = String.format("Fecha inicio: %s", CalendarUtil.getFormatDate(this.startDate,
                    FORMAT_DATE_SERVER, FORMAT_DATE_SHOW));
        }
        return startDateShow;
    }

    public void setStartDateShow(String startDateShow) {
        this.startDateShow = startDateShow;
    }

    public String getEndDateShow() {
        if (endDateShow == null) {
            endDateShow = String.format("Fecha fin: %s", CalendarUtil.getFormatDate(this.endDate,
                    FORMAT_DATE_SERVER, FORMAT_DATE_SHOW));
        }
        return endDateShow;
    }

    public String getDistrictShow() {
        return String.format("Distrito: %s", districtName);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(int idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }

    public int getIdStateType() {
        return idStateType;
    }

    public void setIdStateType(int idStateType) {
        this.idStateType = idStateType;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountQuotes() {
        return countQuotes;
    }

    public void setCountQuotes(int countQuotes) {
        this.countQuotes = countQuotes;
    }

    public List<CategoryModel> getLstCategory() {
        return lstCategory;
    }

    public void setLstCategory(List<CategoryModel> lstCategory) {
        this.lstCategory = lstCategory;
    }

    public List<Integer> getLstImage() {
        return lstImage;
    }

    public void setLstImage(List<Integer> lstImage) {
        this.lstImage = lstImage;
    }

    public void setEndDateShow(String endDateShow) {
        this.endDateShow = endDateShow;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public int getCountQualify() {
        return countQualify;
    }

    public void setCountQualify(int countQualify) {
        this.countQualify = countQualify;
    }

    public RequestModel() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeString(this.endDate);
        dest.writeString(this.startDate);
        dest.writeInt(this.idCotizacion);
        dest.writeInt(this.idRequest);
        dest.writeInt(this.idStateType);
        dest.writeInt(this.idUser);
        dest.writeInt(this.idDistrict);
        dest.writeString(this.districtName);
        dest.writeString(this.name);
        dest.writeInt(this.countQuotes);
        dest.writeTypedList(this.lstCategory);
        dest.writeList(this.lstImage);
        dest.writeString(this.startDateShow);
        dest.writeString(this.endDateShow);
        dest.writeParcelable(this.user, flags);
        dest.writeInt(this.countQualify);
    }

    protected RequestModel(Parcel in) {
        this.description = in.readString();
        this.endDate = in.readString();
        this.startDate = in.readString();
        this.idCotizacion = in.readInt();
        this.idRequest = in.readInt();
        this.idStateType = in.readInt();
        this.idUser = in.readInt();
        this.idDistrict = in.readInt();
        this.districtName = in.readString();
        this.name = in.readString();
        this.countQuotes = in.readInt();
        this.lstCategory = in.createTypedArrayList(CategoryModel.CREATOR);
        this.lstImage = new ArrayList<Integer>();
        in.readList(this.lstImage, Integer.class.getClassLoader());
        this.startDateShow = in.readString();
        this.endDateShow = in.readString();
        this.user = in.readParcelable(UserModel.class.getClassLoader());
        this.countQualify = in.readInt();
    }

    public static final Creator<RequestModel> CREATOR = new Creator<RequestModel>() {
        @Override
        public RequestModel createFromParcel(Parcel source) {
            return new RequestModel(source);
        }

        @Override
        public RequestModel[] newArray(int size) {
            return new RequestModel[size];
        }
    };
}
