package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class RegisterWorkerModel implements Parcelable {

    private int idWorker;
    private List<Integer> listIdsCatAndSubcat;
    private List<Integer> listIdsDistrict;

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public List<Integer> getListIdsCatAndSubcat() {
        return listIdsCatAndSubcat;
    }

    public void setListIdsCatAndSubcat(List<Integer> listIdsCatAndSubcat) {
        this.listIdsCatAndSubcat = listIdsCatAndSubcat;
    }

    public List<Integer> getListIdsDistrict() {
        return listIdsDistrict;
    }

    public void setListIdsDistrict(List<Integer> listIdsDistrict) {
        this.listIdsDistrict = listIdsDistrict;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idWorker);
        dest.writeList(this.listIdsCatAndSubcat);
        dest.writeList(this.listIdsDistrict);
    }

    public RegisterWorkerModel() {
    }

    protected RegisterWorkerModel(Parcel in) {
        this.idWorker = in.readInt();
        this.listIdsCatAndSubcat = new ArrayList<Integer>();
        in.readList(this.listIdsCatAndSubcat, Integer.class.getClassLoader());
        this.listIdsDistrict = new ArrayList<Integer>();
        in.readList(this.listIdsDistrict, Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<RegisterWorkerModel> CREATOR = new Parcelable.Creator<RegisterWorkerModel>() {
        @Override
        public RegisterWorkerModel createFromParcel(Parcel source) {
            return new RegisterWorkerModel(source);
        }

        @Override
        public RegisterWorkerModel[] newArray(int size) {
            return new RegisterWorkerModel[size];
        }
    };
}
