package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ujobs.app.data.entity.QualifyEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.util.DecimalUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bn on 14/11/17.
 */

public class WorkerModel extends UserModel implements Parcelable {

    public int idWorker;


    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idWorker);
    }

    public WorkerModel() {
    }

    protected WorkerModel(Parcel in) {
        this.idWorker = in.readInt();
    }

    public static final Creator<WorkerModel> CREATOR = new Creator<WorkerModel>() {
        @Override
        public WorkerModel createFromParcel(Parcel source) {
            return new WorkerModel(source);
        }

        @Override
        public WorkerModel[] newArray(int size) {
            return new WorkerModel[size];
        }
    };
}
