package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ujobs.app.data.util.DecimalUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bn on 14/11/17.
 */
public class QuoteModel implements Parcelable {

    public String detail;
    public int idQuote;
    public int idWorker;
    public double amount;
    public List<Integer> lstIdsImage;
    public WorkerModel worker;


    public String getAmountTitleShow() {
        return String.format("Monto: %s", getAmountShow());
    }

    public String getAmountShow() {
        return String.format("S/ %s", DecimalUtil.format(amount));
    }

    public String getDestailShow() {
        return String.format("Descripción: %s", detail);
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getIdQuote() {
        return idQuote;
    }

    public void setIdQuote(int idQuote) {
        this.idQuote = idQuote;
    }

    public int getIdWorker() {
        return idWorker;
    }

    public void setIdWorker(int idWorker) {
        this.idWorker = idWorker;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<Integer> getLstIdsImage() {
        return lstIdsImage;
    }

    public void setLstIdsImage(List<Integer> lstIdsImage) {
        this.lstIdsImage = lstIdsImage;
    }

    public WorkerModel getWorker() {
        return worker;
    }

    public void setWorker(WorkerModel worker) {
        this.worker = worker;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.detail);
        dest.writeInt(this.idQuote);
        dest.writeInt(this.idWorker);
        dest.writeDouble(this.amount);
        dest.writeList(this.lstIdsImage);
        dest.writeParcelable(this.worker, flags);
    }

    public QuoteModel() {
    }

    protected QuoteModel(Parcel in) {
        this.detail = in.readString();
        this.idQuote = in.readInt();
        this.idWorker = in.readInt();
        this.amount = in.readDouble();
        this.lstIdsImage = new ArrayList<Integer>();
        in.readList(this.lstIdsImage, Integer.class.getClassLoader());
        this.worker = in.readParcelable(WorkerModel.class.getClassLoader());
    }

    public static final Creator<QuoteModel> CREATOR = new Creator<QuoteModel>() {
        @Override
        public QuoteModel createFromParcel(Parcel source) {
            return new QuoteModel(source);
        }

        @Override
        public QuoteModel[] newArray(int size) {
            return new QuoteModel[size];
        }
    };
}
