package com.ujobs.app.model;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.util.DecimalUtil;

/**
 * Created by bn on 14/11/17.
 */
public class QualifyModel extends UserModel {

    private Integer idQualify;
    private Integer stars;
    private String comment;

    public String getStarsShow() {
        return DecimalUtil.format(stars);
    }

    public Integer getIdQualify() {
        return idQualify;
    }

    public void setIdQualify(Integer idQualify) {
        this.idQualify = idQualify;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.idQualify);
        dest.writeValue(this.stars);
        dest.writeString(this.comment);
    }

    public QualifyModel() {
    }

    protected QualifyModel(Parcel in) {
        super(in);
        this.idQualify = (Integer) in.readValue(Integer.class.getClassLoader());
        this.stars = (Integer) in.readValue(Integer.class.getClassLoader());
        this.comment = in.readString();
    }

    public static final Creator<QualifyModel> CREATOR = new Creator<QualifyModel>() {
        @Override
        public QualifyModel createFromParcel(Parcel source) {
            return new QualifyModel(source);
        }

        @Override
        public QualifyModel[] newArray(int size) {
            return new QualifyModel[size];
        }
    };
}
