package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
public class CategoryModel extends ExpandableGroup<SubcategoryModel> implements Parcelable {

    private int idCatSubcategory;
    private List<SubcategoryModel> listSubcategories;
    private String name;

    public int getIdCatSubcategory() {
        return idCatSubcategory;
    }

    public void setIdCatSubcategory(int idCatSubcategory) {
        this.idCatSubcategory = idCatSubcategory;
    }

    public List<SubcategoryModel> getListSubcategories() {
        return listSubcategories;
    }

    public void setListSubcategories(List<SubcategoryModel> listSubcategories) {
        this.listSubcategories = listSubcategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idCatSubcategory);
        dest.writeList(this.listSubcategories);
        dest.writeString(this.name);
    }

    public CategoryModel() {
        super(null,null);
    }

    public CategoryModel(String title, List<SubcategoryModel> items) {
        super(title, items);
    }

    protected CategoryModel(Parcel in) {
        super(null,null);
        this.idCatSubcategory = in.readInt();
        this.listSubcategories = new ArrayList<SubcategoryModel>();
        in.readList(this.listSubcategories, SubcategoryModel.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Parcelable.Creator<CategoryModel> CREATOR = new Parcelable.Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel source) {
            return new CategoryModel(source);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };
}
