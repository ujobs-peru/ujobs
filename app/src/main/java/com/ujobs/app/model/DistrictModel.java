package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Manuel Torres on 12/11/17.
 */
public class DistrictModel implements Parcelable {

    private int idDistrict;
    private String name;
    private boolean isChecked;

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public DistrictModel() {
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idDistrict);
        dest.writeString(this.name);
        dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
    }

    protected DistrictModel(Parcel in) {
        this.idDistrict = in.readInt();
        this.name = in.readString();
        this.isChecked = in.readByte() != 0;
    }

    public static final Creator<DistrictModel> CREATOR = new Creator<DistrictModel>() {
        @Override
        public DistrictModel createFromParcel(Parcel source) {
            return new DistrictModel(source);
        }

        @Override
        public DistrictModel[] newArray(int size) {
            return new DistrictModel[size];
        }
    };
}
