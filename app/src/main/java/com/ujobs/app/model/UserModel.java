package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ujobs.app.data.util.DecimalUtil;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class UserModel implements Parcelable {

    private String motherLastName;
    private String lastName;
    private String password;
    private String email;
    private String idEstateRequest;
    private int idUser;
    private String names;
    private String dni;
    private String phone;
    private List<DistrictModel> lstDistrict;
    private List<CategoryModel> lstCategory;
    private List<QualifyModel> lstQualify;
    private int idImage;
    private String idFCM;


    public String getQualifyAverageShow() {
        return DecimalUtil.format(getQualifyAverage());
    }

    public double getQualifyAverage() {
        double ldQualifyAverage = 0;
        int count = 0;
        for (QualifyModel loQualifyModel : lstQualify) {
            ldQualifyAverage += loQualifyModel.getStars();
            count++;
        }

        return count > 0 ? ldQualifyAverage / count : 0;
    }

    public String getPhoneShow() {
        return String.format("Teléfono: %s", phone);
    }

    public String getDniShow() {
        return String.format("DNI: %s", dni);
    }

    public String getFullName() {
        return String.format("%s %s", names, lastName);
    }

    public String getMotherLastName() {
        return motherLastName;
    }

    public void setMotherLastName(String motherLastName) {
        this.motherLastName = motherLastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdEstateRequest() {
        return idEstateRequest;
    }

    public void setIdEstateRequest(String idEstateRequest) {
        this.idEstateRequest = idEstateRequest;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<DistrictModel> getLstDistrict() {
        return lstDistrict;
    }

    public void setLstDistrict(List<DistrictModel> lstDistrict) {
        this.lstDistrict = lstDistrict;
    }

    public List<CategoryModel> getLstCategory() {
        return lstCategory;
    }

    public void setLstCategory(List<CategoryModel> lstCategory) {
        this.lstCategory = lstCategory;
    }

    public List<QualifyModel> getLstQualify() {
        return lstQualify;
    }

    public void setLstQualify(List<QualifyModel> lstQualify) {
        this.lstQualify = lstQualify;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public String getIdFCM() {
        return idFCM;
    }

    public void setIdFCM(String idFCM) {
        this.idFCM = idFCM;
    }

    public UserModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.motherLastName);
        dest.writeString(this.lastName);
        dest.writeString(this.password);
        dest.writeString(this.email);
        dest.writeString(this.idEstateRequest);
        dest.writeInt(this.idUser);
        dest.writeString(this.names);
        dest.writeString(this.dni);
        dest.writeString(this.phone);
        dest.writeTypedList(this.lstDistrict);
        dest.writeTypedList(this.lstCategory);
        dest.writeTypedList(this.lstQualify);
        dest.writeInt(this.idImage);
        dest.writeString(this.idFCM);
    }

    protected UserModel(Parcel in) {
        this.motherLastName = in.readString();
        this.lastName = in.readString();
        this.password = in.readString();
        this.email = in.readString();
        this.idEstateRequest = in.readString();
        this.idUser = in.readInt();
        this.names = in.readString();
        this.dni = in.readString();
        this.phone = in.readString();
        this.lstDistrict = in.createTypedArrayList(DistrictModel.CREATOR);
        this.lstCategory = in.createTypedArrayList(CategoryModel.CREATOR);
        this.lstQualify = in.createTypedArrayList(QualifyModel.CREATOR);
        this.idImage = in.readInt();
        this.idFCM = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
