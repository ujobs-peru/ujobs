package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */
public class RegisterRequestModel implements Parcelable {

    public String name;
    public String description;
    public String startDate;
    public int idUser;
    public int idDistrict;
    public List<Integer> lstIdsCatSubcategory;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public List<Integer> getLstIdsCatSubcategory() {
        return lstIdsCatSubcategory;
    }

    public void setLstIdsCatSubcategory(List<Integer> lstIdsCatSubcategory) {
        this.lstIdsCatSubcategory = lstIdsCatSubcategory;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.startDate);
        dest.writeInt(this.idUser);
        dest.writeInt(this.idDistrict);
        dest.writeList(this.lstIdsCatSubcategory);
    }

    public RegisterRequestModel() {
    }

    protected RegisterRequestModel(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.startDate = in.readString();
        this.idUser = in.readInt();
        this.idDistrict = in.readInt();
        this.lstIdsCatSubcategory = new ArrayList<Integer>();
        in.readList(this.lstIdsCatSubcategory, Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<RegisterRequestModel> CREATOR = new Parcelable.Creator<RegisterRequestModel>() {
        @Override
        public RegisterRequestModel createFromParcel(Parcel source) {
            return new RegisterRequestModel(source);
        }

        @Override
        public RegisterRequestModel[] newArray(int size) {
            return new RegisterRequestModel[size];
        }
    };
}
