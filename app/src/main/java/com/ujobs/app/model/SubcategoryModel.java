package com.ujobs.app.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Manuel Torres on 12/11/17.
 */
public class SubcategoryModel implements Parcelable {

    public SubcategoryModel() {
    }


    private int idCatSubcategory;
    private int idSubcategory;
    private String name;
    private boolean isCheck;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public int getIdCatSubcategory() {
        return idCatSubcategory;
    }

    public void setIdCatSubcategory(int idCatSubcategory) {
        this.idCatSubcategory = idCatSubcategory;
    }

    public int getIdSubcategory() {
        return idSubcategory;
    }

    public void setIdSubcategory(int idSubcategory) {
        this.idSubcategory = idSubcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idCatSubcategory);
        dest.writeInt(this.idSubcategory);
        dest.writeString(this.name);
        dest.writeByte(this.isCheck ? (byte) 1 : (byte) 0);
    }

    protected SubcategoryModel(Parcel in) {
        this.idCatSubcategory = in.readInt();
        this.idSubcategory = in.readInt();
        this.name = in.readString();
        this.isCheck = in.readByte() != 0;
    }

    public static final Creator<SubcategoryModel> CREATOR = new Creator<SubcategoryModel>() {
        @Override
        public SubcategoryModel createFromParcel(Parcel source) {
            return new SubcategoryModel(source);
        }

        @Override
        public SubcategoryModel[] newArray(int size) {
            return new SubcategoryModel[size];
        }
    };
}
