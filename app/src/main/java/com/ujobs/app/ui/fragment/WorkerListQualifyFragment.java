package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ujobs.app.R;
import com.ujobs.app.model.QualifyModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.adapter.QualifyAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerListQualifyFragment extends BaseFragment {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_USER_REQUEST = "USER_REQUEST";

    public static WorkerListQualifyFragment newInstance(UserModel poUserModel, UserModel poUserRequestModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_USER_REQUEST, poUserRequestModel);
        WorkerListQualifyFragment loClientListQualifyFragment = new WorkerListQualifyFragment();
        loClientListQualifyFragment.setArguments(loBundle);
        return loClientListQualifyFragment;
    }

    public interface OnClientListQualifyFragment {

    }

    @BindView(R.id.rcvQualify)
    RecyclerView rcvQualify;

    Unbinder unbinder;

    private OnClientListQualifyFragment goOnClientListQualifyFragment;
    private QualifyAdapter goQualifyAdapter;
    private UserModel goUserModel;
    private UserModel goUserRequestModel;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goUserRequestModel = getArguments().getParcelable(BUNDLE_USER_REQUEST);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_quote_list_qualify, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    private void setUpView() {
        goQualifyAdapter = new QualifyAdapter(getContext());
        rcvQualify.setHasFixedSize(true);
    }

    private void setupRecyclerView() {
        this.goQualifyAdapter.setOnItemClickListener(onClientItemClickListener);
        LinearLayoutManager loEntityLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        this.rcvQualify.setLayoutManager(loEntityLayoutManager);
        this.rcvQualify.setAdapter(goQualifyAdapter);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            this.loadListRequest();
        }
    }

    private void loadListRequest() {
        goQualifyAdapter.setQualifyList(goUserRequestModel.getLstQualify());
    }


    private QualifyAdapter.OnItemClickListener onClientItemClickListener =
            new QualifyAdapter.OnItemClickListener() {
                @Override
                public void onQualifyItemClicked(QualifyModel poQualifyModel) {

                }

            };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClientListQualifyFragment) {
            goOnClientListQualifyFragment = (OnClientListQualifyFragment) context;
        }
    }


}
