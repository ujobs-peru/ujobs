package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.ujobs.app.R;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class TypeUserActivity extends BaseActivity implements GetUserView {

    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, TypeUserActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    private GetUserPresenter goGetUserPresenter;
    private UserModel goUserModel;

    @BindView(R.id.btnClient)
    Button btnClient;
    @BindView(R.id.btnWorker)
    Button btnWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_user);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }

    @OnClick(R.id.btnClient)
    public void onBtnClientClicked() {
        TypeUserActivity.this.navigator.navigateToClient();
    }

    @OnClick(R.id.btnWorker)
    public void onBtnWorkerClicked() {
        if (goUserModel.getLstCategory() != null && !goUserModel.getLstCategory().isEmpty()
                && goUserModel.getLstDistrict() != null && !goUserModel.getLstDistrict().isEmpty()) {
            navigator.navigateToWorker();
        } else {
            navigator.navigateToWorkerRegister();
        }
    }

    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        goUserModel = poUserModel;
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @Override
    public Context context() {
        return this;
    }
}
