package com.ujobs.app.ui.view;

import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.model.RequestModel;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface ListRequestView extends LoadDataView {

    void showListRequestSuccess(List<RequestModel> paoListDistrictModel);
}
