package com.ujobs.app.ui.view;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface UploadFileView extends LoadDataView {

    void showUploadFileSuccess(String psMessage);
}
