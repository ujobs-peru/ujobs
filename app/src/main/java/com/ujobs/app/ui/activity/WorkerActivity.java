package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.adapter.WorkerTabAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.WorkerListRequestFragment;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerActivity extends BaseActivity implements
        WorkerListRequestFragment.OnWorkerListRequestFragment, GetUserView {


    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, WorkerActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.tbTabs)
    TabLayout tbTabs;
    @BindView(R.id.vpPager)
    ViewPager vpPager;

    private WorkerTabAdapter goWorkerTabAdapter;
    private GetUserPresenter goGetUserPresenter;
    private UserModel goUserModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }


    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        this.goUserModel = poUserModel;

        goWorkerTabAdapter = new WorkerTabAdapter(getSupportFragmentManager(), goUserModel);
        goWorkerTabAdapter.setTitle(getResources().getStringArray(R.array.worker_tabs));
        vpPager.setAdapter(goWorkerTabAdapter);
        vpPager.setOffscreenPageLimit(2);
        tbTabs.setupWithViewPager(vpPager);
    }

    @Override
    public void goOnRequest(RequestModel poRequestModel) {
        navigator.navigateToWorkerRequest(poRequestModel);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }


}
