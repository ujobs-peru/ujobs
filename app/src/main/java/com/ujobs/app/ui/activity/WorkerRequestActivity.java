package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.ClientQuoteFragment;
import com.ujobs.app.ui.fragment.ClientRequestFragment;
import com.ujobs.app.ui.fragment.WorkerQuoteFragment;
import com.ujobs.app.ui.fragment.WorkerQuoteRegisterDetailFragment;
import com.ujobs.app.ui.fragment.WorkerRequestFragment;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerRequestActivity extends BaseActivity implements GetUserView,
        WorkerRequestFragment.OnWorkerRequestFragment, WorkerQuoteRegisterDetailFragment.OnWorkerQuoteRegisterDetailFragment,
        WorkerQuoteFragment.OnWorkerQuoteFragment {
    private static final String BUNDLE_REQUEST = "REQUEST";

    public static Intent getCallingIntent(Context poContext, RequestModel poRequestModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_REQUEST, poRequestModel);
        Intent loIntent = new Intent(poContext, WorkerRequestActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }


    private GetUserPresenter goGetUserPresenter;
    private UserModel goUserModel;
    private RequestModel goRequestModel;
    private int giPositionTakePhoto = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_request);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        Bundle loBundle = getIntent().getExtras();
        goRequestModel = loBundle.getParcelable(BUNDLE_REQUEST);

        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }


    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        this.goUserModel = poUserModel;
        addFragment(R.id.frlContainer, WorkerRequestFragment.newInstance(goUserModel, goRequestModel));

    }

    @Override
    public void goQuote(QuoteModel poQuoteModel) {
        replaceFragment(R.id.frlContainer, WorkerQuoteFragment.newInstance(goUserModel, goRequestModel, poQuoteModel));
    }

    @Override
    public void onTakePhoto(Integer piPosition) {
        giPositionTakePhoto = piPosition;
        alertPhotoOrGallery();

    }


    @Override
    public void goBack(int piType) {
        goRequestModel.setIdStateType(piType);
        onBackPressed();
    }

    @Override
    public void onFinish() {
        finish();
    }

    @Override
    public void onSelectPhoto(String path) {
        super.onSelectPhoto(path);
        Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
        if (loFragment instanceof WorkerQuoteFragment) {
            ((WorkerQuoteFragment) loFragment).onPhotoPath(path, giPositionTakePhoto);
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
            Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
            if (loFragment instanceof WorkerRequestFragment) {
                ((WorkerRequestFragment) loFragment).refreshView(goRequestModel);
            }
        }

    }


}
