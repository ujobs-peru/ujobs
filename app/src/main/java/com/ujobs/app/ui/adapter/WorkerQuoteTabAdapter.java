package com.ujobs.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.fragment.ClientQuoteDetailFragment;
import com.ujobs.app.ui.fragment.WorkerListQualifyFragment;
import com.ujobs.app.ui.fragment.WorkerQuoteDetailFragment;
import com.ujobs.app.ui.fragment.WorkerQuoteRegisterDetailFragment;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerQuoteTabAdapter extends FragmentStatePagerAdapter {


    private String[] titles;
    private final UserModel goUsermodel;
    private final QuoteModel goQuoteModel;
    private final RequestModel goRequestModel;
    private WorkerQuoteRegisterDetailFragment goWorkerQuoteRegisterDetailFragment;
    private WorkerListQualifyFragment goWorkerListQualifyFragment;
    private WorkerQuoteDetailFragment goWorkerQuoteDetailFragment;

    public WorkerQuoteTabAdapter(FragmentManager fm, UserModel poUserModel, RequestModel poRequestModel, QuoteModel poQuoteModel) {
        super(fm);
        goUsermodel = poUserModel;
        this.goQuoteModel = poQuoteModel;
        this.goRequestModel = poRequestModel;
    }

    public void setTitle(String[] titles) {
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment loFragment = null;
        switch (position) {
            case 0:
                if (goQuoteModel != null) {
                    loFragment = goWorkerQuoteDetailFragment = WorkerQuoteDetailFragment.newInstance(goUsermodel, goQuoteModel);
                } else {
                    loFragment = goWorkerQuoteRegisterDetailFragment = WorkerQuoteRegisterDetailFragment.newInstance(goUsermodel, goRequestModel);
                }
                break;
            case 1:
                loFragment = goWorkerListQualifyFragment = WorkerListQualifyFragment.newInstance(goUsermodel, goRequestModel.getUser());
                break;
        }
        return loFragment;
    }

    public Fragment getFragmentAtPosition(int position) {
        Fragment loFragment = null;
        switch (position) {
            case 0:
                if (goQuoteModel != null) {
                    loFragment = goWorkerQuoteDetailFragment;
                } else {
                    loFragment = goWorkerQuoteRegisterDetailFragment;
                }

//                loFragment = ClientQuoteDetailFragment.newInstance(goUsermodel, goQuoteModel);
                break;
            case 1:
                loFragment = goWorkerListQualifyFragment;
                break;
        }
        return loFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
