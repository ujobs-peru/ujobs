package com.ujobs.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.fragment.ClientListQualifyFragment;
import com.ujobs.app.ui.fragment.ClientQuoteDetailFragment;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientQuoteTabAdapter extends FragmentStatePagerAdapter {


    private String[] titles;
    private final UserModel goUsermodel;
    private final QuoteModel goQuoteModel;

    public ClientQuoteTabAdapter(FragmentManager fm, UserModel poUserModel, QuoteModel poQuoteModel) {
        super(fm);
        goUsermodel = poUserModel;
        this.goQuoteModel = poQuoteModel;
    }

    public void setTitle(String[] titles) {
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment loFragment = null;
        switch (position) {
            case 0:
                loFragment = ClientQuoteDetailFragment.newInstance(goUsermodel, goQuoteModel);
                break;
            case 1:
                loFragment = ClientListQualifyFragment.newInstance(goUsermodel, goQuoteModel.getWorker());
                break;
        }
        return loFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
