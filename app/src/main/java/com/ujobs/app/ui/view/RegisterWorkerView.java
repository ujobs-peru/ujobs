package com.ujobs.app.ui.view;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface RegisterWorkerView extends LoadDataView {

    void showRegisterWorkerSuccess(String psMessage);
}
