package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.ClientRegisterPhotoFragment;
import com.ujobs.app.ui.fragment.ClientRegisterRequestFragment;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientRegisterResquestActivity extends BaseActivity
        implements GetUserView, ClientRegisterRequestFragment.OnClientRegisterRequestFragment,
        ClientRegisterPhotoFragment.OnClientRegisterPhotoFragment {

    private static final String BUNDLE_SUBCATEGORY = "SUBCATEGORY";

    public static Intent getCallingIntent(Context poContext, SubcategoryModel poSubcategoryModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_SUBCATEGORY, poSubcategoryModel);
        Intent loIntent = new Intent(poContext, ClientRegisterResquestActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.frlContainer)
    FrameLayout frlContainer;

    SubcategoryModel goSubcategoryModel;
    GetUserPresenter goGetUserPresenter;
    UserModel goUserModel;

    int giPositionTakePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_request);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        Bundle loBundle = getIntent().getExtras();
        if (loBundle != null) {
            goSubcategoryModel = loBundle.getParcelable(BUNDLE_SUBCATEGORY);
        }

        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }

    @Override
    public void onNextClicked(int piIdRequest) {
        replaceFragment(R.id.frlContainer, ClientRegisterPhotoFragment.newInstance(piIdRequest));
    }

    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        goUserModel = poUserModel;

        addFragment(R.id.frlContainer, ClientRegisterRequestFragment.newInstance(poUserModel, goSubcategoryModel));
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onBackPressed() {
        Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
        String message = "";
//        int count = getSupportFragmentManager().getBackStackEntryCount();
//
//        if (count == 0) {
//            super.onBackPressed();
//        } else {

        if (loFragment instanceof ClientRegisterRequestFragment) {
            message = "Lo ingresado se pederá. ¿Estás seguro de continuar?";
        } else if (loFragment instanceof ClientRegisterPhotoFragment) {
            message = "Se perderán las fotos. ¿Estás seguro de continuar?";
        }
        DialogView.create(context()).showBasicDialog(null, message,
                getString(R.string.yes), new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        finish();
                    }
                }, getString(R.string.not), null);
//        }

    }


    @Override
    public void onTakePhoto(Integer piPosition) {
        giPositionTakePhoto = piPosition;
        alertPhotoOrGallery();

    }

    @Override
    public void onFinish() {
        finish();
    }


    @Override
    public void onSelectPhoto(String path) {
        super.onSelectPhoto(path);
        Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
        if (loFragment instanceof ClientRegisterPhotoFragment) {
            ((ClientRegisterPhotoFragment) loFragment).onPhotoPath(path, giPositionTakePhoto);
        }
    }
}
