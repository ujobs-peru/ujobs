package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.ListRequestPresenter;
import com.ujobs.app.ui.adapter.WorkerRequestAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.ListRequestView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerListRequestFragment extends BaseFragment implements ListRequestView {

    private static final String BUNDLE_USER = "USER";

    public static WorkerListRequestFragment newInstance(UserModel poUserModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        WorkerListRequestFragment loClientListRequestFragment = new WorkerListRequestFragment();
        loClientListRequestFragment.setArguments(loBundle);
        return loClientListRequestFragment;
    }

    public interface OnWorkerListRequestFragment {
        void goOnRequest(RequestModel poRequestModel);

    }

    @BindView(R.id.rvRequest)
    RecyclerView rvRequest;

    Unbinder unbinder;

    private OnWorkerListRequestFragment goOnWorkerListRequestFragment;
    private WorkerRequestAdapter goWorkerRequestAdapter;
    private ListRequestPresenter goListRequestPresenter;
    private UserModel goUserModel;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goListRequestPresenter = ListRequestPresenter.newInstance(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    private void setUpView() {
        goWorkerRequestAdapter = new WorkerRequestAdapter(getContext());
        rvRequest.setHasFixedSize(true);
    }


    private void setupRecyclerView() {
        this.goWorkerRequestAdapter.setOnItemClickListener(onClientItemClickListener);
        LinearLayoutManager loEntityLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        this.rvRequest.setLayoutManager(loEntityLayoutManager);
        this.rvRequest.setAdapter(goWorkerRequestAdapter);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    private void loadListRequest() {
        this.goListRequestPresenter.initialize(goUserModel.getIdUser(), RequestModel.STATE_TYPE_ALL, ListRequestPresenter.TYPE_WORKER);
    }


    private WorkerRequestAdapter.OnItemClickListener onClientItemClickListener =
            new WorkerRequestAdapter.OnItemClickListener() {
                @Override
                public void onClientRequestItemClicked(RequestModel poRequestModel) {
                    goOnWorkerListRequestFragment.goOnRequest(poRequestModel);
                }
            };

    @Override
    public void showListRequestSuccess(List<RequestModel> paoListDistrictModel) {
        goWorkerRequestAdapter.setClientRequestList(paoListDistrictModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadListRequest();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWorkerListRequestFragment) {
            goOnWorkerListRequestFragment = (OnWorkerListRequestFragment) context;
        }
    }


}
