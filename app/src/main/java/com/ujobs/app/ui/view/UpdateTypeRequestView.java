package com.ujobs.app.ui.view;

import com.ujobs.app.model.QuoteModel;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface UpdateTypeRequestView extends LoadDataView {

    void showUpdateTypeRequestSuccess(String poData);
}
