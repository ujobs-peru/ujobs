package com.ujobs.app.ui.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.wdullaer.materialdatetimepicker.date.DateRangeLimiter;

import java.util.Calendar;

/**
 * Created by Manuel Torres on 18/11/17.
 */
public class MyDateRangeLimiter implements DateRangeLimiter {

    public MyDateRangeLimiter() {

    }

    public MyDateRangeLimiter(Parcel in) {

    }

    @Override
    public int getMinYear() {
        return 1900;
    }

    @Override
    public int getMaxYear() {
        return 2100;
    }

    @Override
    public Calendar getStartDate() {
        Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, output.get(Calendar.YEAR));
        output.set(Calendar.DAY_OF_MONTH, output.get(Calendar.DAY_OF_MONTH));
        output.set(Calendar.MONTH, output.get(Calendar.MONTH));
        return output;
    }

    @Override
    public Calendar getEndDate() {
        Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, 2100);
        output.set(Calendar.DAY_OF_MONTH, 1);
        output.set(Calendar.MONTH, Calendar.JANUARY);
        return output;
    }

    @Override
    public boolean isOutOfRange(int year, int month, int day) {
        return false;
    }

    @Override
    public Calendar setToNearestDate(Calendar day) {
        return day;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    public static final Parcelable.Creator<MyDateRangeLimiter> CREATOR
            = new Parcelable.Creator<MyDateRangeLimiter>() {
        public MyDateRangeLimiter createFromParcel(Parcel in) {
            return new MyDateRangeLimiter(in);
        }

        public MyDateRangeLimiter[] newArray(int size) {
            return new MyDateRangeLimiter[size];
        }
    };
}