package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.UpdateTypeRequestPresenter;
import com.ujobs.app.ui.adapter.WorkerQuoteTabAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.util.ImageUtil;
import com.ujobs.app.ui.view.UpdateTypeRequestView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerQuoteFragment extends BaseFragment implements UpdateTypeRequestView {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_REQUEST = "REQUEST";
    private static final String BUNDLE_QUOTE = "QUOTE";


    public static WorkerQuoteFragment newInstance(UserModel poUserModel, RequestModel poRequestModel, QuoteModel poQuoteModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_REQUEST, poRequestModel);
        if (poQuoteModel != null) {
            loBundle.putParcelable(BUNDLE_QUOTE, poQuoteModel);
        }
        WorkerQuoteFragment loWorkerQuoteFragment = new WorkerQuoteFragment();
        loWorkerQuoteFragment.setArguments(loBundle);
        return loWorkerQuoteFragment;
    }

    public interface OnWorkerQuoteFragment {

        void goBack(int piType);

    }

    @BindView(R.id.imgWorker)
    ImageView imgWorker;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvDni)
    TextView tvDni;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvQualify)
    TextView tvQualify;
    @BindView(R.id.tbTabs)
    TabLayout tbTabs;
    @BindView(R.id.vpPager)
    ViewPager vpPager;
    @BindView(R.id.btnAccept)
    Button btnAccept;
    @BindView(R.id.btnCall)
    ImageButton btnCall;

    Unbinder unbinder;

    private OnWorkerQuoteFragment goOnWorkerQuoteFragment;
    private UserModel goUserModel;
    private QuoteModel goQuoteModel;
    private RequestModel goRequestModel;
    private WorkerQuoteTabAdapter goWorkerQuoteTabAdapter;
    private UpdateTypeRequestPresenter goUpdateTypeRequestPresenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goQuoteModel = getArguments().getParcelable(BUNDLE_QUOTE);
        goRequestModel = getArguments().getParcelable(BUNDLE_REQUEST);

        goUpdateTypeRequestPresenter = UpdateTypeRequestPresenter.newInstance(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_quote, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }

    private void setUpView() {

        tvName.setText(goRequestModel.getUser().getFullName());
        tvDni.setText(goRequestModel.getUser().getDniShow());
        tvPhone.setText(goRequestModel.getUser().getPhoneShow());
        tvQualify.setText(goRequestModel.getUser().getQualifyAverageShow());


        switch (goRequestModel.getIdStateType()) {
            case RequestModel.STATE_TYPE_PENDING_WORKER:
                btnAccept.setText("Aceptar solicitud");
                btnAccept.setVisibility(View.VISIBLE);
                break;
            case RequestModel.STATE_TYPE_IN_PROGRESS:
                btnAccept.setText("Finalizar solicitud");
                btnAccept.setVisibility(View.VISIBLE);
                break;
            default:
                btnAccept.setVisibility(View.GONE);
                break;
        }

        if (goRequestModel.getIdStateType() == RequestModel.STATE_TYPE_PENDING_CLIENT) {
            btnCall.setVisibility(View.GONE);
        } else {
            btnCall.setVisibility(View.VISIBLE);
        }

        ImageUtil.getImage(imgWorker, String.valueOf(goRequestModel.getUser().getIdImage()));

        goWorkerQuoteTabAdapter = new WorkerQuoteTabAdapter(getActivity().getSupportFragmentManager(), goUserModel, goRequestModel, goQuoteModel);
        goWorkerQuoteTabAdapter.setTitle(getResources().getStringArray(R.array.worker_quote_tabs));
        vpPager.setAdapter(goWorkerQuoteTabAdapter);
        vpPager.setOffscreenPageLimit(2);
        tbTabs.setupWithViewPager(vpPager);

    }

    public void onPhotoPath(String path, int piPosition) {
        Fragment loFragment = goWorkerQuoteTabAdapter.getFragmentAtPosition(vpPager.getCurrentItem());
        if (loFragment != null && loFragment instanceof WorkerQuoteRegisterDetailFragment) {
            ((WorkerQuoteRegisterDetailFragment) loFragment).onPhotoPath(path, piPosition);
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @OnClick(R.id.btnCall)
    public void onClicBtnCall() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + goUserModel.getPhone()));
        startActivity(callIntent);
    }

    @OnClick(R.id.btnAccept)
    public void onClicBtnAccept() {

        switch (goRequestModel.getIdStateType()) {
            case RequestModel.STATE_TYPE_PENDING_WORKER:
                goUpdateTypeRequestPresenter.initialize(goRequestModel.getIdRequest(),
                        RequestModel.STATE_TYPE_IN_PROGRESS, goQuoteModel.getIdQuote());
                break;
            case RequestModel.STATE_TYPE_IN_PROGRESS:
                goUpdateTypeRequestPresenter.initialize(goRequestModel.getIdRequest(),
                        RequestModel.STATE_TYPE_FINISH, goQuoteModel.getIdQuote());
                break;
            default:
                break;
        }

    }

    @Override
    public void showUpdateTypeRequestSuccess(String poData) {
        DialogView.create(context()).showBasicDialog(null, poData,
                null, new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        goOnWorkerQuoteFragment.goBack(goRequestModel.getIdStateType() + 1);
                    }
                }, false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWorkerQuoteFragment) {
            goOnWorkerQuoteFragment = (OnWorkerQuoteFragment) context;
        }
    }


}
