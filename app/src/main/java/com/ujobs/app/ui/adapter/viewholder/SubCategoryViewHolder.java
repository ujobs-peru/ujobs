package com.ujobs.app.ui.adapter.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.ujobs.app.R;
import com.ujobs.app.model.SubcategoryModel;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class SubCategoryViewHolder extends ChildViewHolder {

    public TextView tvSubCategory;
    public CheckBox ckSelect;

    public SubCategoryViewHolder(View itemView) {
        super(itemView);
        tvSubCategory = itemView.findViewById(R.id.tvSubCategory);
        ckSelect = itemView.findViewById(R.id.ckSelect);
    }

    public void onBind(SubcategoryModel subcategory) {
        tvSubCategory.setText(subcategory.getName());
    }


}
