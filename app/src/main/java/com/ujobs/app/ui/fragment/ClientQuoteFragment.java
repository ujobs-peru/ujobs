package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.UpdateTypeRequestPresenter;
import com.ujobs.app.ui.adapter.ClientQuoteTabAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.util.ImageUtil;
import com.ujobs.app.ui.view.UpdateTypeRequestView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientQuoteFragment extends BaseFragment implements UpdateTypeRequestView {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_REQUEST = "REQUEST";
    private static final String BUNDLE_QUOTE = "QUOTE";


    public static ClientQuoteFragment newInstance(UserModel poUserModel, RequestModel poRequestModel, QuoteModel poQuoteModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_REQUEST, poRequestModel);
        loBundle.putParcelable(BUNDLE_QUOTE, poQuoteModel);
        ClientQuoteFragment loClientQuoteFragment = new ClientQuoteFragment();
        loClientQuoteFragment.setArguments(loBundle);
        return loClientQuoteFragment;
    }


    public interface OnClientQuoteFragment {

        void goBack(int piType);
    }

    @BindView(R.id.imgWorker)
    ImageView imgWorker;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvDni)
    TextView tvDni;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvQualify)
    TextView tvQualify;
    @BindView(R.id.tbTabs)
    TabLayout tbTabs;
    @BindView(R.id.vpPager)
    ViewPager vpPager;
    @BindView(R.id.btnAccept)
    Button btnAccept;
    @BindView(R.id.btnCall)
    ImageButton btnCall;

    Unbinder unbinder;

    private OnClientQuoteFragment goOnClientQuoteFragment;
    private UserModel goUserModel;
    private QuoteModel goQuoteModel;
    private RequestModel goRequestModel;
    private ClientQuoteTabAdapter goClientQuoteTabAdapter;
    private UpdateTypeRequestPresenter goUpdateTypeRequestPresenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goQuoteModel = getArguments().getParcelable(BUNDLE_QUOTE);
        goRequestModel = getArguments().getParcelable(BUNDLE_REQUEST);

        goUpdateTypeRequestPresenter = UpdateTypeRequestPresenter.newInstance(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_quote, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }

    private void setUpView() {

        tvName.setText(goQuoteModel.getWorker().getFullName());
        tvDni.setText(goQuoteModel.getWorker().getDniShow());
        tvPhone.setText(goQuoteModel.getWorker().getPhoneShow());
        tvQualify.setText(goQuoteModel.getWorker().getQualifyAverageShow());

        goClientQuoteTabAdapter = new ClientQuoteTabAdapter(getActivity().getSupportFragmentManager(), goUserModel, goQuoteModel);
        goClientQuoteTabAdapter.setTitle(getResources().getStringArray(R.array.client_quote_tabs));
        vpPager.setAdapter(goClientQuoteTabAdapter);
        vpPager.setOffscreenPageLimit(2);
        tbTabs.setupWithViewPager(vpPager);

        if (goRequestModel.getIdStateType() == RequestModel.STATE_TYPE_PENDING_CLIENT) {
            btnAccept.setVisibility(View.VISIBLE);
            btnCall.setVisibility(View.GONE);
        } else {
            btnAccept.setVisibility(View.GONE);
            btnCall.setVisibility(View.VISIBLE);
        }
        ImageUtil.getImage(imgWorker, String.valueOf(goQuoteModel.getWorker().getIdImage()));
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @OnClick(R.id.btnCall)
    public void onClicBtnCall() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + goUserModel.getPhone()));
        startActivity(callIntent);
    }

    @OnClick(R.id.btnAccept)
    public void onClicBtnAccept() {
        goUpdateTypeRequestPresenter.initialize(goRequestModel.getIdRequest(),
                RequestModel.STATE_TYPE_PENDING_WORKER, goQuoteModel.getIdQuote());
    }

    @Override
    public void showUpdateTypeRequestSuccess(String poData) {
        DialogView.create(context()).showBasicDialog(null, poData,
                null, new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        goOnClientQuoteFragment.goBack(goRequestModel.getIdStateType() + 1);
                    }
                }, false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClientQuoteFragment) {
            goOnClientQuoteFragment = (OnClientQuoteFragment) context;
        }
    }


}
