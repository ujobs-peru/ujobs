package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.ListQuotePresenter;
import com.ujobs.app.presenter.RegisterQualifyPresenter;
import com.ujobs.app.ui.adapter.ImageAdapter;
import com.ujobs.app.ui.adapter.WorkerQuoteAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.ListQuoteView;
import com.ujobs.app.ui.view.RegisterQualifyView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerRequestFragment extends BaseFragment implements ListQuoteView, RegisterQualifyView {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_REQUEST = "REQUEST";

    public static WorkerRequestFragment newInstance(UserModel poUserModel, RequestModel poRequestModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_REQUEST, poRequestModel);
        WorkerRequestFragment loWorkerRequestFragment = new WorkerRequestFragment();
        loWorkerRequestFragment.setArguments(loBundle);
        return loWorkerRequestFragment;
    }

    public interface OnWorkerRequestFragment {
        void goQuote(QuoteModel poQuoteModel);
    }

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvState)
    TextView tvState;
    @BindView(R.id.tvCategoryDescription)
    TextView tvCategoryDescription;
    @BindView(R.id.tvDisrict)
    TextView tvDisrict;
    @BindView(R.id.tvStartDate)
    TextView tvStartDate;
    @BindView(R.id.tvComment)
    TextView tvComment;
    @BindView(R.id.rcvQuote)
    RecyclerView rcvQuote;
    @BindView(R.id.btnQuote)
    Button btnQuote;
    @BindView(R.id.rcvImage)
    RecyclerView rcvImage;


    Unbinder unbinder;

    private OnWorkerRequestFragment goOnWorkerRequestFragment;
    private WorkerQuoteAdapter goWorkerQuoteAdapter;
    private ListQuotePresenter goListQuotePresenter;
    private UserModel goUserModel;
    private RequestModel goRequestModel;
    private ImageAdapter goImageAdapter;
    private RegisterQualifyPresenter goRegisterQualifyPresenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goRegisterQualifyPresenter = RegisterQualifyPresenter.newInstance(this);
        goListQuotePresenter = ListQuotePresenter.newInstance(this);
        goUserModel = (UserModel) getArguments().getParcelable(BUNDLE_USER);
        goRequestModel = (RequestModel) getArguments().getParcelable(BUNDLE_REQUEST);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    private void setUpView() {
        goWorkerQuoteAdapter = new WorkerQuoteAdapter(getContext());
        goImageAdapter = new ImageAdapter(getContext());
        rcvQuote.setHasFixedSize(true);
        rcvImage.setHasFixedSize(true);

        tvName.setText(goRequestModel.getName());
        tvState.setText(goRequestModel.getTitleStateTypeWorker());
        tvCategoryDescription.setText(goRequestModel.getTextCategory());
        tvDisrict.setText(goRequestModel.getDistrictShow());
        tvStartDate.setText(goRequestModel.getStartDateShow());
        tvComment.setText(goRequestModel.getDescription());
    }

    private void setupRecyclerView() {
        this.goWorkerQuoteAdapter.setOnItemClickListener(onWorkerQuoteItemClickListener);
        LinearLayoutManager loEntityLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        this.rcvQuote.setLayoutManager(loEntityLayoutManager);
        this.rcvQuote.setAdapter(goWorkerQuoteAdapter);

        this.goImageAdapter.setOnItemClickListener(onImageClickListener);
        LinearLayoutManager loEntityLayoutHorizontalManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        this.rcvImage.setLayoutManager(loEntityLayoutHorizontalManager);
        this.rcvImage.setAdapter(goImageAdapter);

    }

    public void refreshView(RequestModel poRequestModel) {
        this.goRequestModel = poRequestModel;
        setUpView();
        setupRecyclerView();
        loadListRequest();
        loadListImage();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            this.loadListRequest();
            this.loadListImage();
        }
    }

    private void loadListRequest() {
        this.goListQuotePresenter.initialize(goRequestModel.getIdRequest());
    }

    private void loadListImage() {
        goImageAdapter.setImageList(goRequestModel.getLstImage());
    }

    private WorkerQuoteAdapter.OnItemClickListener onWorkerQuoteItemClickListener =
            new WorkerQuoteAdapter.OnItemClickListener() {
                @Override
                public void onWorkerQuoteItemClicked(QuoteModel poQuoteModel) {
                    goOnWorkerRequestFragment.goQuote(poQuoteModel);
                }
            };

    private ImageAdapter.OnItemClickListener onImageClickListener =
            new ImageAdapter.OnItemClickListener() {
                @Override
                public void onImageItemClicked(Integer poInteger) {

                }
            };

    @Override
    public void showListQuoteSuccess(List<QuoteModel> paoData) {
        if (goRequestModel.getIdStateType() == RequestModel.STATE_TYPE_PENDING_CLIENT) {
            List<QuoteModel> loQuoteModelList = new ArrayList<>();
            for (QuoteModel loQuoteModel : paoData) {
                if (loQuoteModel.getIdWorker() == goUserModel.getIdUser()) {
                    loQuoteModelList.add(loQuoteModel);
                }
            }
            if (loQuoteModelList.isEmpty()) {
                btnQuote.setVisibility(View.VISIBLE);
            }
            goWorkerQuoteAdapter.setWorkerQuoteList(loQuoteModelList);
        } else {
            goWorkerQuoteAdapter.setWorkerQuoteList(paoData);
        }


        if (goRequestModel.getCountQualify() == 0 && goRequestModel.getIdStateType() == RequestModel.STATE_TYPE_FINISH) {
            showComment();
        }
    }

    void showComment() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        final View view = layoutInflater.inflate(R.layout.view_validate, null);

        final CheckBox btnStart1 = ((CheckBox) view.findViewById(R.id.btnStart1));
        final CheckBox btnStart2 = ((CheckBox) view.findViewById(R.id.btnStart2));
        final CheckBox btnStart3 = ((CheckBox) view.findViewById(R.id.btnStart3));
        final CheckBox btnStart4 = ((CheckBox) view.findViewById(R.id.btnStart4));
        final CheckBox btnStart5 = ((CheckBox) view.findViewById(R.id.btnStart5));
        final EditText edtComment = ((EditText) view.findViewById(R.id.edtComment));

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnStart5.setChecked(false);
                btnStart4.setChecked(false);
                btnStart3.setChecked(false);
                btnStart2.setChecked(false);
                btnStart1.setChecked(false);
                switch (v.getId()) {
                    case R.id.btnStart5:
                        btnStart5.setChecked(true);
                    case R.id.btnStart4:
                        btnStart4.setChecked(true);
                    case R.id.btnStart3:
                        btnStart3.setChecked(true);
                    case R.id.btnStart2:
                        btnStart2.setChecked(true);
                    case R.id.btnStart1:
                        btnStart1.setChecked(true);
                        break;
                }
            }
        };

        btnStart1.setOnClickListener(onClickListener);
        btnStart2.setOnClickListener(onClickListener);
        btnStart3.setOnClickListener(onClickListener);
        btnStart4.setOnClickListener(onClickListener);
        btnStart5.setOnClickListener(onClickListener);


        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext(), R.style.AppCompatAlertDialogStyle);
        alertBuilder.setCancelable(false);
        alertBuilder.setView(view);

        alertBuilder.setPositiveButton(getString(R.string.valoration_accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int stars = 0;
                if (btnStart5.isChecked()) {
                    stars = 5;
                } else if (btnStart4.isChecked()) {
                    stars = 4;
                } else if (btnStart3.isChecked()) {
                    stars = 3;
                } else if (btnStart2.isChecked()) {
                    stars = 2;
                } else if (btnStart1.isChecked()) {
                    stars = 1;
                }
                String comment = edtComment.getText().toString();
                goRegisterQualifyPresenter.initialize(goRequestModel.getIdRequest(), stars, comment, true);

            }
        });
        alertBuilder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertBuilder.show();
    }

    @Override
    public void showRegisterQualifySuccess(Integer piId) {

    }


    @OnClick(R.id.btnQuote)
    public void onViewClicked() {
        goOnWorkerRequestFragment.goQuote(null);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWorkerRequestFragment) {
            goOnWorkerRequestFragment = (OnWorkerRequestFragment) context;
        }
    }


}
