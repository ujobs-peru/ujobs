package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.entity.mapper.CategoryModelToMapper;
import com.ujobs.app.data.entity.mapper.CategoryToMapper;
import com.ujobs.app.data.entity.mapper.DistrictToMapper;
import com.ujobs.app.data.entity.mapper.SubcategoryToMapper;
import com.ujobs.app.data.entity.mapper.UserToMapper;
import com.ujobs.app.data.repository.datasource.UserDataStore;
import com.ujobs.app.data.repository.datasource.UserSharedDataStore;
import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.model.RegisterWorkerModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.presenter.RegisterWorkerPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.ClientRegisterPhotoFragment;
import com.ujobs.app.ui.fragment.ClientRegisterRequestFragment;
import com.ujobs.app.ui.fragment.WorkerCategoryFragment;
import com.ujobs.app.ui.fragment.WorkerDistrictFragment;
import com.ujobs.app.ui.view.GetUserView;
import com.ujobs.app.ui.view.RegisterWorkerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerRegisterActivity extends BaseActivity
        implements GetUserView, WorkerCategoryFragment.OnWorkerCategoryFragment,
        WorkerDistrictFragment.OnWorkerDistrictFragment, RegisterWorkerView {

    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, WorkerRegisterActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.frlContainer)
    FrameLayout frlContainer;

    List<SubcategoryModel> goLstSubcategoryModel;
    GetUserPresenter goGetUserPresenter;
    RegisterWorkerPresenter goRegisterWorkerPresenter;
    UserModel goUserModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_register);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        Bundle loBundle = getIntent().getExtras();
        if (loBundle != null) {
        }

        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goRegisterWorkerPresenter = RegisterWorkerPresenter.newInstance(this);

        goGetUserPresenter.initialize();
    }

    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        goUserModel = poUserModel;
        addFragment(R.id.frlContainer, WorkerCategoryFragment.newInstance(goUserModel));
    }

    @Override
    public void onNext(List<SubcategoryModel> poListSubcategortyModel) {
        if (poListSubcategortyModel.isEmpty()) {
            showError("Debes seleccionar una categoría para continuar");
            return;
        }
        goLstSubcategoryModel = poListSubcategortyModel;
        replaceFragment(R.id.frlContainer, WorkerDistrictFragment.newInstance(goUserModel));
    }

    @Override
    public void onSave(List<DistrictModel> paoDistrictModel) {
        if (paoDistrictModel.isEmpty()) {
            showError("Debes seleccionar un distrito para continuar");
            return;
        }

        List<Integer> laoListIdCatAndSubcat = new ArrayList<>();
        List<Integer> laoListIdDistrict = new ArrayList<>();

        for (SubcategoryModel loSubcategoryModel : goLstSubcategoryModel) {
            laoListIdCatAndSubcat.add(loSubcategoryModel.getIdCatSubcategory());
        }
        for (DistrictModel loDistrictModel : paoDistrictModel) {
            laoListIdDistrict.add(loDistrictModel.getIdDistrict());
        }


        RegisterWorkerModel loRegisterWorkerModel = new RegisterWorkerModel();
        loRegisterWorkerModel.setIdWorker(goUserModel.getIdUser());
        loRegisterWorkerModel.setListIdsCatAndSubcat(laoListIdCatAndSubcat);
        loRegisterWorkerModel.setListIdsDistrict(laoListIdDistrict);
        goRegisterWorkerPresenter.initialize(loRegisterWorkerModel);
    }

    @Override
    public void showRegisterWorkerSuccess(String psMessage) {
        DialogView.create(this).showBasicDialog(null, psMessage, null, new DialogView.OnSingleButton() {
            @Override
            public void onClick(@NonNull MaterialDialog poDialog) {
                navigator.navigateToWorker();
                finish();
            }
        }, false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
//        String message = "";
//        int count = getSupportFragmentManager().getBackStackEntryCount();
//
//        if (count == 0) {
//            super.onBackPressed();
//        } else {
//
//        if (loFragment instanceof ClientRegisterRequestFragment) {
//            message = "Lo ingresado se pederá. ¿Estás seguro de continuar?";
//        } else if (loFragment instanceof ClientRegisterPhotoFragment) {
//            message = "Se perderán las fotos. ¿Estás seguro de continuar?";
//        }
//        DialogView.create(context()).showBasicDialog(null, "Se perderán los datos seleccionados. ¿Estás seguro de continuar?",
//                getString(R.string.yes), new DialogView.OnSingleButton() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog poDialog) {
//                        finish();
//                    }
//                }, getString(R.string.not), null);
//        }
    }


}
