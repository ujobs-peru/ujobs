package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ujobs.app.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class SaveImageAdapter extends RecyclerView.Adapter<SaveImageAdapter.SaveImageViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link String}.
         *
         * @param poString {@link String}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onImageItemClicked(String poString);

        void onTakeImageItemClicked(int piPosition);
    }

    private List<String> gaoString;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link SaveImageAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public SaveImageAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoString = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoString != null) ? this.gaoString.size() : 0;
    }

    @Override
    public SaveImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_save_image, parent, false);
        return new SaveImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SaveImageViewHolder holder, final int position) {
        final String loString = this.gaoString.get(position);
//        holder.imgPhoto.setTag(1, loString);
        holder.btnPhoto.setTag(position);
        if (TextUtils.isEmpty(loString)) {
            holder.imgPhoto.setVisibility(View.GONE);
            holder.btnPhoto.setVisibility(View.VISIBLE);
        } else {
            holder.imgPhoto.setVisibility(View.VISIBLE);
            holder.btnPhoto.setVisibility(View.GONE);
            Glide.with(goContext).load(loString).into(holder.imgPhoto);
        }
        holder.btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveImageAdapter.this.onItemClickListener != null) {
                    SaveImageAdapter.this.onItemClickListener.onTakeImageItemClicked((int) v.getTag());
                }
            }
        });
        holder.imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SaveImageAdapter.this.onItemClickListener != null) {
//                    SaveImageAdapter.this.onItemClickListener.onImageItemClicked((String) v.getTag(1));
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link String}.
     *
     * @param poStringCollection List of {@link String}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setImageList(List<String> poStringCollection) {
        if (validateImageList(poStringCollection)) {
            this.gaoString = poStringCollection;
            this.notifyDataSetChanged();
        }
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link String}
     *
     * @param poStringCollection List of {@link String}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private boolean validateImageList(List<String> poStringCollection) {
        if (poStringCollection == null) {
            return false;
        }
        return true;
    }


    public void onPhotoPath(String path, int piPosition) {
        gaoString.set(piPosition, path);
        notifyDataSetChanged();
    }

    public List<String> getImages() {
        List<String> lasPath = new ArrayList<>();
        for (String lsPath : gaoString) {
            if (!TextUtils.isEmpty(lsPath)) {
                lasPath.add(lsPath);
            }
        }
        return lasPath;
    }

    static class SaveImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgPhoto)
        ImageView imgPhoto;
        @BindView(R.id.btnPhoto)
        ImageButton btnPhoto;


        /**
         * Constructs a {@link SaveImageViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        SaveImageViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
