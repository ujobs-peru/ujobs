package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ujobs.app.R;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.presenter.ListCatAndSubcatPresenter;
import com.ujobs.app.ui.adapter.CategoryAdapter;
import com.ujobs.app.ui.view.ListCatAndSubcatView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientSearchFragment extends BaseFragment implements ListCatAndSubcatView, CategoryAdapter.OnItemClickListener {


    //@BindView(R.id.edtField)
    //EditText edtField;
    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    Unbinder unbinder;

    private OnSearchClientFragment goSearchClientFragment;

    ListCatAndSubcatPresenter goListCatPresenter;


    public interface  OnSearchClientFragment{
        void onSubCategorySelected(SubcategoryModel subcategory);
    }

    public static ClientSearchFragment newInstance() {
        return new ClientSearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_search, container,false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }

    private void setUpView(){
        goListCatPresenter = ListCatAndSubcatPresenter.newInstance(this);
        goListCatPresenter.initialize();
    }


    @Override
    public void showListCatAndSubcatSuccess(List<CategoryModel> paoListCategoryModel) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        CategoryAdapter adapter = new CategoryAdapter(paoListCategoryModel,this);
        rvCategory.setLayoutManager(layoutManager);
        rvCategory.setAdapter(adapter);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @Override
    public Context context() {
        return getContext();
    }


    @Override
    public void onSubCategoryClicked(SubcategoryModel subcategory) {
        goSearchClientFragment.onSubCategorySelected(subcategory);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchClientFragment){
            goSearchClientFragment = (OnSearchClientFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
