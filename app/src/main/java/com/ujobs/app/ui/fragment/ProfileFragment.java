package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.UpdateTypeRequestPresenter;
import com.ujobs.app.ui.util.ImageUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ProfileFragment extends BaseFragment {

    private static final String BUNDLE_USER = "USER";

    public static ProfileFragment newInstance(UserModel poUserModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        ProfileFragment loProfileFragment = new ProfileFragment();
        loProfileFragment.setArguments(loBundle);
        return loProfileFragment;
    }

    public interface OnProfileFragment {
    }

    private UserModel goUserModel;

    @BindView(R.id.imgWorker)
    ImageView imgWorker;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvLastName)
    TextView tvLastName;
    @BindView(R.id.tvSurName)
    TextView tvSurName;
    @BindView(R.id.tvDni)
    TextView tvDni;
    @BindView(R.id.tvPhone)
    TextView tvPhone;

    Unbinder unbinder;
    private OnProfileFragment goProfileFragment;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = (UserModel) getArguments().getParcelable(BUNDLE_USER);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }


    private void setUpView() {

        tvEmail.setText(goUserModel.getEmail());
        tvName.setText(goUserModel.getNames());
        tvLastName.setText(goUserModel.getLastName());
        tvSurName.setText(goUserModel.getMotherLastName());
        tvDni.setText(goUserModel.getDni());
        tvPhone.setText(goUserModel.getPhone());
        ImageUtil.getImage(imgWorker, String.valueOf(goUserModel.getIdImage()));


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragment) {
            goProfileFragment = (OnProfileFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
