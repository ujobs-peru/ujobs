package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.RegisterQuotePresenter;
import com.ujobs.app.presenter.UploadFilePresenter;
import com.ujobs.app.ui.adapter.SaveImageAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.RegisterQuoteView;
import com.ujobs.app.ui.view.UploadFileView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerQuoteRegisterDetailFragment extends BaseFragment implements RegisterQuoteView, UploadFileView {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_RUEST = "BUNDLE_RUEST";


    public static WorkerQuoteRegisterDetailFragment newInstance(UserModel poUserModel, RequestModel goRequestModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_RUEST, goRequestModel);
        WorkerQuoteRegisterDetailFragment loClientListRequestFragment = new WorkerQuoteRegisterDetailFragment();
        loClientListRequestFragment.setArguments(loBundle);
        return loClientListRequestFragment;
    }


    public interface OnWorkerQuoteRegisterDetailFragment {
        void onTakePhoto(Integer piPosition);

        void onFinish();
    }

    @BindView(R.id.edtAmout)
    EditText edtAmout;
    @BindView(R.id.edtDescription)
    EditText edtDescription;
    @BindView(R.id.rcvImage)
    RecyclerView rcvImage;

    Unbinder unbinder;

    private OnWorkerQuoteRegisterDetailFragment goOnWorkerQuoteRegisterDetailFragment;
    private SaveImageAdapter goSaveImageAdapter;
    private UserModel goUserModel;
    private RequestModel goRequestModel;
    private RegisterQuotePresenter goRegisterQuotePresenter;
    private UploadFilePresenter goUploadFilePresenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WorkerQuoteRegisterDetailFragment.OnWorkerQuoteRegisterDetailFragment) {
            goOnWorkerQuoteRegisterDetailFragment = (WorkerQuoteRegisterDetailFragment.OnWorkerQuoteRegisterDetailFragment) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goRequestModel = getArguments().getParcelable(BUNDLE_RUEST);

        goRegisterQuotePresenter = RegisterQuotePresenter.newInstance(this);
        goUploadFilePresenter = UploadFilePresenter.newInstance(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_quote_register_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    private void setUpView() {
        goSaveImageAdapter = new SaveImageAdapter(getContext());
        rcvImage.setHasFixedSize(true);

    }

    private void setupRecyclerView() {
        this.goSaveImageAdapter.setOnItemClickListener(onSaveImageAdapterClickListener);
        this.rcvImage.setLayoutManager(new GridLayoutManager(getContext(), 2));
        this.rcvImage.setAdapter(goSaveImageAdapter);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            this.loadListImages();
        }
    }


    private void loadListImages() {
//        goImageAdapter.setImageList(goRequestModel.getLstIdsImage());
        List<String> lsImages = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            lsImages.add("");
        }

        goSaveImageAdapter.setImageList(lsImages);
    }

    @OnClick(R.id.btnCotizar)
    public void onViewClicked() {
        String txtAmount = edtAmout.getText().toString();
        String txtDetail = edtDescription.getText().toString();

        goRegisterQuotePresenter.initialize(goUserModel.getIdUser(), goRequestModel.getIdRequest(), txtAmount, txtDetail);
    }

    public void onPhotoPath(String path, int piPosition) {
        goSaveImageAdapter.onPhotoPath(path, piPosition);
    }

    @Override
    public void showRegisterQuoteSuccess(Integer piId) {
        List<String> pathImages = goSaveImageAdapter.getImages();
        if (pathImages.isEmpty()) {
            DialogView.create(getContext()).showBasicDialog(null, "Debes agregar una foto.");
        } else {
            goUploadFilePresenter.initialize(piId, UploadFilePresenter.TYPE_QUOTE, pathImages);
        }
    }

    @Override
    public void showUploadFileSuccess(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage,
                null, new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        goOnWorkerQuoteRegisterDetailFragment.onFinish();
                    }
                }, false);
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return getContext();
    }

    private SaveImageAdapter.OnItemClickListener onSaveImageAdapterClickListener =
            new SaveImageAdapter.OnItemClickListener() {
                @Override
                public void onImageItemClicked(String poString) {

                }

                @Override
                public void onTakeImageItemClicked(int piPosition) {
                    goOnWorkerQuoteRegisterDetailFragment.onTakePhoto(piPosition);
                }
            };
}
