package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.ui.util.ImageUtil;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class ClientQuoteAdapter extends RecyclerView.Adapter<ClientQuoteAdapter.ClientQuoteViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link QuoteModel}.
         *
         * @param poQuoteModel {@link QuoteModel}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onClientQuoteItemClicked(QuoteModel poQuoteModel);
    }

    private List<QuoteModel> gaoQuoteModel;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link ClientQuoteAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public ClientQuoteAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoQuoteModel = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoQuoteModel != null) ? this.gaoQuoteModel.size() : 0;
    }

    @Override
    public ClientQuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_client_quote, parent, false);
        return new ClientQuoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClientQuoteViewHolder holder, final int position) {
        final QuoteModel loQuoteModel = this.gaoQuoteModel.get(position);
        ImageUtil.getImage(holder.imgWorker, String.valueOf(loQuoteModel.getWorker().getIdImage()));
        holder.tvName.setText(loQuoteModel.getWorker().getFullName());
        holder.tvAmount.setText(loQuoteModel.getAmountShow());
        holder.tvQualify.setText(loQuoteModel.getWorker().getQualifyAverageShow());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClientQuoteAdapter.this.onItemClickListener != null) {
                    ClientQuoteAdapter.this.onItemClickListener.onClientQuoteItemClicked(loQuoteModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link QuoteModel}.
     *
     * @param poQuoteModelCollection List of {@link QuoteModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setClientQuoteList(List<QuoteModel> poQuoteModelCollection) {
        this.validateClientQuoteList(poQuoteModelCollection);
        this.gaoQuoteModel = poQuoteModelCollection;
        this.notifyDataSetChanged();
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link QuoteModel}
     *
     * @param poQuoteModelCollection List of {@link QuoteModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private void validateClientQuoteList(List<QuoteModel> poQuoteModelCollection) {
        if (poQuoteModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }


    static class ClientQuoteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgWorker)
        ImageView imgWorker;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvQualify)
        TextView tvQualify;

        /**
         * Constructs a {@link ClientQuoteViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        ClientQuoteViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
