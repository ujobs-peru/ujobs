package com.ujobs.app.ui.view;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface RegisterRequestView extends LoadDataView {

    void showRegisterRequestSuccess(Integer piIdRequest);
}
