package com.ujobs.app.ui.view;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface RegisterQuoteView extends LoadDataView {

    void showRegisterQuoteSuccess(Integer piId);
}
