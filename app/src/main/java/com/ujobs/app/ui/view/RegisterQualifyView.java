package com.ujobs.app.ui.view;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface RegisterQualifyView extends LoadDataView {

    void showRegisterQualifySuccess(Integer piId);
}
