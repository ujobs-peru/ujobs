package com.ujobs.app.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;

import com.ujobs.app.R;

/**
 * Created by Manuel Torres on 9/11/17.
 */

public class SplashActivity extends BaseActivity {


    private static final String TAG = SplashActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpView();
    }

    private void setUpView() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashActivity.this.navigator.navigateToTypeLogin();
            }
        }, getResources().getInteger(R.integer.splash_time_second) * DateUtils.SECOND_IN_MILLIS);


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
