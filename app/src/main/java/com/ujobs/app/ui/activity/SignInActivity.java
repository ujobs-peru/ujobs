package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.ujobs.app.R;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.SignInPresenter;
import com.ujobs.app.presenter.UploadFilePresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.SignInView;
import com.ujobs.app.ui.view.UploadFileView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.anderscheow.validator.Validation;
import io.github.anderscheow.validator.Validator;
import io.github.anderscheow.validator.constant.Mode;
import io.github.anderscheow.validator.rules.common.NotEmptyRule;
import io.github.anderscheow.validator.rules.regex.EmailRule;

public class SignInActivity extends BaseActivity implements SignInView, UploadFileView {


    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, SignInActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.btnPhoto)
    ImageButton btnPhoto;
    @BindView(R.id.imgPhoto)
    ImageView imgPhoto;


    @BindView(R.id.edtEmailLayout)
    TextInputLayout edtEmailLayout;
    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.edtPasswordLayout)
    TextInputLayout edtPasswordLayout;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;

    @BindView(R.id.edtPasswordConfirmationLayout)
    TextInputLayout edtPasswordConfirmationLayout;
    @BindView(R.id.edtPasswordConfirmation)
    TextInputEditText edtPasswordConfirmation;

    @BindView(R.id.edtNameLayout)
    TextInputLayout edtNameLayout;
    @BindView(R.id.edtName)
    TextInputEditText edtName;

    @BindView(R.id.edtLastNameLayout)
    TextInputLayout edtLastNameLayout;
    @BindView(R.id.edtLastName)
    TextInputEditText edtLastName;

    @BindView(R.id.edtSurNameLayout)
    TextInputLayout edtSurNameLayout;
    @BindView(R.id.edtSurName)
    TextInputEditText edtSurName;

    @BindView(R.id.edtDniLayout)
    TextInputLayout edtDniLayout;
    @BindView(R.id.edtDni)
    TextInputEditText edtDni;

    @BindView(R.id.edtPhoneLayout)
    TextInputLayout edtPhoneLayout;
    @BindView(R.id.edtPhone)
    TextInputEditText edtPhone;

    SignInPresenter goSignInPresenter;
    UploadFilePresenter goUploadFilePresenter;
    String pathImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);

        setUpView();
    }

    private void setUpView() {
        goSignInPresenter = SignInPresenter.newInstance(this);
        goUploadFilePresenter = UploadFilePresenter.newInstance(this);

        Bundle loBundle = getIntent().getExtras();

    }


    @OnClick(R.id.btnSignIn)
    public void onViewClicked() {

        if (TextUtils.isEmpty(pathImage)) {
            DialogView.create(context()).showBasicDialog(null, "Debe agregar una foto");
            return;
        }

        final Validation emailValidation = new Validation(edtEmailLayout)
                .and(new EmailRule(R.string.validate_email_format));

        final Validation passwordValidation = new Validation(edtPasswordLayout)
                .and(new NotEmptyRule(R.string.validate_password_notempty));

        final Validation passwordConfirmationValidation = new Validation(edtPasswordConfirmationLayout)
                .and(new NotEmptyRule(R.string.validate_passwordconfirmation_notempty));

        final Validation nameValidation = new Validation(edtNameLayout)
                .and(new NotEmptyRule(R.string.validate_name_notempty));

        final Validation lastNameValidation = new Validation(edtLastNameLayout)
                .and(new NotEmptyRule(R.string.validate_lastname_notempty));

        final Validation surNameValidation = new Validation(edtLastNameLayout)
                .and(new NotEmptyRule(R.string.validate_surname_notempty));

        final Validation dniValidation = new Validation(edtLastNameLayout)
                .and(new NotEmptyRule(R.string.validate_dni_notempty));

        final Validation phoneValidation = new Validation(edtLastNameLayout)
                .and(new NotEmptyRule(R.string.validate_phone_notempty));


        Validator.with(getApplicationContext())
                .setMode(Mode.SINGLE)
                .validate(new Validator.OnValidateListener() {
                              @Override
                              public void onValidateSuccess(List<String> values) throws IndexOutOfBoundsException {
                                  String txtEmail = edtEmail.getText().toString();
                                  String txtPassword = edtPassword.getText().toString();
                                  String txtName = edtName.getText().toString();
                                  String txtLastName = edtLastName.getText().toString();
                                  String txtSurName = edtSurName.getText().toString();
                                  String txtDni = edtDni.getText().toString();
                                  String txtTelephone = edtPhone.getText().toString();

                                  UserModel loUserModel = new UserModel();
                                  loUserModel.setEmail(txtEmail);
                                  loUserModel.setPassword(txtPassword);
                                  loUserModel.setDni(txtDni);
                                  loUserModel.setNames(txtName);
                                  loUserModel.setLastName(txtLastName);
                                  loUserModel.setMotherLastName(txtSurName);
                                  loUserModel.setPhone(txtTelephone);
                                  goSignInPresenter.initialize(loUserModel);
                              }

                              @Override
                              public void onValidateFailed() {

                              }
                          },
                        emailValidation, passwordValidation, passwordConfirmationValidation,
                        nameValidation, lastNameValidation, surNameValidation, dniValidation,
                        phoneValidation
                );


    }


    @OnClick(R.id.btnPhoto)
    public void onBtnPhotoClicked() {
        alertPhotoOrGallery();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void showSignInSuccess(Integer pidId) {
        List<String> pathImages = new ArrayList<>();
        pathImages.add(pathImage);
        goUploadFilePresenter.initialize(pidId, UploadFilePresenter.TYPE_USER, pathImages);
    }

    @Override
    public void showUploadFileSuccess(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage, null, new DialogView.OnSingleButton() {
            @Override
            public void onClick(@NonNull MaterialDialog poDialog) {
                finish();
            }
        }, false);
    }

    @Override
    public void onSelectPhoto(String path) {
        super.onSelectPhoto(path);
        File file = new File(path);
        pathImage = path;
        btnPhoto.setVisibility(View.GONE);
        imgPhoto.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(path)
                .into(imgPhoto);
    }


}
