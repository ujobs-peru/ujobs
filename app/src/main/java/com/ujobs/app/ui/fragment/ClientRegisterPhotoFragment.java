package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.presenter.UploadFilePresenter;
import com.ujobs.app.ui.adapter.SaveImageAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.UploadFileView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.http.PATCH;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientRegisterPhotoFragment extends BaseFragment implements UploadFileView {

    private static final String TAG = ClientRegisterPhotoFragment.class.getSimpleName();
    private static final String BUNDLE_ID = "ID";

    public static ClientRegisterPhotoFragment newInstance(int piId) {
        Bundle loBundle = new Bundle();
        loBundle.putInt(BUNDLE_ID, piId);
        ClientRegisterPhotoFragment loClientRegisterPhotoFragment = new ClientRegisterPhotoFragment();
        loClientRegisterPhotoFragment.setArguments(loBundle);
        return loClientRegisterPhotoFragment;
    }


    public interface OnClientRegisterPhotoFragment {
        void onTakePhoto(Integer piPosition);

        void onFinish();

    }

    @BindView(R.id.rvGalery)
    RecyclerView rvGalery;
    @BindView(R.id.btnSend)
    Button btnSend;
    Unbinder unbinder;

    private OnClientRegisterPhotoFragment onClientRegisterPhotoFragment;
    private int goId;
    private SaveImageAdapter goSaveImageAdapter;
    private UploadFilePresenter goUploadFilePresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUploadFilePresenter = UploadFilePresenter.newInstance(this);
        goId = getArguments().getInt(BUNDLE_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<String> lsImages = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            lsImages.add("");
        }

        goSaveImageAdapter.setImageList(lsImages);
    }

    private void setUpView() {
        goSaveImageAdapter = new SaveImageAdapter(getContext());
        rvGalery.setHasFixedSize(true);

    }

    private void setupRecyclerView() {
        this.goSaveImageAdapter.setOnItemClickListener(onSaveImageAdapterClickListener);
        this.rvGalery.setLayoutManager(new GridLayoutManager(getContext(), 2));
        this.rvGalery.setAdapter(goSaveImageAdapter);

    }

    @OnClick(R.id.btnSend)
    public void onViewClicked() {

        List<String> pathImages = goSaveImageAdapter.getImages();
        if (pathImages.isEmpty()) {
            DialogView.create(getContext()).showBasicDialog(null, "Debes agregar una foto.");
        } else {
            goUploadFilePresenter.initialize(goId, UploadFilePresenter.TYPE_REQUEST, pathImages);
        }


    }

    @Override
    public void showUploadFileSuccess(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage,
                null, new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        onClientRegisterPhotoFragment.onFinish();
                    }
                }, false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @Override
    public Context context() {
        return getContext();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClientRegisterPhotoFragment) {
            onClientRegisterPhotoFragment = (OnClientRegisterPhotoFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void onPhotoPath(String path, int piPosition) {
        goSaveImageAdapter.onPhotoPath(path, piPosition);
    }

    private SaveImageAdapter.OnItemClickListener onSaveImageAdapterClickListener =
            new SaveImageAdapter.OnItemClickListener() {
                @Override
                public void onImageItemClicked(String poString) {

                }

                @Override
                public void onTakeImageItemClicked(int piPosition) {
                    onClientRegisterPhotoFragment.onTakePhoto(piPosition);
                }
            };

}
