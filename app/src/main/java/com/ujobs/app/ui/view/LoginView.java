package com.ujobs.app.ui.view;

import com.ujobs.app.model.UserModel;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface LoginView extends LoadDataView {

    void showLoginSuccess(UserModel poUserModel);
}
