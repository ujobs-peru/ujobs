package com.ujobs.app.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.BuildConfig;
import com.ujobs.app.R;
import com.ujobs.app.navigation.Navigator;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.util.RealPathUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Manuel Torres on 11/01/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private static final int REQUEST_PERMISSION_SETTING = 121;
    public static final int REQUEST_IMAGE_CAPTURE = 122;
    public static final int REQUEST_IMAGE_GALLERY = 123;

    public Navigator navigator;
    private ProgressDialog goProgressDialog;

    private int requestcode;
    private List<String> pendingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        navigator = new Navigator(this);
        super.onCreate(savedInstanceState);
    }


    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    public void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Replace a {@link Fragment} to this activity's layout.
     *
     * @param piContainerViewId The container view to where replace the fragment.
     * @param poFragment        The fragment to be replaced.
     */
    public void replaceFragment(int piContainerViewId, Fragment poFragment) {
        replaceFragment(piContainerViewId, poFragment, null);

    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param fragment The fragment to be added.
     */
    public void deleteFragment(Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }


    /**
     * Replace a {@link Fragment} to this activity's layout.
     *
     * @param piContainerViewId The container view to where replace the fragment.
     * @param poFragment        The fragment to be replaced.
     * @param psAddToBackStack  An optional name for this back stack state, or null.
     */
    protected void replaceFragment(int piContainerViewId, Fragment poFragment, String psAddToBackStack) {
        FragmentTransaction loTransaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
//        loTransaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left,
//                android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        loTransaction.replace(piContainerViewId, poFragment);
        loTransaction.addToBackStack(psAddToBackStack);
        loTransaction.commit();
    }


    public void showLoading() {
        if (goProgressDialog == null) {
            goProgressDialog = new ProgressDialog(BaseActivity.this, R.style.AppTheme_ProgressBar) {
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.view_progress);
                    getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                }
            };
            goProgressDialog.setCancelable(false);
        }
        if (!goProgressDialog.isShowing()) {
            goProgressDialog.show();
        }

    }

    public void hideLoading() {
        if (goProgressDialog != null && goProgressDialog.isShowing()) {
            goProgressDialog.dismiss();
        }
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        final List<String> lasPermissionPending = new ArrayList<>();
        List<String> lasPendingTaskAgain = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                lasPermissionPending.add(permissions[i]);

                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                    lasPendingTaskAgain.add(permissions[i]);
                }
            }

        }

        if (!lasPendingTaskAgain.isEmpty()) {
            //guardar pendientes

            DialogView.create(BaseActivity.this)
                    .showBasicDialog(null, "Usted ha cancelado la solicitud de permisos que es necesario en la aplicación. Para ingresar debe aceptar los permisos desde configuracion.",
                            null, new DialogView.OnSingleButton() {
                                @Override
                                public void onClick(@NonNull MaterialDialog poDialog) {
                                    BaseActivity.this.pendingList = lasPermissionPending;
                                    BaseActivity.this.requestcode = requestCode;
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                                }
                            }, getString(R.string.cancel), null);
            return;
        }

        if (lasPermissionPending.isEmpty()) {
            if (Navigator.Type.values()[requestCode].equals(Navigator.Type.LOGIN)) {
                finish();
            }
            Navigator.Type.values()[requestCode].go(BaseActivity.this, navigator);
        } else {
            DialogView.create(BaseActivity.this)
                    .showBasicDialog(null, "Debe aceptar los permisos para continuar", null,
                            new DialogView.OnSingleButton() {
                                @Override
                                public void onClick(@NonNull MaterialDialog poDialog) {
                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                        return;
                                    }
                                    navigator.mayPermission(BaseActivity.this, lasPermissionPending, requestCode);
                                }
                            }, getString(R.string.cancel), null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_PERMISSION_SETTING:
                // load tasks from preference
                Navigator.Type.values()[BaseActivity.this.requestcode].go(this, navigator);
                break;
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == Activity.RESULT_OK) {
                    captureImageResult(data);
                }
                break;
            case REQUEST_IMAGE_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    selectFromGalleryResult(data);
                }
                break;
        }

    }

    public void alertPhotoOrGallery() {
        DialogView.create(this).showBasicDialog(null, "¿Donde quiere tomar la foto?",
                "Galería", new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        dispatchGalleryIntent();
                    }
                }, "Tomar foto", new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        dispatchTakePictureIntent();
                    }
                });
    }

    Uri imageUri;
    String mCurrentPhotoPath;

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider",
                createImageFile());
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    private void dispatchGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
    }


    private void selectFromGalleryResult(Intent data) {
        String realPath = "";
        if (Build.VERSION.SDK_INT < 11)
            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

            // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19)
            realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

            // SDK > 19 (Android 4.4)
        else
            realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());


        onSelectPhoto(realPath);

    }

    private void captureImageResult(Intent data) {

        onSelectPhoto(mCurrentPhotoPath);


    }

    public String getImagePNG(String filenameJPEG) {
        String fileNamePNG = "";
        try {
            fileNamePNG = createImageFile().getAbsolutePath();
            Bitmap bitmap = BitmapFactory.decodeFile(filenameJPEG);
            FileOutputStream out = new FileOutputStream(fileNamePNG);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); //100-best quality
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileNamePNG;
    }

    public void onSelectPhoto(String path) {

    }

}