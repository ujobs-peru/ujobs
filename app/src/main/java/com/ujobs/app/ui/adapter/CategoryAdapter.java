package com.ujobs.app.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.ujobs.app.R;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.ui.adapter.viewholder.CategoryViewHolder;
import com.ujobs.app.ui.adapter.viewholder.SubCategoryViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class CategoryAdapter extends ExpandableRecyclerViewAdapter<CategoryViewHolder, SubCategoryViewHolder> {


    private OnItemClickListener mListener;
    private int giVisibilityCheck = View.GONE;
    private List<Integer> listSelected = new ArrayList<Integer>();

    public interface OnItemClickListener {
        void onSubCategoryClicked(SubcategoryModel subcategory);
    }

    public CategoryAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    public CategoryAdapter(List<? extends ExpandableGroup> groups, OnItemClickListener mListener) {
        super(groups);
        this.mListener = mListener;
    }

    public CategoryAdapter(List<? extends ExpandableGroup> groups, OnItemClickListener mListener, int piVisibilityCheck, List<Integer> listSelected) {
        super(groups);
        this.mListener = mListener;
        this.giVisibilityCheck = piVisibilityCheck;
        this.listSelected = listSelected;
    }

    @Override
    public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public SubCategoryViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sub_category, parent, false);
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SubCategoryViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final SubcategoryModel subCategory = ((CategoryModel) group).getItems().get(childIndex);
        holder.tvSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSubCategoryClicked(subCategory);
            }
        });
        holder.ckSelect.setOnCheckedChangeListener(null);
        holder.ckSelect.setChecked(isSelected(subCategory.getIdCatSubcategory()));
        holder.ckSelect.setTag(subCategory);
        holder.ckSelect.setVisibility(giVisibilityCheck);
        holder.ckSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SubcategoryModel loSubcategoryModel = (SubcategoryModel) compoundButton.getTag();
//                loSubcategoryModel.setCheck(b);
                changeSelected(loSubcategoryModel.getIdCatSubcategory(), b);
            }
        });
        holder.onBind(subCategory);
    }

    @Override
    public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition, ExpandableGroup group) {
        final CategoryModel category = (CategoryModel) group;
        holder.setCategory(category);
    }

    public List<SubcategoryModel> getSelected() {
        List<SubcategoryModel> laoSubcategoryModel = new ArrayList<>();
        List<CategoryModel> laoCategoryModel = (List<CategoryModel>) expandableList.groups;
        for (CategoryModel loCategoryModel : laoCategoryModel) {
            for (SubcategoryModel loSubcategoryModel : loCategoryModel.getListSubcategories()) {
                if (isSelected(loSubcategoryModel.getIdCatSubcategory())) {
                    laoSubcategoryModel.add(loSubcategoryModel);
                }
            }
        }
        return laoSubcategoryModel;
    }


    private boolean isSelected(int piId) {
        boolean exist = false;
        for (Integer data : listSelected) {
            if (data == piId) {
                exist = true;
            }
        }
        return exist;
    }

    private void changeSelected(int piId, boolean b) {

        boolean exist = false;
        int count = -1;
        for (Integer data : listSelected) {
            count++;
            if (data == piId) {
                exist = true;
                break;
            }
        }
        if (b) {
            if (!exist) {
                listSelected.add(piId);
            }
        } else {
            if (exist) {
                listSelected.remove(count);
            }
        }
    }

}


