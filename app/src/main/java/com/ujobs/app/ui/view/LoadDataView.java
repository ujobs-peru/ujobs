/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.ujobs.app.ui.view;

import android.content.Context;

/**
 * Interface representing a View that will use to load data.
 * <p>
 * Created by Manuel Torres on 10/01/2017.
 */
public interface LoadDataView {
    /**
     * Show a view with a progress bar indicating a loading process.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void showLoading();

    /**
     * Hide a loading view.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void hideLoading();

    /**
     * Show a retry view in case of an error when retrieving data.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void showRetry();

    /**
     * Show a retry view in case of an error when retrieving data.
     *
     * @param message A string representing an error.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */

    /**
     * Hide a retry view shown if there was an error when retrieving data.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    void hideRetry();

    void showError(String psMessage);

    /**
     * Get a {@link Context}.
     *
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    Context context();
}
