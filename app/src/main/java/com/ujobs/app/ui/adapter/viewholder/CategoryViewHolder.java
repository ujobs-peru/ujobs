package com.ujobs.app.ui.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.ujobs.app.R;
import com.ujobs.app.model.CategoryModel;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class CategoryViewHolder extends GroupViewHolder {


    private TextView tvCategory;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        tvCategory = itemView.findViewById(R.id.tvCategory);
    }

    public void setCategory(CategoryModel category) {
        tvCategory.setText(category.getName());
    }


}
