package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.DistrictModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.DistrictViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link DistrictModel}.
         *
         * @param poDistrictModel {@link DistrictModel}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onDistrictItemClicked(DistrictModel poDistrictModel);
    }

    private List<DistrictModel> gaoDistrictModel;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link DistrictAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public DistrictAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoDistrictModel = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoDistrictModel != null) ? this.gaoDistrictModel.size() : 0;
    }

    @Override
    public DistrictViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_district, parent, false);
        return new DistrictViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DistrictViewHolder holder, final int position) {
        final DistrictModel loDistrictModel = this.gaoDistrictModel.get(position);
        holder.tvName.setText(loDistrictModel.getName());
        holder.ckSelect.setChecked(loDistrictModel.isChecked());
        holder.ckSelect.setTag(loDistrictModel);
        holder.ckSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                DistrictModel loDistrictModel = (DistrictModel) compoundButton.getTag();
                loDistrictModel.setChecked(b);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DistrictAdapter.this.onItemClickListener != null) {
                    DistrictAdapter.this.onItemClickListener.onDistrictItemClicked(loDistrictModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link DistrictModel}.
     *
     * @param poDistrictModelCollection List of {@link DistrictModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setDistrictList(List<DistrictModel> poDistrictModelCollection) {
        this.validateDistrictList(poDistrictModelCollection);
        this.gaoDistrictModel = poDistrictModelCollection;
        this.notifyDataSetChanged();
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link DistrictModel}
     *
     * @param poDistrictModelCollection List of {@link DistrictModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private void validateDistrictList(List<DistrictModel> poDistrictModelCollection) {
        if (poDistrictModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    public List<DistrictModel> getSelected() {
        List<DistrictModel> laoDistrictModel = new ArrayList<>();
        for (DistrictModel loDistrictModel : gaoDistrictModel) {
            if (loDistrictModel.isChecked()) {
                laoDistrictModel.add(loDistrictModel);
            }
        }
        return laoDistrictModel;
    }

    static class DistrictViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.ckSelect)
        CheckBox ckSelect;

        /**
         * Constructs a {@link DistrictViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        DistrictViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
