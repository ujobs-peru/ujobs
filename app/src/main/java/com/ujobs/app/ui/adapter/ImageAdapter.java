package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ujobs.app.R;
import com.ujobs.app.ui.util.ImageUtil;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link Integer}.
         *
         * @param poInteger {@link Integer}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onImageItemClicked(Integer poInteger);
    }

    private List<Integer> gaoInteger;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link ImageAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public ImageAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoInteger = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoInteger != null) ? this.gaoInteger.size() : 0;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {
        final Integer loInteger = this.gaoInteger.get(position);
        ImageUtil.getImage(holder.imgWorker, loInteger.toString());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ImageAdapter.this.onItemClickListener != null) {
                    ImageAdapter.this.onItemClickListener.onImageItemClicked(loInteger);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link Integer}.
     *
     * @param poIntegerCollection List of {@link Integer}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setImageList(List<Integer> poIntegerCollection) {
        if (validateImageList(poIntegerCollection)) {
            this.gaoInteger = poIntegerCollection;
            this.notifyDataSetChanged();
        }
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link Integer}
     *
     * @param poIntegerCollection List of {@link Integer}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private boolean validateImageList(List<Integer> poIntegerCollection) {
        if (poIntegerCollection == null) {
            return false;
        }
        return true;
    }


    static class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgImage)
        ImageView imgWorker;


        /**
         * Constructs a {@link ImageViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        ImageViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
