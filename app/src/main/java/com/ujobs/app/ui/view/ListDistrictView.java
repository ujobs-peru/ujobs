package com.ujobs.app.ui.view;

import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.DistrictModel;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface ListDistrictView extends LoadDataView {

    void showListDistrictSuccess(List<DistrictModel> paoListDistrictModel);
}
