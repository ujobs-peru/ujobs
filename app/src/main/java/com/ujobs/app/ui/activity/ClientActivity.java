package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.adapter.ClientTabAdapter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.ClientListRequestFragment;
import com.ujobs.app.ui.fragment.ClientSearchFragment;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientActivity extends BaseActivity implements ClientSearchFragment.OnSearchClientFragment,
        ClientListRequestFragment.OnClientListRequetFragment, GetUserView {


    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, ClientActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.tbTabs)
    TabLayout tbTabs;
    @BindView(R.id.vpPager)
    ViewPager vpPager;

    private ClientTabAdapter clientTabAdapter;
    private GetUserPresenter goGetUserPresenter;
    private UserModel goUserModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }


    @Override
    public void onSubCategorySelected(SubcategoryModel subcategory) {
        ClientActivity.this.navigator.navigateToRegisterRequest(subcategory);
    }

    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        this.goUserModel = poUserModel;

        clientTabAdapter = new ClientTabAdapter(getSupportFragmentManager(), goUserModel);
        clientTabAdapter.setTitle(getResources().getStringArray(R.array.client_tabs));
        vpPager.setAdapter(clientTabAdapter);
        vpPager.setOffscreenPageLimit(2);
        tbTabs.setupWithViewPager(vpPager);
    }


    @Override
    public void goOnRequest(RequestModel poRequestModel) {
        navigator.navigateToClientRequest(poRequestModel);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }


}
