package com.ujobs.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.fragment.ProfileFragment;
import com.ujobs.app.ui.fragment.WorkerListRequestFragment;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerTabAdapter extends FragmentStatePagerAdapter {


    private String[] titles;
    private UserModel goUserModel;

    public WorkerTabAdapter(FragmentManager fm, UserModel poUserModel) {
        super(fm);
        this.goUserModel = poUserModel;
    }

    public void setTitle(String[] titles) {
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment loFragment = null;
        switch (position) {
            case 0:
                loFragment = WorkerListRequestFragment.newInstance(goUserModel);
                break;
            case 1:
                loFragment = ProfileFragment.newInstance(goUserModel);
                break;
        }
        return loFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
