package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.RequestModel;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class ClientRequestAdapter extends RecyclerView.Adapter<ClientRequestAdapter.ClientRequestViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link RequestModel}.
         *
         * @param poRequestModel {@link RequestModel}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onClientRequestItemClicked(RequestModel poRequestModel);
    }

    private List<RequestModel> gaoRequestModel;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link ClientRequestAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public ClientRequestAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoRequestModel = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoRequestModel != null) ? this.gaoRequestModel.size() : 0;
    }

    @Override
    public ClientRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_client_request, parent, false);
        return new ClientRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClientRequestViewHolder holder, final int position) {
        final RequestModel loRequestModel = this.gaoRequestModel.get(position);
        holder.tvName.setText(loRequestModel.getName());
        holder.tvState.setText(loRequestModel.getTitleStateTypeClient());
        holder.tvCategoryDescription.setText(loRequestModel.getTextCategory());
        holder.tvCount.setText(String.valueOf(loRequestModel.getCountQuotes()));
        holder.tvDisrict.setText(loRequestModel.getDistrictShow());
        holder.tvStartDate.setText(loRequestModel.getStartDateShow());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClientRequestAdapter.this.onItemClickListener != null) {
                    ClientRequestAdapter.this.onItemClickListener.onClientRequestItemClicked(loRequestModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link RequestModel}.
     *
     * @param poRequestModelCollection List of {@link RequestModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setClientRequestList(List<RequestModel> poRequestModelCollection) {
        this.validateClientRequestList(poRequestModelCollection);
        this.gaoRequestModel = poRequestModelCollection;
        this.notifyDataSetChanged();
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link RequestModel}
     *
     * @param poRequestModelCollection List of {@link RequestModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private void validateClientRequestList(List<RequestModel> poRequestModelCollection) {
        if (poRequestModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class ClientRequestViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvState)
        TextView tvState;
        @BindView(R.id.tvCategoryDescription)
        TextView tvCategoryDescription;
        @BindView(R.id.tvStartDate)
        TextView tvStartDate;
        @BindView(R.id.tvCount)
        TextView tvCount;
        @BindView(R.id.tvDisrict)
        TextView tvDisrict;


        /**
         * Constructs a {@link ClientRequestViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        ClientRequestViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
