package com.ujobs.app.ui.view;

import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.model.UserModel;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface GetUserView extends LoadDataView {

    void showGetUserSuccess(UserModel poUserModel);
}
