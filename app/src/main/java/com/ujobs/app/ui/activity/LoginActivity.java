package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.TokenWatcher;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ujobs.app.R;
import com.ujobs.app.data.util.CalendarUtil;
import com.ujobs.app.data.util.LogUtil;
import com.ujobs.app.model.RegisterRequestModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.LoginPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.view.LoginView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.anderscheow.validator.Validation;
import io.github.anderscheow.validator.Validator;
import io.github.anderscheow.validator.conditions.common.And;
import io.github.anderscheow.validator.conditions.common.Or;
import io.github.anderscheow.validator.constant.Mode;
import io.github.anderscheow.validator.rules.common.NotEmptyRule;
import io.github.anderscheow.validator.rules.regex.EmailRule;
import io.github.anderscheow.validator.rules.regex.PasswordRule;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    private static final String TAG = LoginActivity.class.getSimpleName();

    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();

        Intent loIntent = new Intent(poContext, LoginActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }


    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;
    @BindView(R.id.edtEmailLayout)
    TextInputLayout edtEmailLayout;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;
    @BindView(R.id.edtPasswordLayout)
    TextInputLayout edtPasswordLayout;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    LoginPresenter goLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        setUpView();
    }

    private void setUpView() {
        goLoginPresenter = LoginPresenter.newInstance(this);
        Bundle loBundle = getIntent().getExtras();
    }


    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        final String txtEmail = edtEmail.getText().toString();
        final String txtPassword = edtPassword.getText().toString();
        final String token = FirebaseInstanceId.getInstance().getToken();


        /*
        if (TextUtils.isEmpty(txtEmail)) {
            edtEmail.setError("Debes escribir tu nombre de usuario");
            lbSuccess = false;
        }
        */


        /*
        if (TextUtils.isEmpty(txtPassword)) {
            edtPassword.setError("Debes escribir tu contraseña");
            lbSuccess = false;
        }
        */

        final Validation emailValidation = new Validation(edtEmailLayout)
                .and(new EmailRule(R.string.validate_email_format));

        final Validation passwordValidation = new Validation(edtPasswordLayout)
                .and(new NotEmptyRule(R.string.validate_password_notempty));

        Validator.with(getApplicationContext())
                .setMode(Mode.SINGLE)
                .validate(new Validator.OnValidateListener() {
                              @Override
                              public void onValidateSuccess(List<String> values) throws IndexOutOfBoundsException {
                                  goLoginPresenter.initialize(txtEmail, txtPassword, token);

                              }

                              @Override
                              public void onValidateFailed() {

                              }
                          },
                        emailValidation, passwordValidation);


    }


    @Override
    public void showLoginSuccess(UserModel poUserModel) {
        Toast.makeText(this, "Bienvenido: " + poUserModel.getNames(), Toast.LENGTH_LONG).show();

        LoginActivity.this.navigator.navigateToTypeUser();
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }


}
