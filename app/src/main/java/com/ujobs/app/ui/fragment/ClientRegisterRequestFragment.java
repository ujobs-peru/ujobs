package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.TypedArrayUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ujobs.app.R;
import com.ujobs.app.data.util.CalendarUtil;
import com.ujobs.app.data.util.LogUtil;
import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.model.RegisterRequestModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.ListDistrictPresenter;
import com.ujobs.app.presenter.RegisterRequestPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.util.MyDateRangeLimiter;
import com.ujobs.app.ui.view.ListDistrictView;
import com.ujobs.app.ui.view.RegisterRequestView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientRegisterRequestFragment extends BaseFragment implements ListDistrictView, RegisterRequestView, DatePickerDialog.OnDateSetListener {
    private final static String TAG = ClientRegisterRequestFragment.class.getSimpleName();

    private final static String BUNDLE_USER = "USER";
    private final static String BUNDLE_SUBCATEGORY = "SUBCATEGORY";

    public static ClientRegisterRequestFragment newInstance(UserModel poUserModel, SubcategoryModel poSubcategoryModel) {
        ClientRegisterRequestFragment fragment = new ClientRegisterRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_USER, poUserModel);
        bundle.putParcelable(BUNDLE_SUBCATEGORY, poSubcategoryModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    public interface OnClientRegisterRequestFragment {
        void onNextClicked(int piIdRequest);
    }


    @BindView(R.id.swNow)
    Switch swNow;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.ivDate)
    ImageButton ivDate;
    @BindView(R.id.llDate)
    LinearLayout llDate;
    @BindView(R.id.spDistrict)
    Spinner spDistrict;
    @BindView(R.id.edtDescription)
    EditText edtDescription;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.edtShorDescription)
    EditText edtShorDescription;

    Unbinder unbinder;

    OnClientRegisterRequestFragment goOnClientRegisterRequestFragment;
    ListDistrictPresenter districtPresenter;
    RegisterRequestPresenter goRegisterRequestPresenter;
    private Calendar goCalendarSelect;
    private UserModel goUserModel;
    private SubcategoryModel goSubcategoryModel;
    private boolean now;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goSubcategoryModel = getArguments().getParcelable(BUNDLE_SUBCATEGORY);

        districtPresenter = ListDistrictPresenter.newInstance(this);
        goRegisterRequestPresenter = RegisterRequestPresenter.newInstance(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }

    private void setUpView() {
        swNow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                now = isChecked;
                llDate.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            districtPresenter.initialize();
        }
    }

    @Override
    public void showListDistrictSuccess(List<DistrictModel> paoListDistrictModel) {
        ArrayAdapter<DistrictModel> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, paoListDistrictModel);
        spDistrict.setAdapter(adapter);
    }

    @Override
    public void showRegisterRequestSuccess(final Integer piIdRequest) {
        DialogView.create(context()).showBasicDialog(null, "Se registró correctamente",
                null, new DialogView.OnSingleButton() {
                    @Override
                    public void onClick(@NonNull MaterialDialog poDialog) {
                        goOnClientRegisterRequestFragment.onNextClicked(piIdRequest);
                    }
                }, false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @OnClick(R.id.btnNext)
    public void onNextClicked() {
        Calendar loCalendar = null;
        if (swNow.isChecked()) {
            loCalendar = Calendar.getInstance();
        } else {
            loCalendar = goCalendarSelect;
        }

        String txtShorDescription = edtShorDescription.getText().toString();
        String txtDescription = edtDescription.getText().toString();
        DistrictModel loDistrictModel = (DistrictModel) spDistrict.getSelectedItem();

        boolean lbSuccess = true;

        if (loCalendar == null) {
            DialogView.create(context()).showBasicDialog(null, "Debes seleccionar una fecha", false);
            lbSuccess = false;
        }

        if (TextUtils.isEmpty(txtShorDescription)) {
            edtShorDescription.setError("Debes escribir un titulo");
            lbSuccess = false;
        }

        if (TextUtils.isEmpty(txtDescription)) {
            edtDescription.setError("Debes escribir una descripción");
            lbSuccess = false;
        }

        if (lbSuccess) {
            RegisterRequestModel loRegisterRequestModel = new RegisterRequestModel();
            loRegisterRequestModel.setDescription(txtDescription);
            loRegisterRequestModel.setIdDistrict(loDistrictModel.getIdDistrict());
            loRegisterRequestModel.setIdUser(goUserModel.getIdUser());
            loRegisterRequestModel.setName(txtShorDescription);
            loRegisterRequestModel.setStartDate(CalendarUtil.getFormatDate(loCalendar.getTime(), RequestModel.FORMAT_DATE_SHOW));
            loRegisterRequestModel.setLstIdsCatSubcategory(Arrays.asList(goSubcategoryModel.getIdCatSubcategory()));
            goRegisterRequestPresenter.initialize(loRegisterRequestModel);
        }
    }


    @OnClick(R.id.ivDate)
    public void onCalendarClicked() {
        Calendar loCurrentCalendar = Calendar.getInstance();

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                loCurrentCalendar.get(Calendar.YEAR),
                loCurrentCalendar.get(Calendar.MONTH),
                loCurrentCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(loCurrentCalendar);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        goCalendarSelect = Calendar.getInstance();
        goCalendarSelect.set(Calendar.YEAR, year);
        goCalendarSelect.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        goCalendarSelect.set(Calendar.MONTH, monthOfYear);

        tvDate.setText(CalendarUtil.getFormatDate(goCalendarSelect.getTime(), RequestModel.FORMAT_DATE_SHOW));

    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClientRegisterRequestFragment) {
            goOnClientRegisterRequestFragment = (OnClientRegisterRequestFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
