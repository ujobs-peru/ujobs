package com.ujobs.app.ui.view;

import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface ListQuoteView extends LoadDataView {

    void showListQuoteSuccess(List<QuoteModel> paoData);
}
