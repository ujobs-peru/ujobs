/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.ujobs.app.ui.activity.BaseActivity;


/**
 * Base {@link android.app.Fragment} class for every fragment in this application.
 * <p>
 * Created by Manuel Torres on 11/01/2017.
 */
public abstract class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();

    /**
     * Shows a {@link Toast} message.
     *
     * @param psMessage An string representing a message to be shown.
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    protected void showToastMessage(String psMessage) {
        Toast.makeText(getActivity(), psMessage, Toast.LENGTH_SHORT).show();
    }


    /**
     * Get a context to activity.
     *
     * @return Context {@link Context}
     * @author Manuel Torres
     * @version 1.0
     * @since 10/01/2017
     */
    public Context getContext() {
        return getActivity();
    }


    public void showLoading() {
        ((BaseActivity) getActivity()).showLoading();
    }

    public void hideLoading() {
        ((BaseActivity) getActivity()).hideLoading();
    }
}
