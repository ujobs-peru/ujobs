package com.ujobs.app.ui.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ujobs.app.R;

/**
 * Created by bn on 22/11/17.
 */

public class ImageUtil {

    public static void getImage(ImageView imageView, String psId) {
        Context goContext = imageView.getContext();
        String url = String.format("%s%s%s%s%s%s", goContext.getString(R.string.protocol), goContext.getString(R.string.host),
                goContext.getString(R.string.context_jobs), goContext.getString(R.string.ws_resource), goContext.getString(R.string.endpoint_get_image), psId);
        Glide.with(goContext)
                .load(url)
                .into(imageView);
    }
}
