package com.ujobs.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.fragment.ClientListRequestFragment;
import com.ujobs.app.ui.fragment.ProfileFragment;
import com.ujobs.app.ui.fragment.ClientSearchFragment;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientTabAdapter extends FragmentStatePagerAdapter {


    private String[] titles;
    private final UserModel goUsermodel;

    public ClientTabAdapter(FragmentManager fm, UserModel poUserModel) {
        super(fm);
        goUsermodel = poUserModel;
    }

    public void setTitle(String[] titles) {
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment loFragment = null;
        switch (position) {
            case 0:
                loFragment = ClientSearchFragment.newInstance();
                break;
            case 1:
                loFragment = ClientListRequestFragment.newInstance(goUsermodel);
                break;
            case 2:
                loFragment = ProfileFragment.newInstance(goUsermodel);
                break;
        }
        return loFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
