package com.ujobs.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.QualifyModel;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

;

/**
 * Created by Manuel Torres on 12/01/2017.
 */
public class QualifyAdapter extends RecyclerView.Adapter<QualifyAdapter.QualifyViewHolder> {

    public interface OnItemClickListener {
        /**
         * On Item Clicked in list to {@link QualifyModel}.
         *
         * @param poQualifyModel {@link QualifyModel}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        void onQualifyItemClicked(QualifyModel poQualifyModel);
    }

    private List<QualifyModel> gaoQualifyModel;
    private final LayoutInflater goLayoutInflater;
    private final Context goContext;
    private OnItemClickListener onItemClickListener;

    /**
     * Constructs a {@link QualifyAdapter}.
     *
     * @param poContext {@link Context}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public QualifyAdapter(Context poContext) {
        this.goContext = poContext;
        this.goLayoutInflater =
                (LayoutInflater) poContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gaoQualifyModel = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.gaoQualifyModel != null) ? this.gaoQualifyModel.size() : 0;
    }

    @Override
    public QualifyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.goLayoutInflater.inflate(R.layout.row_qualify, parent, false);
        return new QualifyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QualifyViewHolder holder, final int position) {
        final QualifyModel loQualifyModel = this.gaoQualifyModel.get(position);
        holder.tvName.setText(loQualifyModel.getFullName());
        holder.tvQualify.setText(loQualifyModel.getStarsShow());
        holder.tvDescription.setText(loQualifyModel.getComment());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (QualifyAdapter.this.onItemClickListener != null) {
                    QualifyAdapter.this.onItemClickListener.onQualifyItemClicked(loQualifyModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set List of {@link QualifyModel}.
     *
     * @param poQualifyModelCollection List of {@link QualifyModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setQualifyList(List<QualifyModel> poQualifyModelCollection) {
        this.validateQualifyList(poQualifyModelCollection);
        this.gaoQualifyModel = poQualifyModelCollection;
        this.notifyDataSetChanged();
    }

    /**
     * Set on item click listener of {@link OnItemClickListener}.
     *
     * @param poOnItemClickListener List of {@link OnItemClickListener}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    public void setOnItemClickListener(OnItemClickListener poOnItemClickListener) {
        this.onItemClickListener = poOnItemClickListener;
    }

    /**
     * Validate to list of {@link QualifyModel}
     *
     * @param poQualifyModelCollection List of {@link QualifyModel}.
     * @author Manuel Torres
     * @version 1.0
     * @since 12/01/2017
     */
    private void validateQualifyList(List<QualifyModel> poQualifyModelCollection) {
        if (poQualifyModelCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }


    static class QualifyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgUser)
        ImageView imgUser;
        @BindView(R.id.tvQualify)
        TextView tvQualify;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        /**
         * Constructs a {@link QualifyViewHolder}.
         *
         * @param poItemView {@link View}.
         * @author Manuel Torres
         * @version 1.0
         * @since 12/01/2017
         */
        QualifyViewHolder(View poItemView) {
            super(poItemView);
            ButterKnife.bind(this, poItemView);
        }
    }

}
