package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ujobs.app.R;
import com.ujobs.app.model.DistrictModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.ListDistrictPresenter;
import com.ujobs.app.ui.adapter.CategoryAdapter;
import com.ujobs.app.ui.adapter.DistrictAdapter;
import com.ujobs.app.ui.view.ListDistrictView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerDistrictFragment extends BaseFragment implements ListDistrictView, CategoryAdapter.OnItemClickListener, DistrictAdapter.OnItemClickListener {


    private static final String BUNDLE_USER = "USER";

    public interface OnWorkerDistrictFragment {
        void onSave(List<DistrictModel> paoDistrictModel);
    }

    public static WorkerDistrictFragment newInstance(UserModel poUserModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        WorkerDistrictFragment loWorkerCategoryFragment = new WorkerDistrictFragment();
        loWorkerCategoryFragment.setArguments(loBundle);
        return loWorkerCategoryFragment;
    }

    @BindView(R.id.rvDistrict)
    RecyclerView rvDistrict;

    Unbinder unbinder;

    private OnWorkerDistrictFragment goWorkerDistrictFragment;

    ListDistrictPresenter goLisDistrictPresenter;
    private DistrictAdapter goDistrictAdapter;
    private UserModel goUserModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);

        goLisDistrictPresenter = ListDistrictPresenter.newInstance(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_district, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            loadListDistrict();
        }
    }

    private void setUpView() {


    }

    private void setupRecyclerView() {
        goDistrictAdapter = new DistrictAdapter(getContext());
        rvDistrict.setHasFixedSize(true);
        this.goDistrictAdapter.setOnItemClickListener(this);
        LinearLayoutManager loEntityLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        this.rvDistrict.setLayoutManager(loEntityLayoutManager);
        this.rvDistrict.setAdapter(goDistrictAdapter);

    }


    private void loadListDistrict() {
        this.goLisDistrictPresenter.initialize();
    }

    @Override
    public void showListDistrictSuccess(List<DistrictModel> paoListDistrictModel) {
        for (DistrictModel loDistrictModel : goUserModel.getLstDistrict()) {
            for (DistrictModel loDistrictModelAux : paoListDistrictModel) {
                if (loDistrictModel.getIdDistrict() == loDistrictModelAux.getIdDistrict()) {
                    loDistrictModelAux.setChecked(true);
                }
            }
        }
        this.goDistrictAdapter.setDistrictList(paoListDistrictModel);
    }

    @Override
    public void onDistrictItemClicked(DistrictModel poDistrictModel) {

    }


    @OnClick(R.id.btnSave)
    public void onViewClicked() {
        goWorkerDistrictFragment.onSave(goDistrictAdapter.getSelected());
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @Override
    public Context context() {
        return getContext();
    }


    @Override
    public void onSubCategoryClicked(SubcategoryModel subcategory) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWorkerDistrictFragment) {
            goWorkerDistrictFragment = (OnWorkerDistrictFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
