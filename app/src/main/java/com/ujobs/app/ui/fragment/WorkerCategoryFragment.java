package com.ujobs.app.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ujobs.app.R;
import com.ujobs.app.model.CategoryModel;
import com.ujobs.app.model.SubcategoryModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.ListCatAndSubcatPresenter;
import com.ujobs.app.ui.adapter.CategoryAdapter;
import com.ujobs.app.ui.view.ListCatAndSubcatView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class WorkerCategoryFragment extends BaseFragment implements ListCatAndSubcatView, CategoryAdapter.OnItemClickListener {


    private static final String BUNDLE_USER = "USER";

    public interface OnWorkerCategoryFragment {
        void onNext(List<SubcategoryModel> poListSubcategortyModel);
    }

    public static WorkerCategoryFragment newInstance(UserModel poUserModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        WorkerCategoryFragment loWorkerCategoryFragment = new WorkerCategoryFragment();
        loWorkerCategoryFragment.setArguments(loBundle);
        return loWorkerCategoryFragment;
    }

    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    Unbinder unbinder;

    private OnWorkerCategoryFragment goWorkerCategoryFragment;

    ListCatAndSubcatPresenter goListCatPresenter;
    private CategoryAdapter goCategoryAdapter;
    private UserModel goUserModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);

        goListCatPresenter = ListCatAndSubcatPresenter.newInstance(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_worker_category, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            goListCatPresenter.initialize();
        }
    }

    private void setUpView() {


    }


    @Override
    public void showListCatAndSubcatSuccess(List<CategoryModel> paoListCategoryModel) {

//        for (CategoryModel loCategoryModel : goUserModel.getLstCategory()) {
//            for (CategoryModel loCategoryModelAux : paoListCategoryModel) {
//                if (loCategoryModel.getIdCatSubcategory() == loCategoryModelAux.getIdCatSubcategory()) {
//                    for (SubcategoryModel loSubcategoryModel : loCategoryModel.getListSubcategories()) {
//                        for (SubcategoryModel loSubcategoryModelAux : loCategoryModelAux.getListSubcategories()) {
//                            if (loSubcategoryModel.getIdSubcategory() == loSubcategoryModelAux.getIdSubcategory()) {
//                                loSubcategoryModelAux.setCheck(true);
////                                break;
//                            }
//                        }
//                    }
////                    break;
//                }
//            }
//        }
        List<Integer> laoInteger = new ArrayList<>();
        for (CategoryModel loCategoryModel : goUserModel.getLstCategory()) {
            for (SubcategoryModel loSubcategoryModel : loCategoryModel.getListSubcategories()) {
                laoInteger.add(loSubcategoryModel.getIdCatSubcategory());
            }
        }

        goCategoryAdapter = new CategoryAdapter(paoListCategoryModel, this, View.VISIBLE, laoInteger);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCategory.setLayoutManager(layoutManager);
        rvCategory.setAdapter(goCategoryAdapter);
    }


    @OnClick(R.id.btnNext)
    public void onViewClicked() {
        goWorkerCategoryFragment.onNext(goCategoryAdapter.getSelected());
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {

    }

    @Override
    public Context context() {
        return getContext();
    }


    @Override
    public void onSubCategoryClicked(SubcategoryModel subcategory) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWorkerCategoryFragment) {
            goWorkerCategoryFragment = (OnWorkerCategoryFragment) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
