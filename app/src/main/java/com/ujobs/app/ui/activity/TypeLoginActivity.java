package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ujobs.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TypeLoginActivity extends BaseActivity {

    public static Intent getCallingIntent(Context poContext) {
        Bundle loBundle = new Bundle();
        Intent loIntent = new Intent(poContext, TypeLoginActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }

    @BindView(R.id.btnLoginFacebook)
    Button btnLoginFacebook;
    @BindView(R.id.btnLoginGoogle)
    Button btnLoginGoogle;
    @BindView(R.id.llLogin)
    LinearLayout llLogin;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnRegister)
    Button btnRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_login);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.btnLoginFacebook)
    public void onBtnLoginFacebookClicked() {
    }

    @OnClick(R.id.btnLoginGoogle)
    public void onBtnLoginGoogleClicked() {
    }

    @OnClick(R.id.btnLogin)
    public void onBtnLoginClicked() {
        TypeLoginActivity.this.navigator.navigateToLogin();
    }

    @OnClick(R.id.btnRegister)
    public void onBtnRegisterClicked() {
        TypeLoginActivity.this.navigator.navigateToSignIn();
    }
}
