package com.ujobs.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.RequestModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.presenter.GetUserPresenter;
import com.ujobs.app.ui.component.DialogView;
import com.ujobs.app.ui.fragment.ClientQuoteFragment;
import com.ujobs.app.ui.fragment.ClientRequestFragment;
import com.ujobs.app.ui.fragment.WorkerRequestFragment;
import com.ujobs.app.ui.view.GetUserView;

import butterknife.ButterKnife;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientRequestActivity extends BaseActivity implements GetUserView, ClientRequestFragment.OnClientRequestFragment,
        ClientQuoteFragment.OnClientQuoteFragment {
    private static final String BUNDLE_REQUEST = "REQUEST";

    public static Intent getCallingIntent(Context poContext, RequestModel poRequestModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_REQUEST, poRequestModel);
        Intent loIntent = new Intent(poContext, ClientRequestActivity.class);
        loIntent.putExtras(loBundle);
        return loIntent;
    }


    private GetUserPresenter goGetUserPresenter;
    private UserModel goUserModel;
    private RequestModel goRequestModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_request);
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        Bundle loBundle = getIntent().getExtras();
        goRequestModel = loBundle.getParcelable(BUNDLE_REQUEST);

        goGetUserPresenter = GetUserPresenter.newInstance(this);
        goGetUserPresenter.initialize();
    }


    @Override
    public void showGetUserSuccess(UserModel poUserModel) {
        this.goUserModel = poUserModel;
        addFragment(R.id.frlContainer, ClientRequestFragment.newInstance(goUserModel, goRequestModel));

    }

    @Override
    public void goQuote(QuoteModel poQuoteModel) {
        replaceFragment(R.id.frlContainer, ClientQuoteFragment.newInstance(goUserModel, goRequestModel, poQuoteModel));
    }

    @Override
    public void goBack(int piType) {
        goRequestModel.setIdStateType(piType);
        onBackPressed();
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String psMessage) {
        DialogView.create(context()).showBasicDialog(null, psMessage);
    }

    @Override
    public Context context() {
        return this;
    }


    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
            Fragment loFragment = getSupportFragmentManager().findFragmentById(R.id.frlContainer);
            if (loFragment instanceof ClientRequestFragment) {
                ((ClientRequestFragment) loFragment).refreshView(goRequestModel);
            }
        }

    }

}
