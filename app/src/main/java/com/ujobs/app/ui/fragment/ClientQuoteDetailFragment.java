package com.ujobs.app.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ujobs.app.R;
import com.ujobs.app.model.QuoteModel;
import com.ujobs.app.model.UserModel;
import com.ujobs.app.ui.adapter.ImageAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by dsbmobile on 11/12/17.
 */

public class ClientQuoteDetailFragment extends BaseFragment {

    private static final String BUNDLE_USER = "USER";
    private static final String BUNDLE_QUOTE = "QUOTE";


    public static ClientQuoteDetailFragment newInstance(UserModel poUserModel, QuoteModel poQuoteModel) {
        Bundle loBundle = new Bundle();
        loBundle.putParcelable(BUNDLE_USER, poUserModel);
        loBundle.putParcelable(BUNDLE_QUOTE, poQuoteModel);
        ClientQuoteDetailFragment loClientListRequestFragment = new ClientQuoteDetailFragment();
        loClientListRequestFragment.setArguments(loBundle);
        return loClientListRequestFragment;
    }

    public interface OnClientQuoteDetailFragment {

    }

    @BindView(R.id.tvAmount)
    TextView tvAmount;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.rcvImage)
    RecyclerView rcvImage;

    Unbinder unbinder;

    private OnClientQuoteDetailFragment goOnClientQuoteDetailFragment;
    private ImageAdapter goImageAdapter;
    private UserModel goUserModel;
    private QuoteModel goQuoteModel;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goUserModel = getArguments().getParcelable(BUNDLE_USER);
        goQuoteModel = getArguments().getParcelable(BUNDLE_QUOTE);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_quote_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        setUpView();
        setupRecyclerView();
        return view;
    }

    private void setUpView() {
        goImageAdapter = new ImageAdapter(getContext());
        rcvImage.setHasFixedSize(true);

        tvAmount.setText(goQuoteModel.getAmountTitleShow());
        tvDescription.setText(goQuoteModel.getDestailShow());
    }

    private void setupRecyclerView() {
        this.goImageAdapter.setOnItemClickListener(onImageItemClickListener);
        this.rcvImage.setLayoutManager(new GridLayoutManager(getContext(), 2));
        this.rcvImage.setAdapter(goImageAdapter);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            this.lodListImages();
        }
    }

    private void lodListImages() {
        goImageAdapter.setImageList(goQuoteModel.getLstIdsImage());

    }


    private ImageAdapter.OnItemClickListener onImageItemClickListener =
            new ImageAdapter.OnItemClickListener() {
                @Override
                public void onImageItemClicked(Integer poInteger) {

                }
            };
}
