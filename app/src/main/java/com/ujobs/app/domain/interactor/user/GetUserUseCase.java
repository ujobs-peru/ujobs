package com.ujobs.app.domain.interactor.user;


import android.content.Context;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.repository.UserDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.UserRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class GetUserUseCase {

    private final UserRepository goUserRepository;


    public static GetUserUseCase newInstance(Context poContext) {
        return new GetUserUseCase(poContext);
    }

    public GetUserUseCase(Context poContext) {
        this.goUserRepository = UserDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(DomainCallback<UserEntity> poCallback) {
        goUserRepository.getUser(poCallback);
    }


}
