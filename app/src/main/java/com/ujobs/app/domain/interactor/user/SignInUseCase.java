package com.ujobs.app.domain.interactor.user;


import android.content.Context;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.repository.UserDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.UserRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class SignInUseCase {

    private final UserRepository goUserRepository;


    public static SignInUseCase newInstance(Context poContext) {
        return new SignInUseCase(poContext);
    }

    public SignInUseCase(Context poContext) {
        this.goUserRepository = UserDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<Integer> poCallback) {

        goUserRepository.signIn(poParams.userEntity, poCallback);
    }


    public static final class Params {

        private final UserEntity userEntity;

        private Params(UserEntity userEntity) {
            this.userEntity = userEntity;
        }

        public static Params forUser(UserEntity userEntity) {
            return new Params(userEntity);
        }
    }

}
