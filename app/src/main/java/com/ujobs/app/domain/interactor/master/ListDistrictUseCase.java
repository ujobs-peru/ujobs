package com.ujobs.app.domain.interactor.master;


import android.content.Context;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.repository.MasterDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.MasterRepository;

import java.util.List;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class ListDistrictUseCase {

    private final MasterRepository goMasterRepository;


    public static ListDistrictUseCase newInstance(Context poContext) {
        return new ListDistrictUseCase(poContext);
    }

    public ListDistrictUseCase(Context poContext) {
        this.goMasterRepository = MasterDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(DomainCallback<List<DistrictEntity>> poCallback) {

        goMasterRepository.listDistrict(poCallback);
    }


}
