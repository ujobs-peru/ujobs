package com.ujobs.app.domain.interactor.user;


import android.content.Context;

import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.repository.UserDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.UserRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class RegisterWorkerUseCase {

    private final UserRepository goUserRepository;


    public static RegisterWorkerUseCase newInstance(Context poContext) {
        return new RegisterWorkerUseCase(poContext);
    }

    public RegisterWorkerUseCase(Context poContext) {
        this.goUserRepository = UserDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<String> poCallback) {

        goUserRepository.registerWorker(poParams.registerWorkerEntity, poCallback);
    }


    public static final class Params {

        private final RegisterWorkerEntity registerWorkerEntity;

        private Params(RegisterWorkerEntity registerWorkerEntity) {
            this.registerWorkerEntity = registerWorkerEntity;
        }

        public static Params forRegisterWorker(RegisterWorkerEntity registerWorkerEntity) {
            return new Params(registerWorkerEntity);
        }
    }

}
