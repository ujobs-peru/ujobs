package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

import java.util.List;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class ListQuoteUseCase {

    private final RequestRepository goRequestRepository;


    public static ListQuoteUseCase newInstance(Context poContext) {
        return new ListQuoteUseCase(poContext);
    }

    public ListQuoteUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<List<QuoteEntity>> poCallback) {
        FindQuoteEntity loFindQuoteEntity = new FindQuoteEntity();
        loFindQuoteEntity.setIdRequest(poParams.idRequest);
        goRequestRepository.listQuote(loFindQuoteEntity, poCallback);
    }


    public static final class Params {

        private final int idRequest;

        private Params(int idRequest) {
            this.idRequest = idRequest;
        }

        public static Params forListQuote(int idRequest) {
            return new Params(idRequest);
        }
    }

}
