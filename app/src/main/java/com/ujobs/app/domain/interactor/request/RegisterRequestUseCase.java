package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class RegisterRequestUseCase {

    private final RequestRepository goRequestRepository;


    public static RegisterRequestUseCase newInstance(Context poContext) {
        return new RegisterRequestUseCase(poContext);
    }

    public RegisterRequestUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<Integer> poCallback) {
        goRequestRepository.registerRequest(poParams.registerRequestEntity, poCallback);
    }


    public static final class Params {

        private final RegisterRequestEntity registerRequestEntity;

        private Params(RegisterRequestEntity registerRequestEntity) {
            this.registerRequestEntity = registerRequestEntity;
        }

        public static Params forRegisterRequest(RegisterRequestEntity registerRequestEntity) {
            return new Params(registerRequestEntity);
        }
    }

}
