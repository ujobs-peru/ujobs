package com.ujobs.app.domain;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface DomainCallback<T> {

    public void onSuccess(T poData);

    public void onError(Throwable poException);
}
