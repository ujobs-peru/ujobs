package com.ujobs.app.domain.interactor.master;


import android.content.Context;

import com.ujobs.app.data.repository.MasterDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.MasterRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class UploadFileUseCase {

    private final MasterRepository goMasterRepository;


    public static UploadFileUseCase newInstance(Context poContext) {
        return new UploadFileUseCase(poContext);
    }

    public UploadFileUseCase(Context poContext) {
        this.goMasterRepository = MasterDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<String> poCallback) {
        List<File> files = new ArrayList<>();
        for (String path : poParams.filePath) {
            files.add(new File(path));
        }
        goMasterRepository.uploadFile(files, poParams.type, poParams.id, poCallback);
    }


    public static final class Params {

        private final int id;
        private final List<String> filePath;
        private final String type;

        private Params(int id, List<String> filePath, String type) {
            this.id = id;
            this.filePath = filePath;
            this.type = type;
        }

        public static Params forUploadFile(int id, List<String> filePath, String type) {
            return new Params(id, filePath, type);
        }
    }

}
