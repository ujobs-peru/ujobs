package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class RegisterQualifyUseCase {

    private final RequestRepository goRequestRepository;


    public static RegisterQualifyUseCase newInstance(Context poContext) {
        return new RegisterQualifyUseCase(poContext);
    }

    public RegisterQualifyUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<Integer> poCallback) {
        RegisterQualifyEntity loRegisterQualifyEntity = new RegisterQualifyEntity();
        loRegisterQualifyEntity.setIdRequest(poParams.idRequest);
        loRegisterQualifyEntity.setStars(poParams.stars);
        loRegisterQualifyEntity.setComment(poParams.comment);
        goRequestRepository.registerQualify(loRegisterQualifyEntity, poParams.isClient, poCallback);
    }


    public static final class Params {

        public final Integer idRequest;
        public final Integer stars;
        public final String comment;
        public final boolean isClient;

        public Params(Integer idRequest, Integer stars, String comment, boolean isClient) {
            this.idRequest = idRequest;
            this.stars = stars;
            this.comment = comment;
            this.isClient = isClient;
        }


        public static Params forRegisterQualify(Integer idRequest, Integer stars, String comment, boolean isClient) {
            return new Params(idRequest, stars, comment, isClient);
        }
    }

}
