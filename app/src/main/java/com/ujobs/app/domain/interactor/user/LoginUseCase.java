package com.ujobs.app.domain.interactor.user;


import android.content.Context;

import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.repository.UserDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.UserRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class LoginUseCase {

    private final UserRepository goUserRepository;


    public static LoginUseCase newInstance(Context poContext) {
        return new LoginUseCase(poContext);
    }

    public LoginUseCase(Context poContext) {
        this.goUserRepository = UserDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<UserEntity> poCallback) {
        UserEntity loUserEntity = new UserEntity();
        loUserEntity.setEmail(poParams.email);
        loUserEntity.setPassword(poParams.password);
        loUserEntity.setIdFCM(poParams.idFCM);
        goUserRepository.login(loUserEntity, poCallback);
    }


    public static final class Params {

        private final String email;
        private final String password;
        private final String idFCM;

        private Params(String email, String password, String idFCM) {
            this.email = email;
            this.password = password;
            this.idFCM = idFCM;
        }

        public static Params forUser(String email, String password, String idFCM) {
            return new Params(email, password, idFCM);
        }
    }

}
