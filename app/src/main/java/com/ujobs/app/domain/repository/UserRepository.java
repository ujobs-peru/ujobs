package com.ujobs.app.domain.repository;

import com.ujobs.app.data.entity.RegisterWorkerEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.domain.DomainCallback;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface UserRepository {


    void login(UserEntity poUser, DomainCallback<UserEntity> poUserDomainCallback);

    void signIn(UserEntity poUser, DomainCallback<Integer> psDomainCallback);

    void registerWorker(RegisterWorkerEntity poRequest, DomainCallback<String> poCallback);

    void getUser(DomainCallback<UserEntity> poCallback);
}
