package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class RegisterQuoteUseCase {

    private final RequestRepository goRequestRepository;


    public static RegisterQuoteUseCase newInstance(Context poContext) {
        return new RegisterQuoteUseCase(poContext);
    }

    public RegisterQuoteUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<Integer> poCallback) {
        RegisterQuoteEntity loRegisterQuoteEntity = new RegisterQuoteEntity();
        loRegisterQuoteEntity.setAmount(poParams.amount);
        loRegisterQuoteEntity.setDetail(poParams.detail);
        loRegisterQuoteEntity.setIdRequest(poParams.idRequest);
        loRegisterQuoteEntity.setIdWorker(poParams.idWorker);
        goRequestRepository.registerQuote(loRegisterQuoteEntity, poCallback);
    }


    public static final class Params {

        public final int idWorker;
        public final int idRequest;
        public final String amount;
        public final String detail;

        private Params(int idWorker, int idRequest, String amount, String detail) {
            this.idWorker = idWorker;
            this.idRequest = idRequest;
            this.amount = amount;
            this.detail = detail;
        }

        public static Params forRegisterQuote(int idWorker, int idRequest, String amount, String detail) {
            return new Params(idWorker, idRequest, amount, detail);
        }
    }

}
