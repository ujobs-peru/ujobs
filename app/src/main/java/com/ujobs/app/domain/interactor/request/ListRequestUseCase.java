package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

import java.util.List;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class ListRequestUseCase {

    public final static int TYPE_USER = 1;
    public final static int TYPE_WORKER = 2;

    private final RequestRepository goRequestRepository;


    public static ListRequestUseCase newInstance(Context poContext) {
        return new ListRequestUseCase(poContext);
    }

    public ListRequestUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<List<RequestEntity>> poCallback) {
        FindRequestEntity loFindRequestEntity = new FindRequestEntity();
        loFindRequestEntity.setIdStateRequestType(poParams.idStateRequestType);
        switch (poParams.type) {
            case TYPE_USER:
                loFindRequestEntity.setIdUser(poParams.idUser);
                goRequestRepository.listRequestClient(loFindRequestEntity, poCallback);
                break;
            case TYPE_WORKER:
                loFindRequestEntity.setIdWorker(poParams.idUser);
                goRequestRepository.listRequestWorker(loFindRequestEntity, poCallback);
                break;
        }


    }


    public static final class Params {

        private final int idUser;
        private final int idStateRequestType;
        private final int type;

        private Params(int idUser, int idStateRequestType, int type) {
            this.idUser = idUser;
            this.idStateRequestType = idStateRequestType;
            this.type = type;
        }

        public static Params forListRequest(int idUser, int idStateRequestType, int type) {
            return new Params(idUser, idStateRequestType, type);
        }
    }

}
