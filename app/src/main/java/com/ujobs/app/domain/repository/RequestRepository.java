package com.ujobs.app.domain.repository;

import com.ujobs.app.data.entity.FindQuoteEntity;
import com.ujobs.app.data.entity.FindRequestEntity;
import com.ujobs.app.data.entity.QuoteEntity;
import com.ujobs.app.data.entity.RegisterQualifyEntity;
import com.ujobs.app.data.entity.RegisterQuoteEntity;
import com.ujobs.app.data.entity.RegisterRequestEntity;
import com.ujobs.app.data.entity.RequestEntity;
import com.ujobs.app.data.entity.UpdateTypeRequestEntity;
import com.ujobs.app.domain.DomainCallback;

import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface RequestRepository {


    void registerRequest(RegisterRequestEntity poRegisterRequestEntity, DomainCallback<Integer> poDomainCallback);

    void listRequestClient(FindRequestEntity poFindRequestEntity, DomainCallback<List<RequestEntity>> poDomainCallback);

    void listRequestWorker(FindRequestEntity poFindRequestEntity, DomainCallback<List<RequestEntity>> poDomainCallback);

    void listQuote(FindQuoteEntity poFindRequestEntity, DomainCallback<List<QuoteEntity>> poDomainCallback);

    void updateTypeRequest(UpdateTypeRequestEntity poUpdateTypeRequestEntity, DomainCallback<String> poDomainCallback);


    void registerQuote(RegisterQuoteEntity poRegisterQuoteEntity, DomainCallback<Integer> poDomainCallback);

    void registerQualify(RegisterQualifyEntity poRequest, boolean isClient, DomainCallback<Integer> poCallback);
}
