package com.ujobs.app.domain.repository;

import com.ujobs.app.data.entity.CategoryEntity;
import com.ujobs.app.data.entity.DistrictEntity;
import com.ujobs.app.data.entity.UserEntity;
import com.ujobs.app.data.repository.datasource.CallbackDataStore;
import com.ujobs.app.domain.DomainCallback;

import java.io.File;
import java.util.List;

/**
 * Created by Manuel Torres on 12/11/17.
 */

public interface MasterRepository {


    void listCatAndSubcategories(DomainCallback<List<CategoryEntity>> poDomainCallback);

    void listDistrict(DomainCallback<List<DistrictEntity>> poDomainCallback);

    void uploadFile(List<File> files, String psType, int piId, DomainCallback<String> poCallback);

}
