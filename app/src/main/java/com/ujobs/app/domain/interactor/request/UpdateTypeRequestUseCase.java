package com.ujobs.app.domain.interactor.request;


import android.content.Context;

import com.ujobs.app.data.entity.UpdateTypeRequestEntity;
import com.ujobs.app.data.repository.RequestDataRepository;
import com.ujobs.app.domain.DomainCallback;
import com.ujobs.app.domain.repository.RequestRepository;

/**
 * Created by Manuel TOrres on 8/02/2017.
 */

public class UpdateTypeRequestUseCase {

    private final RequestRepository goRequestRepository;


    public static UpdateTypeRequestUseCase newInstance(Context poContext) {
        return new UpdateTypeRequestUseCase(poContext);
    }

    public UpdateTypeRequestUseCase(Context poContext) {
        this.goRequestRepository = RequestDataRepository.newInstance(poContext);
    }


    public void buildUseCaseObservable(Params poParams, DomainCallback<String> poCallback) {
        UpdateTypeRequestEntity loUpdateTypeRequestEntity = new UpdateTypeRequestEntity();
        loUpdateTypeRequestEntity.setIdRequest(poParams.idRequest);
        loUpdateTypeRequestEntity.setIdTypeRequest(poParams.idTypeRequest);
        loUpdateTypeRequestEntity.setIdQuote(poParams.idQuote);
        goRequestRepository.updateTypeRequest(loUpdateTypeRequestEntity, poCallback);
    }


    public static final class Params {

        private final int idTypeRequest;
        private final int idRequest;
        private final int idQuote;

        private Params(int idRequest, int idTypeRequest, int idQuote) {
            this.idTypeRequest = idTypeRequest;
            this.idRequest = idRequest;
            this.idQuote = idQuote;
        }

        public static Params forUpdateTypeRequest(int idRequest, int idTypeRequest, int idQuote) {
            return new Params(idRequest, idTypeRequest, idQuote);
        }
    }

}
